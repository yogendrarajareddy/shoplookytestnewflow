var server = require('../../server/server');
module.exports = function (BuyerEarnedPoints) {


    BuyerEarnedPoints.sendPasscodeToBuyer = function (userData, cb) {
        //console.log('Entered...');
        var cb = cb;

        var randomstring = require("randomstring");

        var passcode = randomstring.generate({
            length: 6,
            charset: 'numeric'
        });
        var sellerId = userData.sellerId;
        var buyerId = userData.buyerId;
        var SMS = server.models.SMS;
        var Notification=server.models.Notification;
        var Buyer = server.models.Buyer;
        var LoyaltySeller = server.models.LoyaltySeller;
        var Seller = server.models.Seller;
        var LoyaltyBuyer = server.models.LoyaltyBuyer;
        Seller.findOne({"where": {id: sellerId}}, function (err, seller) {
            Buyer.findOne({"where": {id: buyerId}}, function (err, buyerR) {

                /*if (userData.redeemedPoints == "0" && userData.type=="redeem") {


                }*/
                 if (userData.redeemedPoints == "0") {
                    /*SMS.create({
                     destination: "8801407198",
                     message: 'your unique code for redeeming loyalty points is ' + passcode
                     }, function (err, sms) {
                     console.log(sms);
                     });*/
                    LoyaltyBuyer.create({
                        'accumulatedPoints': "NA",
                        'redeemedAmount': "0",
                        'redeemedPoints': "0",
                        'orderId': "NA",
                        'passcode': passcode,
                        'sellerId': sellerId,
                        'sellerName':seller.name,
                        'sellerPhone':seller.phone,
                        'buyerName': buyerR.name,
                        'buyerPhone': buyerR.phone,
                        'buyerId': buyerId,
                        'status': "InActive"
                    }, function (err, loyaltyBuyerDet) {
                        var sellerJson = {};
                        sellerJson['status'] = "passcodeSent";
                        sellerJson['redeemAmount'] = "0";
                        cb(null, sellerJson);
                    });


                } else {
                    var buyerRedeemedPoints = Number(userData.redeemedPoints);

                    console.log("buyer: " + userData.buyerId + " seller: " + userData.sellerId);
                    console.log("inputPoints: " + userData.redeemedPoints);
                    LoyaltySeller.find({"where": {"sellerId": sellerId}}, function (err, loyaltySellerDet) {
                        //console.log('...Customer:'+JSON.stringify(buyers));
                        if (err) {
                            cb(err, null);
                        } else {

                            if (loyaltySellerDet.length > 0) {
                                var loyaltySellerDetails = loyaltySellerDet[0];
                                console.log("amount: " + loyaltySellerDetails.redemptionAmount);
                                console.log("points: " + loyaltySellerDetails.redemptionPoints);
                                var buyerRedeemAmountForSeller = (Number(loyaltySellerDetails.redemptionAmount)) * (buyerRedeemedPoints) / Number(loyaltySellerDetails.redemptionPoints)
                                if (buyerRedeemAmountForSeller <= 0) {
                                    buyerRedeemAmountForSeller = 0;
                                }
                                var sellerJson = {};
                                sellerJson['status'] = "passcodeSent";
                                sellerJson['redeemAmount'] = buyerRedeemAmountForSeller;
                                /*SMS.create({
                                 destination: "8801407198",
                                 message: 'your unique code for redeeming loyalty points is ' + passcode
                                 }, function (err, sms) {
                                 console.log(sms);
                                 });*/
                                LoyaltyBuyer.create({
                                    'accumulatedPoints': "NA",
                                    'redeemedAmount': loyaltySellerDetails.redemptionAmount,
                                    'redeemedPoints': loyaltySellerDetails.redemptionPoints,
                                    'orderId': "NA",
                                    'passcode': passcode,
                                    'sellerId': sellerId,
                                    'sellerName':seller.name,
                                    'sellerPhone':seller.phone,
                                    'buyerId': buyerId,
                                    'buyerName': buyerR.name,
                                    'buyerPhone': buyerR.phone,
                                    'status': "InActive"
                                }, function (err, loyaltyBuyerDet) {


                                    var seller_gcm_id;
                                    var seller_device_token_id;
                                    try {
                                        if (seller.gcm_id != "NA") {
                                            seller_gcm_id = seller.gcm_id;
                                            var gcmData = {
                                                "registration_ids": [
                                                    seller_gcm_id
                                                ],
                                                "data": {
                                                    "message": "Request from buyer to  redeem loyalty points",
                                                    "type": "loyaltyBuyerDet",
                                                    'sellerId': sellerId,
                                                    'sellerName':seller.name,
                                                    'sellerPhone':seller.phone,
                                                    'buyerId': buyerId,
                                                    'buyerName': buyerR.name,
                                                    'buyerPhone': buyerR.phone,
                                                    "operation": "redeemLoyaltyPoints_Request",
                                                    "id": loyaltyBuyerDet.id
                                                }
                                            };
                                            GCM.create(gcmData, function (err, gcm) {
                                            });
                                        }
                                    } catch (e) {
                                        console.log("Error in redeemLoyaltyPoints request in buyerEarnedPoints: " + JSON.stringify(e));
                                    }
                                    try {
                                        if (seller.devicetoken != "NA") {
                                            seller_device_token_id = seller.devicetoken;
                                            var appleNotificationData = {
                                                "registration_ids": [
                                                    seller_device_token_id
                                                ],
                                                "data": {
                                                    "message": "Request from buyer to  redeem loyalty points",
                                                    "type": "loyaltyBuyerDet",
                                                    'sellerId': sellerId,
                                                    'sellerName':seller.name,
                                                    'sellerPhone':seller.phone,
                                                    'buyerId': buyerId,
                                                    'buyerName': buyerR.name,
                                                    'buyerPhone': buyerR.phone,
                                                    "operation": "redeemLoyaltyPoints_Request",
                                                    "id": loyaltyBuyerDet.id
                                                }
                                            };
                                            APN.create(appleNotificationData, function (err, apn) {
                                            });
                                        }
                                    } catch (e) {
                                        console.log("Error in redeemLoyaltyPoints request in buyerEarnedPoints: " + JSON.stringify(e));
                                    }
                                    var notificationDetails = {
                                        "from": buyerId,
                                        "fromType": "buyer",
                                        "to": sellerId,
                                        "toType": "seller",
                                        "status": "new",
                                        "message": "Request from buyer to  redeem loyalty points",
                                        "type": "loyaltyBuyerDet",
                                        'sellerName':seller.name,
                                        'sellerPhone':seller.phone,
                                        'buyerId': buyerId,
                                        'buyerName': buyerR.name,
                                        'buyerPhone': buyerR.phone,
                                        "operation": "redeemLoyaltyPoints_Request",
                                        "typeId": loyaltyBuyerDet.id
                                    };
                                    Notification.create(notificationDetails, function (err, notificationR) {
                                        console.log("Subscription_Request notificationR: " + JSON.stringify(notificationR));
                                    });

                                    cb(null, sellerJson);
                                    //});

                                });


                            } else {
                                var sellerJson = {};
                                sellerJson['status'] = "seller redeem details not found";
                                cb(null, sellerJson);
                            }
                        }
                    });

                }
            });
        });
    };

    BuyerEarnedPoints.remoteMethod('sendPasscodeToBuyer', {
        description: "Calculating redeem amount for buyer points with particular seller",
        returns: {
            arg: 'redeemDetails',
            type: 'object'
        },
        accepts: [{arg: 'userData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/sendPasscodeToBuyer',
            verb: 'POST'
        }
    });

};
