var server = require('../../server/server');

module.exports = function (Account) {

    Account.observe('loaded', function(ctx, next) {

        var Seller = server.models.Seller;
        var Buyer = server.models.Buyer;
        if(ctx.instance) {

            //console.log('Instance......:'+JSON.stringify(ctx.instance));
            var phone = ctx.instance.userId;

            if(ctx.instance.userType == 'Seller'){

                Seller.findOne({"where":{"phone":phone}}, function(err, seller){
                    if(seller==undefined){
                        next();
                    }else{
                        ctx.instance['seller'] = seller;
                        ctx.instance['frameCount']=seller.frameCount;
                        next();
                    }


                });

            } else if (ctx.instance.userType == 'Buyer') {

                Buyer.findOne({"where":{"phone":phone}}, function(err, buyer){
                    //console.log('Error:'+JSON.stringify(err));
                    //console.log('Buyer data:'+JSON.stringify(buyer));
                    ctx.instance['buyer'] = buyer;
                    next();

                });

            }

        } else {
            next();
        }

    });

    Account.listAccounts = function (accountName, cb) {

        Account.findOne({"where": {"userId": accountName, "userType": "Seller"}}, function(err, account){

            if(err) {
                var err = new Error("error in msg");
                cb(err, null);
            } else if(account != undefined && account != null) {

                var response = [];
                response.push(account);
                cb(null, response);

            } else {

                var Business = server.models.Business;
                var pattern = new RegExp('.*' + accountName + '.*', "i");
                //console.log('pattern: '+ pattern);
                var filterString = {where: {businessName: {like: pattern}}};
                Account.find(filterString, function (err, accounts) {

                    if (err) cb(null, []);
                    //console.log(accounts);

                    if (accounts.length == 0) {
                        cb(null, []);
                    } else {

                        var accountsSize = accounts.length;

                        var returnAccounts = [];
                        for (var z in accounts) {

                            var account = accounts[z];

                            if((account['seller']['otpStatus'] == true ) || account['seller']['paymentStatus'] == true) {

                                returnAccounts.push(account);
                            }

                        }

                        cb(null, returnAccounts);

                    }

                });

            }

        });

    }

    Account.remoteMethod('listAccounts', {
        description: "Gets all accounts information bases on business name",
        returns: {
            arg: 'accounts',
            type: 'array'
        },
        accepts: {arg: 'accountName', type: 'string', http: {source: 'query'}},
        http: {
            path: '/list-accounts',
            verb: 'get'
        }
    });


    Account.validateOTP = function (phone, otp, userType, cb) {

        var cb = cb;
        if (userType.toString() == 'Buyer') {

            var Buyer = server.models.Buyer;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Buyer.findOne({"where": {"phone": phone}}, function (err, buyer) {

                if (err) {
                    cb(null, err);
                }
                //console.log("buyer info: " + JSON.stringify(buyer));

                if (buyer.otp == otp) {

                    //console.log('OTP is updated');

                    buyer.updateAttributes({otpStatus: true}, function (err, buyerNext) {
                        //console.log("buyer is updated: " + buyer);
                        var smsMessage = 'Hi! Thank you for downloading and registering on VYULI, now keep adding your Local retailers to get updates on what is new in their store';
                        var destination = buyerNext.phone;

                        SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                        });

                        var fs = require('fs');

                        fs.readFile(__dirname + '/../../client/htmltemplates/BuyerConfirmation.html', 'utf8', function (err, data) {
                            if (err) {
                                return console.log(err);
                            }

                            data = data.replace('$userName$', buyerNext.name);

                            EmailN.create({
                                to: buyerNext.mail,
                                from: 'info@ouroneapp.com',
                                subject: 'Registration Success',
                                text: '',
                                html: data
                            }, function (err, email) {

                            });

                            EmailN.create({
                                to: 'info@ouroneapp.com',
                                from: 'info@ouroneapp.com',
                                subject: 'Registration Success',
                                text: '',
                                html: data
                            }, function (err, email) {

                            });
                            //console.log(data);
                        });


                        cb(null, buyerNext);
                    });

                } else {
                    cb(null, null);
                    //next();
                }

            });

        } else if (userType.toString() == 'Seller') {

            var Seller = server.models.Seller;
            Seller.findOne({"where": {"phone": phone}}, function (err, seller) {

                if (err) {
                    cb(null, err);
                }
                //console.log("seller: " + JSON.stringify(seller));

                if (seller.otp == otp) {

                    seller.updateAttributes({otpStatus: true}, function (err, sellerNext) {

                        var SMS = server.models.SMS;

                        var smsMessage = 'Hi! Thank you for downloading and registering on VYULI, now showcase your product or services to your customers, and keep them updated.';
                        var destination = phone;

                        SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                        });
                        var mobileNo="9100287823";
                         var smsContent="Hi Admin, New seller: "+seller.name+", SellerBusinessName: "+seller.businessName+", BusinessName: "+seller.businessTypeName+" and Mobile No: "+seller.phone+" is registered with VYULI";
                         SMS.create({destination: mobileNo, message: smsContent}, function(err, sms){

                         });
                        //console.log("seller is updated: " + JSON.stringify(sellerNext));
                        if(sellerNext.paymentStatus == true) {

                            var fs = require('fs');
                            fs.readFile(__dirname + '/../../client/htmltemplates/SellerConfirmation.html', 'utf8', function (err, data) {
                                if (err) {
                                    return console.log(err);
                                }

                                data = data.replace('$userName$', sellerNext.name);

                                EmailN.create({
                                    to: sellerNext.mail,
                                    from: 'info@ouroneapp.com',
                                    subject: 'Registration Success',
                                    text: '',
                                    html: data
                                }, function (err, email) {

                                });

                                EmailN.create({
                                    to: 'info@ouroneapp.com',
                                    from: 'info@ouroneapp.com',
                                    subject: 'Registration Success',
                                    text: '',
                                    html: data
                                }, function (err, email) {

                                });
/*
                                var SMS = server.models.SMS;

                                var smsMessage = 'Hi! Thank you for downloading and registering on VYULI, now showcase your latest product offering to your customers.';
                                var destination = buyerNext.phone;

                                SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                                });*/
                                //console.log(data);
                            });
                            cb(null, sellerNext);

                        } else {

                            var SMS = server.models.SMS;

                            var smsMessage = 'Hi! Thank you for downloading and registering on VYULI, on your payment realisation your account would be activated to showcase your product offering to your customers.';
                            var destination = sellerNext.phone;

                            SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                            });
                            cb(null, sellerNext);
                        }
                    });

                } else {
                    var cr = {
                        status: "failure",
                        message: "OTP is not matched",
                        data: {}
                    };

                    //ctx,status = "Failure";
                    //ctx.message = "OTP Is not validated";
                    cb(null, null);
                    //next();
                }

            });

        }

    };

    Account.remoteMethod('validateOTP', {
        description: "Enter your otp to activate account",
        returns: {
            arg: 'user',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'otp', type: 'string', http: {source: 'query'}},
            {arg: 'userType', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/validateOTP',
            verb: 'get'
        }
    });

    Account.resendOTP = function (phone, userType, cb) {

        var cb = cb;
        if (userType.toString() == 'Buyer') {

            var Buyer = server.models.Buyer;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Buyer.findOne({"where": {"phone": phone}}, function (err, buyer) {

                if (err) {
                    cb(null, err);
                }

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });;

                var SMS = server.models.SMS;

                var smsMessage = 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline';
                var destination = buyer.phone;


                buyer.updateAttributes({otp: otp, password: otp}, function (err, buyerNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });

                });

            });

        } else if (userType.toString() == 'Seller') {

            var Seller = server.models.Seller;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Seller.findOne({"where": {"phone": phone}}, function (err, seller) {

                if (err) {
                    cb(null, err);
                }

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });;

                var SMS = server.models.SMS;

                var smsMessage = 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline';
                var destination = seller.phone;


                seller.updateAttributes({otp: otp}, function (err, sellerNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });

                });

            });

        }

    };

    Account.remoteMethod('resendOTP', {
        description: "Enter your otp to activate account",
        returns: {
            arg: 'buyer',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'userType', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/resendOTP',
            verb: 'get'
        }
    });

    Account.forgotPassword = function (phone, userType, cb) {

        var cb = cb;
        if (userType.toString() == 'Buyer') {

            var Buyer = server.models.Buyer;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Buyer.findOne({"where": {"phone": phone}}, function (err, buyer) {

                if (err) {
                    cb(null, err);
                }

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                var smsMessage = 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline';
                var destination = buyer.phone;
                var responseData={};
                responseData.message=smsMessage;
                responseData.destination=buyer.phone;
                responseData.otp=otp;


                buyer.updateAttributes({"otp": otp}, function (err, buyerNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, responseData);
                    });

                });

            });

        } else if (userType.toString() == 'Seller') {

            var Seller = server.models.Seller;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Seller.findOne({"where": {"phone": phone}}, function (err, seller) {

                if (err) {
                    cb(null, err);
                }

                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;


                var smsMessage = 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline';
                var destination = seller.phone;

                var responseData={};
                responseData.message=smsMessage;
                responseData.destination=seller.phone;
                responseData.otp=otp;


                seller.updateAttributes({"otp": otp}, function (err, sellerNext) {

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, responseData);
                    });

                });

            });

        }

    };

    Account.remoteMethod('forgotPassword', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'response',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'userType', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/forgotPassword',
            verb: 'get'
        }
    });

    Account.paymentStatusUpdate = function (phone, paymentStatus, cb) {

        var cb = cb;

        var Seller = server.models.Seller;
        var SMS = server.models.SMS;
        var EmailN = server.models.EmailN;

        Seller.findOne({"where": {"phone": phone}}, function (err, seller) {

            if (err) {
                cb(null, err);
            }

            var SMS = server.models.SMS;

            seller.updateAttributes({"paymentStatus": paymentStatus}, function (err, sellerNext) {

                if(err){
                    cb(null, err);
                }

                if(paymentStatus){

                    var smsMessage = 'Hi, Congratulations your Vyuli App account is now active, please go ahead uploading the images and showcase your latest product offering to your customers';
                    var destination = seller.phone;

                    var fs = require('fs');
                    fs.readFile(__dirname + '/../../client/htmltemplates/SellerConfirmation.html', 'utf8', function (err, data) {
                        if (err) {
                            return console.log(err);
                        }

                        data = data.replace('$userName$', sellerNext.name);

                        EmailN.create({
                            to: sellerNext.mail,
                            from: 'info@ouroneapp.com',
                            subject: 'Registration Success',
                            text: '',
                            html: data
                        }, function (err, email) {

                        });

                        EmailN.create({
                            to: 'info@ouroneapp.com',
                            from: 'info@ouroneapp.com',
                            subject: 'Registration Success',
                            text: '',
                            html: data
                        }, function (err, email) {

                        });
                        //console.log(data);
                    });

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });
                } else {
                    var smsMessage = 'Dear Customer,Your Vyuli App account has been deactivated due to your payment realisation was unsucessful. Please contact our support team for further assistance.';
                    var destination = seller.phone;

                    var fs = require('fs');
                    fs.readFile(__dirname + '/../../client/htmltemplates/SellerPending.html', 'utf8', function (err, data) {
                        if (err) {
                            return console.log(err);
                        }

                        data = data.replace('$userName$', sellerNext.name);

                        EmailN.create({
                            to: sellerNext.mail,
                            from: 'info@ouroneapp.com',
                            subject: 'Registration Pending',
                            text: '',
                            html: data
                        }, function (err, email) {

                        });

                        EmailN.create({
                            to: 'info@ouroneapp.com',
                            from: 'info@ouroneapp.com',
                            subject: 'Registration Pending',
                            text: '',
                            html: data
                        }, function (err, email) {

                        });
                        //console.log(data);
                    });

                    SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                        cb(null, sms);
                    });
                }

            });

        });

    };

    Account.remoteMethod('paymentStatusUpdate', {
        description: "please update payment status",
        returns: {
            arg: 'seller',
            type: 'array'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'paymentStatus', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/paymentStatusUpdate',
            verb: 'get'
        }
    });

    Account.addNewFrames = function (sellerId, noOfFrames, cb) {

        var cb = cb;

        var Frame = server.models.Frame;

        var cc = 0;
        var frames = [];
        for(var c=0; c<noOfFrames; c++) {

            Frame.create({"sellerId":sellerId, "status":true}, function(err, frame){
                frames.push(frame);
                cc++;
                if(err) {
                    cb(null, err);
                }
                if(cc == noOfFrames){
                    cb(null, frames);
                }

            });

        }

        //var Seller = server.models.Seller;
        ////var SMS = server.models.SMS;
        ////var EmailN = server.models.EmailN;
        //
        //Seller.findOne({"where": {"id": sellerId}}, function (err, seller) {
        //
        //    if (err) {
        //        cb(null, err);
        //    }
        //
        //    //var SMS = server.models.SMS;
        //    //
        //    //var smsMessage = 'Your Account has been activated please login';
        //    //var destination = seller.phone;
        //
        //
        //
        //
        //
        //    //seller.updateAttributes({"paymentStatus": true}, function (err, sellerNext) {
        //    //
        //    //    if(err){
        //    //        cb(null, err);
        //    //    }
        //    //    //SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
        //    //    //    cb(null, sms);
        //    //    //});
        //    //
        //    //});
        //
        //});

    };

    Account.remoteMethod('addNewFrames', {
        description: "please update payment status",
        returns: {
            arg: 'seller',
            type: 'array'
        },
        accepts: [{arg: 'sellerId', type: 'string', http: {source: 'query'}},
            {arg: 'noOfFrames', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/addNewFrames',
            verb: 'get'
        }
    });

    Account.tempLogin = function (phone, otp, userType, cb) {

        var cb = cb;
        var AccessToken = server.models.AccessToken;
        if (userType.toString() == 'Buyer') {

            var Buyer = server.models.Buyer;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Buyer.findOne({"where": {"phone": phone}}, function (err, buyer) {

                if (err) {
                    cb(err, null);
                }

                if(buyer != null && buyer != undefined) {

                    if(buyer.otp == otp) {

                        AccessToken.create({
                            "ttl": 600,
                            "created": new Date(),
                            "userId": buyer.id
                        },function(err, accessToken){

                            cb(null, accessToken);

                        });

                    } else {
                        var err = new Error("OTP is not matched");
                        cb(err, null);
                    }

                } else {
                    var err = new Error("user not found");
                    cb(err, null);
                }

            });

        } else if (userType.toString() == 'Seller') {

            var Seller = server.models.Seller;
            var SMS = server.models.SMS;
            var EmailN = server.models.EmailN;

            Seller.findOne({"where": {"phone": phone}}, function (err, seller) {

                if (err) {
                    cb(err, null);
                }

                if(seller != null && seller != undefined) {

                    if(seller.otp == otp) {

                        AccessToken.create({
                            "ttl": 600,
                            "created": new Date(),
                            "userId": seller.id
                        },function(err, accessToken){

                            cb(null, accessToken);

                        });

                    } else {
                        var err = new Error("OTP is not matched");
                        cb(err, null);
                    }

                } else {
                    var err = new Error("user not found");
                    cb(err, null);
                }

            });

        } else {
            var err = new Error("user type not available");
            cb(err, null);
        }

    };

    Account.remoteMethod('tempLogin', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'user',
            type: 'object'
        },
        accepts: [{arg: 'phone', type: 'string', http: {source: 'query'}},
            {arg: 'otp', type: 'string', http: {source: 'query'}},
            {arg: 'userType', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/tempLogin',
            verb: 'get'
        }
    });


    Account.customImplementation = function (cb) {

        var cb = cb;

        var Buyer = server.models.Buyer;

        Buyer.find({}, function (err, buyers) {

            var count = 0;
            for (var b in buyers) {

                var buyer = buyers[b];

                if (buyer.otpStatus == undefined || buyer.otpStatus == null) {

                    var randomstring = require("randomstring");

                    var otp = randomstring.generate(6);

                    buyer.updateAttributes({otpStatus: false, otp: otp}, function (err, buyerNext) {
                        //console.log("buyer is updated: " + JSON.stringify(buyerNext));
                        count++;
                        //if(count == buyers.length) {
                        //    cb(null, buyerNext);
                        //}
                    });

                }

            }

        });

    }

    Account.remoteMethod('customImplementation', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'subscriptions',
            type: 'array'
        },
        accepts: {http: {source: 'query'}},
        http: {
            path: '/customImplementation',
            verb: 'get'
        }
    });


    Account.ci = function (cb) {

        var cb = cb;

        var Buyer = server.models.Buyer;
        var Seller = server.models.Seller;

        Buyer.find({}, function (err, buyers) {

            for (var b in buyers) {

                var buyer = buyers[b];

                buyer.updateAttributes({appUrl: 'https://play.google.com/store/apps/details?id=com.aaray.ouroneapp'}, function (err, buyerNext) {
                    //console.log("buyer is updated: " + JSON.stringify(buyerNext));
                    //if(count == buyers.length) {
                    //    cb(null, buyerNext);
                    //}
                });

            }

        });

        Seller.find({}, function (err, sellers) {

            for (var b in sellers) {

                var seller = sellers[b];

                seller.updateAttributes({appUrl: 'https://play.google.com/store/apps/details?id=com.aaray.ouroneapp'}, function (err, sellerNext) {
                    //console.log("seller is updated: " + JSON.stringify(sellerNext));
                    //if(count == sellers.length) {
                    //    cb(null, sellerNext);
                    //}
                });

            }

        });

    }

    Account.remoteMethod('ci', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'subscriptions',
            type: 'array'
        },
        accepts: {http: {source: 'query'}},
        http: {
            path: '/ci',
            verb: 'get'
        }
    });

    Account.buyerExists = function (userData, cb) {
        //console.log('Entered...');
        var cb = cb;

        var Buyer = server.models.Buyer;
        var Account = server.models.Account;
        var Business=server.models.Business;
        var BuyerBusiness=server.models.BuyerBusiness;
        var phone = userData.phone;
        Account.find({"where": {"userId": phone, "userType":"Buyer"}}, function (err, buyers) {
        //    console.log('...Customer:'+JSON.stringify(buyers));
            if (err){
                console.log("error at initial stage");
                cb(err, null);

            }else{

            var randomstring = require("randomstring");

            var otp = randomstring.generate({
                length: 6,
                charset: 'numeric'
            });

            var SMS = server.models.SMS;

            if (buyers.length > 0) {

                var buyer = buyers[0];

                Buyer.find({"where":{"id":buyer.uid}}, function(err, buyerInfo){
                    var buyerInfo = buyerInfo[0];
                    //console.log("Buyers Entered:");

                    buyerInfo.updateAttributes({"otp": otp,"otpStatus":true, "password": otp}, function (err, buyerData) {

                        SMS.create({destination: phone, message: 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline'}, function (err, sms) {
                            console.log(sms);
                        });
                        cb(null, buyerData);
                    })
                });

            } else {
                /*if (true) {
                    Business.find({}, function(err, businessInfo){
                        for(var i=0;i<businessInfo.length;i++){
                            BuyerBusiness.create({'buyerId': "test",'businessName': businessInfo.name, 'businessTypeId': businessInfo.id, 'status':'enabled'}, function () {
                            });
                        }

                    });

                }else if(true){*/

                console.log("buyer new");
                var buyerName="NA";
                var countryCode="+91";
                if(userData.name!=undefined){
                    buyerName=userData.name;
                }
                if(userData.countryCode!=undefined){
                    countryCode=userData.countryCode;
                }
                Buyer.create({
                    "phone": phone,
                    "otp": otp,
                    "email": phone + '@ouroneapp.com',
                    "mail": phone + '@ouroneapp.com',
                    "password": otp,
                    "gcm_id":userData.gcm_id,
                    "gcm_flag":userData.gcm_flag,
                    "registerdate":userData.registerdate,
                    "otpStatus": false,
                    "buyerProfilePic":userData.buyerProfilePic,
                    "devicetoken":userData.devicetoken,
                    "dashboardSetting":0,
                    "originId":"NA",
                    "buyerCityName":"NA",
                    "buyerCountryName":"NA",
                    "buyerloclatitude":"NA",
                    "buyerloclongitude":"NA",
                    "buyerAddress":"NA",
                    "countryCode":countryCode,
                    "name": buyerName
                }, function (err, buyer) {
                    console.log("buyer after creation: " + JSON.stringify(buyer));
                    if (err) {
                        console.log("entered into error");
                        cb(err, null);

                    } else if (buyer) {
                        //cb(null, buyer);
                        Account.create({
                            'uid': buyer.id,
                            'userType': 'Buyer',
                            'userId': buyer.phone,
                            'email': buyer.email,
                            'mail': buyer.email,
                            'name': buyer.name,
                            "buyer": buyer
                        }, function (err, accountDetails) {
                            console.log('account is created');
                             SMS.create({
                             destination: phone,
                             message: 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline'
                             }, function (err, sms) {
                             console.log(sms);
                             //cb(null, buyer);
                                 Business.find({}, function(err, businessInfo){
                                     for(var i=0;i<businessInfo.length;i++){
                                         var businessName=businessInfo[i].name;
                                         var businessId=businessInfo[i].id;
                                         console.log("BuyerBusinesses ......"+businessName);
                                         console.log("BuyerBusinesses ......"+businessId);
                                         console.log("");
                                         BuyerBusiness.create({'buyerId': buyer.id,'businessName':businessName,'businessTypeId':businessId,'status':'enabled'}, function () {
                                         });
                                         //console.log("BuyerBusinesses creating......"+businessName);
                                     }
                                     if(i==businessInfo.length){
                                         cb(null, buyer);
                                         console.log("called back at end......");
                                     }
                             });


                            });

                            //console.log("creating customer"+JSON.stringify(buyer));

                        });
                    }

                });
            //}
            }
                //var err = new Error("User information is not available");
                //cb(err, null);
            }

        });

    };

    Account.remoteMethod('buyerExists', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'buyer',
            type: 'array'
        },
        accepts: [{arg: 'userData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/buyerExists',
            verb: 'POST'
        }
    });

    Account.test = function (userData, cb) {
        var Business=server.models.Business;
        var BuyerBusiness=server.models.BuyerBusiness;
        Business.find({}, function(err, businessInfo){
            console.log("businessInfo length: "+businessInfo.length);
            console.log("businessInfo: "+JSON.stringify(businessInfo));
            for(var i=0;i<businessInfo.length;i++){
                BuyerBusiness.create({'buyerId': "test",'businessName': businessInfo.name, 'businessTypeId': businessInfo.id, 'status':'enabled'}, function (err,buyerBusinesses) {
                    if(err){
                        console.log("err in buyerBusinesses: "+JSON.stringify(err));
                    }else {
                        console.log("buyerBusinesses: " + JSON.stringify(buyerBusinesses));
                    }
                });
                console.log("BuyerBusinesses creating......");
            }
            if(i==businessInfo.length){
                cb(null, businessInfo);
                console.log("called back at end......");
            }

        });
    }
    Account.remoteMethod('test', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'buyer',
            type: 'array'
        },
        accepts: [{arg: 'userData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/test',
            verb: 'POST'
        }
    });



    // for checking seller existed or not
    Account.sellerExists = function (userData, cb) {
        //console.log('Entered...');
        var cb = cb;

        var Seller = server.models.Seller;
        var Account = server.models.Account;
        var phone = userData.phone;
        Account.find({"where": {"userId": phone, "userType":"Seller"}}, function (err, sellers) {
            //console.log('...Customer:'+JSON.stringify(buyers));
            if (err) {
                cb(err, null);
            } else {


                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;
                var isSellerFound = "";
                if (sellers.length > 0) {
                    isSellerFound = "yes";
                    var seller = sellers[0];

                    Seller.find({"where": {"id": seller.uid}}, function (err, sellerInfo) {
                        var sellerInfo = sellerInfo[0];
                        //console.log("Buyers Entered:");

                        sellerInfo.updateAttributes({"otp": otp, "password": otp}, function (err, sellerData) {

                            SMS.create({
                             destination: phone,
                             message: 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline'
                             }, function (err, sms) {
                             console.log(sms);
                             });
                            var sellerJson={};
                            sellerJson['sellerStatus']="seller found";
                            sellerJson['sellerDet']=sellerData;
                            cb(null, sellerJson);
                        })
                    });

                } else {
                    /*SMS.create({
                        destination: phone,
                        message: 'your one time password is ' + otp
                    }, function (err, sms) {
                        console.log(sms);
                    });*/
                    isSellerFound = "no";
                    var sellerJson={};
                    sellerJson['sellerStatus']="seller not found";
                    cb(null, sellerJson);
                }
            }
        });

    };

    Account.remoteMethod('sellerExists', {
        description: "Enter mobile number and user type to reset the password",
        returns: {
            arg: 'seller',
            type: 'array'
        },
        accepts: [{arg: 'userData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/sellerExists',
            verb: 'POST'
        }
    });


};
