module.exports = function(Apn) {

    Apn.beforeSave = function(next, apn2) {

        console.log("sending apple push notifiction");
        var apn = require('apn');

        var options = {
            cert:'conf/OuroneAppNewCerti.pem',
            key:'conf/ouroneappNewKey.pem'
        };

        var apnConnection = new apn.Connection(options);

        var note = new apn.Notification();

        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.badge = 1;
        note.sound = "ping.aiff";
        note.alert = apn2.data.message;
        //note.data = apn2.data;
        note.payload = {'messageFrom': 'ouroneapp', 'data':apn2.data};
        //console.log("registration_ids: "+JSON.stringify(apn2.registration_ids));
        for(var ri in apn2.registration_ids) {

            var registration_id = apn2.registration_ids[ri];
            //console.log("ri: "+registration_id);
            var myDevice = new apn.Device(registration_id);
            //console.log('Note data:'+JSON.stringify(note));
            apnConnection.pushNotification(note, myDevice);

        }

        apnConnection.shutdown();

        next();

        //var data = {
        //    "registration_ids": gcm.registration_ids,
        //    "data": gcm.data
        //};
        //
        //var https = require('https');
        //
        //var options = {
        //    host: 'android.googleapis.com',
        //    path: '/gcm/send',
        //    method:'POST',
        //    headers: {
        //        'Content-Type': 'application/json',
        //        'Authorization': 'key = AIzaSyDK2DkIfTr4WzX15RnM8E_cLkl476ZGsx4'
        //    }
        //};
        //
        //var req = https.request(options, function(res){
        //    res.setEncoding('utf8');
        //    res.on('data', function (chunk) {
        //        console.log('Response: ' + chunk);
        //    });
        //});
        //
        //req.write(JSON.stringify(data));
        //req.end();
        ////your logic goes here
        //next();
    };


};
