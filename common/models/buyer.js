var server = require('../../server/server');

module.exports = function(Buyer) {

    //Buyer.beforeCreate = function (next, buyer) {
    Buyer.observe('before save', function filterProperties(ctx, next) {
        if(ctx.instance) {
            console.log('Buyer:' + JSON.stringify(ctx.instance));
            //var randomstring = require("randomstring");

            //var otp = randomstring.generate({
            //    length: 6,
            //    charset: 'numeric'
            //});
            //
            //var SMS = server.models.SMS;
            //var EmailN = server.models.EmailN;
            //
            //buyer.otp = otp;
            //buyer.otpStatus = false;
            var currentDate = new Date().toISOString();
            var splitDate = currentDate.split('-');
            var splitTime = currentDate.split('T');
            var time=splitTime[1];
            console.log("time: "+time);
            var currentTime=time.split('.');
            console.log("currentTime: "+currentTime[0]);
            var orderHour = splitTime[1].split(':');
            var orderDate = splitDate[2].split('T');
            ctx.instance.registeredDate = splitDate[0] + "-" + splitDate[1] + "-" + orderDate[0];
            ctx.instance.createdTime= splitDate[0] + "-" + splitDate[1] + "-" + orderDate[0]+" "+currentTime[0];
            var Buyer=server.models.Buyer;

            /*ctx.instance.loyalty = {
                "totalPoints": 0,
                "points": 0,
                "amount": 0
            };
            ctx.instance.referral = {
                "totalPoints": 0,
                "points": 0,
                "amount": 0
            };*/
            ctx.instance.appUrl = 'https://play.google.com/store/apps/details?id=com.aaray.ouroneapp';

            //var smsMessage = 'Your unique verification code for Ouroneapp is '+otp+' Thank you';
            //var destination = buyer.phone;
            //
            //SMS.create({destination: destination, message: smsMessage}, function(err, sms){
            //
            //});
            console.log("inside buyer log");
            var buyerstotalCount=0;
            Buyer.count({}, function(err,totalCount){
                    if (err) {
                        console.log("Total error "+err);
                    }else {
                        console.log("Total count "+totalCount);
                        buyerstotalCount=Number(totalCount)+1;
                        ctx.instance.SNo=buyerstotalCount;
                        if(buyerstotalCount>0){
                            next();
                        }
                    }
                }
            );



        }else{
            next();
        }

        });

    Buyer.observe('after save', function filterProperties(ctx, next) {
       console.log('after save details are'+JSON.stringify(ctx.instance));

        var SMS = server.models.SMS;
        if (ctx.isNewInstance) {

            var smsMessage ="Hi! Thank you for downloading and registering on VYULI, now keep adding your Local retailers to get updates on what is new in their store.";
            var destination = ctx.instance.phone;

            SMS.create({destination: destination, message: smsMessage}, function(err, sms){

            });
            next();
        }else{
            next();
        }



    });

    Buyer.buyerSMS = function (message,cb) {


        Buyer.find({}, function(err, buyers){
            var SMS = server.models.SMS;
            /*for(var b in buyers) {
                var buyer = buyers[b];
            }*/
            console.log("buyers list: "+JSON.stringify(buyers));
            for(var i=0;i<buyers.length;i++){
                var buyer=buyers[i];
                //var phone=buyer.phone;

                //var smsMessage = 'Your unique verification code for Ouroneapp is '+otp+' Thank you';
                var destination = buyer.phone;
                console.log("Buyer phone: "+destination);

                SMS.create({destination: destination, message: message}, function(err, sms){

                });

            }
            cb(null,"complted");
        });


    }

    Buyer.remoteMethod(
        'buyerSMS',
        {
           accepts:[{arg:'message',type:'string',http: {source: 'query'}}],
           returns:[{arg:'buyers',type:'array'}],
           http: {
                path: '/sendBuyerSMS',
                verb: 'get'
            }
        }
    );
    // sending sms for referring to others
    Buyer.buyerReferralSMS = function (phone,message,cb) {
            var SMS = server.models.SMS;
                var destination = phone;
                console.log("Buyer phone: "+destination);
                SMS.create({destination: destination, message: message}, function(err, sms){
                    if (err) {
                        cb(err, null);
                    }else{
                        cb(null,"success");
                    }
                });
    }
    Buyer.remoteMethod(
        'buyerReferralSMS',
        {
            accepts:[
                     {arg:'phone',type:'string',http: {source: 'query'}},
                     {arg:'message',type:'string',http: {source: 'query'}}
            ],
            //returns:[{arg:'status',type:'string'}],
            http: {
                path: '/buyerReferralSMS',
                verb: 'get'
            }
        }
    );
    Buyer.buyerReferralDefaultSMS = function (cb) {
        var message = 'Hi! Check out this new app, here I can view fresh arrivals and offers in my favourite Shops. https://play.google.com/store/apps/details?id=com.aaray.ouroneapp';

            cb(null,message);

    }
    Buyer.remoteMethod(
        'buyerReferralDefaultSMS',
        {
            returns:[{arg:'message',type:'string'}],
            http: {
                path: '/buyerReferralDefaultSMS',
                verb: 'get'
            }
        }
    );
    // notification to seller from buyer for uploading new frames
    Buyer.notifyAboutUploadingFrames = function (userData, cb) {
        var Seller = server.models.Seller;
        var GCM = server.models.GCM;
        var APN = server.models.APN;
        var Notification=server.models.Notification;
        var cb = cb;
        var sellerId=userData.sellerId;

        Seller.findOne({"where": {id: sellerId}}, function (err, seller) {
            if (err) {
                next(err);
            } else {
               console.log('Else')
                Notification.find({
                    "where": {
                        "to": userData.sellerId, "type": "NofitySellerAboutUploadFrames",
                        "operation": "Frames_Upload_Request","status":"new"
                    }
                }, function (err, NotificationR) {
                    //var NotificationRes=NotificationR[0];
                    //console.log("NotificationR: " + JSON.stringify(NotificationR));
                    //if (NotificationR.length == 0) {
                    //    console.log("NotificationRLength: " + NotificationR.length);
                    var notificationDetails = {
                        "from": userData.buyerId,
                        "fromType": "buyer",
                        "buyerName": userData.buyerName,
                        "to": userData.sellerId,
                        "toType": "seller",
                        "buyerContact":userData.buyerContact,
                        "sellerContact": userData.sellerContact,
                        "sellergcmid": seller.gcm_id,
                        "deviceToken": seller.devicetoken,
                        "message": "Please Upload New Frames",
                        "type": "NofitySellerAboutUploadFrames",
                        "operation": "Frames_Upload_Request",
                        "status": "new",
                        "typeId": userData.sellerId
                    };
                    Notification.create(notificationDetails, function (err, notificationR) {
                        console.log("NofitySellerAboutUploadFrames notificationR: ");
                        var seller_gcm_id;

                        if(seller.gcm_id!="NA"){
                            seller_gcm_id=seller.gcm_id;
                        }else if(seller.devicetoken!="NA"){
                            seller_gcm_id=seller.devicetoken;
                        }
                        console.log("sellergcmid: "+seller_gcm_id);
                        // newly added code for sending notification regarding New Frames update
                        var gcmData = {
                            "registration_ids": [
                                seller_gcm_id
                            ],
                            "data": {
                                "message": "Please Upload New Frames",
                                "type": "NofityAboutUploadFrames",
                                "operation": "Frames_Upload_Request",
                                "buyerName": userData.buyerName,
                                "buyerContact":userData.buyerContact,
                                "id": sellerId
                            }
                        };

                        if(seller.gcm_id!="NA"){
                            GCM.create(gcmData, function(err, gcm){
                            });
                        }else if(seller.devicetoken!="NA"){
                            APN.create(gcmData, function(err, apn){
                            });
                        }
                    });
cb(null,'Ok');
                    //}
                });
            }

        });

    };
    Buyer.remoteMethod('notifyAboutUploadingFrames', {
        description: "Enter sellerId to send notification",
        returns: {
            type: 'string', arg:'rest'
        },
        accepts: [{arg: 'userData', type: 'object', http: {source: 'body'}}],
        http: {
            path: '/notifyAboutUploadingFrames',
            verb: 'POST'
        }
    });

    //  buyerDefaultDataAppending
    Buyer.buyerDefaultDataAppending = function (searchData, cb) {
        var Buyer = server.models.Buyer;
        var buyersList = [];
        /*"registeredDate": "NA",
         "createdTime": "NA",*/
        var updateCount = 0;
        Buyer.find({}, function (err, buyers) {
            for (var i = 0; i < buyers.length; i++) {
                var buyerDet = buyers[i];
                buyerDet.updateAttributes({
                    "dashboardSetting":0,
                    "originId": "NA",
                    "buyerProfilePic": "NA",
                    "devicetoken": "NA",
                    "buyerCityName": "NA",
                    "buyerCountryName": "NA",
                    "buyerloclatitude": "NA",
                    "buyerloclongitude": "NA",
                    "countryCode":"+91",
                    "buyerAddress": "NA",
                    "SNo":i+1
                    }, function (err, buyerInfoR) {
                    updateCount = updateCount + 1;
                    console.log("updateCount: " + updateCount);
                    if (buyers.length == updateCount) {
                        console.log("response given......");
                        cb(null, "completed");
                    }
                });
            }
            console.log("buyersCount: " + buyers.length);
        });
    }

    Buyer.remoteMethod(
        'buyerDefaultDataAppending',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/buyerDefaultDataAppending',
                verb: 'get'
            }
        }
    );

    //  updateBuyerRegisteredDate
    Buyer.updateBuyerRegisteredDate = function (searchData, cb) {
        var Buyer = server.models.Buyer;
        Buyer.find({}, function (err, buyers) {
            for (var i = 0; i < buyers.length; i++) {
                var buyerData = buyers[i];
                var registerdate = buyerData.registerdate;
                // var date = '05-Jul-2016'.split("-");
                var date = registerdate.split("-");
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                for (var j = 0; j < months.length; j++) {
                    if (date[1] == months[j]) {
                        date[1] = months.indexOf(months[j]) + 1;
                    }
                }
                if (date[1] < 10) {
                    date[1] = '0' + date[1];
                }
                var formattedDate = date[2] + "-" + date[1] + "-" + date[0];
                console.log("formattedDate: " + formattedDate);
                buyerData.updateAttributes({
                    "registeredDate": formattedDate,
                    "createdTime": formattedDate + " 00:00:00"
                }, function (err, buyerInfoR) {

                });
            }
        });
        cb(null, "completed");
    }

    Buyer.remoteMethod(
        'updateBuyerRegisteredDate',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/updateBuyerRegisteredDate',
                verb: 'get'
            }
        }
    );


    //  buyerBusinessDefaultDataAppending
    Buyer.buyerBusinessDefaultDataAppending = function (searchData, cb) {
        var Buyer = server.models.Buyer;
        var BuyerBusiness=server.models.BuyerBusiness;
        var Business=server.models.Business;
        var buyersList = [];
        var updateCount = 0;
        Buyer.find({}, function (err, buyers) {
            Business.find({}, function(err, businessInfo){
            for (var i = 0; i < buyers.length; i++) {
                // var buyerDet = buyers[i];
                var  buyerDetId = buyers[i].id;
                    for(var j=0;j<businessInfo.length;j++){
                        var businessName=businessInfo[j].name;
                        var businessId=businessInfo[j].id;
                        console.log("BuyerBusinesses ......"+businessName);
                        console.log("BuyerBusinesses ......"+businessId);
                        console.log("buyerDetId: "+buyerDetId);
                        BuyerBusiness.create({'buyerId': buyerDetId,'businessName':businessName,'businessTypeId':businessId,'status':'enabled'}, function () {
                        });
                    }
            }
            });
            console.log("buyersCount: " + buyers.length);
        });
        cb(null, "completed");
    }

    Buyer.remoteMethod(
        'buyerBusinessDefaultDataAppending',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/buyerBusinessDefaultDataAppending',
                verb: 'get'
            }
        }
    );
    
};


