var server = require('../../server/server');

module.exports = function (Subscription) {
    var Seller = server.models.Seller;

    Subscription.validatesUniquenessOf('sbid', {message: 'buyer already subscribed for given seller'});

    Subscription.observe('loaded', function (ctx, next) {

        var Seller = server.models.Seller;
        var Buyer = server.models.Buyer;
        var Frame = server.models.Frame;
        if (ctx.instance) {

            var sellerId = ctx.instance.sellerId;
            var buyerId = ctx.instance.buyerId;

            Seller.findById(sellerId, function (err, seller) {

                ctx.instance['seller'] = seller;
                Buyer.findById(buyerId, function (err, buyer) {

                    ctx.instance['buyer'] = buyer;
                    Frame.find({"where": {"sellerId": sellerId, "status": true}}, function (err, frames) {
                        if (frames == undefined) {
                            ctx.instance['uploadedFramesCount'] = 0;
                        } else {
                            ctx.instance['uploadedFramesCount'] = frames.length;
                        }

                        next();

                    });

                    /* next();*/

                });

            });

        } else {
            next();
        }

    });

    Subscription.observe('before save', function subscriptionDetails(ctx, next) {
        if (ctx.instance) {
            console.log("entering...before save subscriptionDetails");

            var Subscription = server.models.Subscription;
            var subscriptionstotalCount = 0;
            Subscription.count({}, function (err, totalCount) {
                    if (err) {
                        console.log("Total error " + JSON.stringify(err));
                    } else {
                        console.log("Total count " + totalCount);
                        var currentDate = new Date().toISOString();
                        var splitDate = currentDate.split('-');
                        var splitTime = currentDate.split('T');
                        var time = splitTime[1];
                        var currentTime = time.split('.');
                        var orderHour = splitTime[1].split(':');
                        var orderDate = splitDate[2].split('T');
                        ctx.instance.subScribedDate = splitDate[0] + "-" + splitDate[1] + "-" + orderDate[0];
                        ctx.instance.createdTime = splitDate[0] + "-" + splitDate[1] + "-" + orderDate[0] + " " + currentTime[0];
                        subscriptionstotalCount = Number(totalCount) + 1;
                        ctx.instance.SNo = subscriptionstotalCount;
                        if (subscriptionstotalCount > 0) {
                            next();
                        }
                    }
                }
            );

            // next();
        } else {
            console.log("coming inside before save but not new instance");
            next();
        }
    });
    Subscription.observe('after save', function subscriptionDetails(ctx, next) {
        console.log("ctx.isNewInstance: " + ctx.isNewInstance);
        var Seller = server.models.Seller;
        var Buyer = server.models.Buyer;
        var Notification = server.models.Notification;
        if (ctx.isNewInstance) {

            console.log('after save subscriptionDetails details are: ' + JSON.stringify(ctx));
            //next();

            Seller.find({"where": {"id": ctx.instance.sellerId}}, function (err, sellerR) {
                console.log("sellerR: " + JSON.stringify(sellerR));
                var sellerRes = sellerR[0];
                if (sellerRes.subscriptionInvitation != undefined && sellerRes.subscriptionInvitation == "enabled") {

                    Buyer.find({"where": {"id": ctx.instance.buyerId}}, function (err, buyerR) {
                        var buyerRes = buyerR[0];
                        var seller_gcm_id;
                        try {
                            if (sellerRes.gcm_id != "NA") {
                                seller_gcm_id = sellerRes.gcm_id;
                            }
                        } catch (e) {
                            console.log("Error in subscription request in aftersave: " + JSON.stringify(e));
                        }

                        try {
                            if (sellerRes.devicetoken != "NA") {
                                seller_gcm_id = sellerRes.devicetoken;
                            }
                        } catch (e) {
                            console.log("Error in subscription request in aftersave : " + JSON.stringify(e));
                        }
                        /*                        "sellerBusinessname": sellerRes.businessName,
                         "businessCatgName": sellerRes.businessTypeName,
                         "businessTypeId": sellerRes.businessTypeId,
                         "sellergcmid": sellerRes.gcm_id,
                         "deviceToken": sellerRes.devicetoken,*/
                        var gcmData = {
                            "registration_ids": [
                                seller_gcm_id
                            ],
                            "data": {
                                "message": "Please Approve Subscription Request",
                                "type": "Subscription",
                                "operation": "Subscription_Request",
                                "buyerName": buyerRes.name,
                                "buyerContact": buyerRes.phone,
                                "sellerName": sellerRes.name,
                                "sellerContact": sellerRes.phone,
                                "buyerId": ctx.instance.buyerId,
                                "fromType": "buyer",
                                "sellerId": ctx.instance.sellerId,
                                "toType": "seller",
                                "id": ctx.instance.id
                            }
                        };
                        try {
                            if (sellerRes.gcm_id != "NA") {
                                GCM.create(gcmData, function (err, gcm) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in subscription  Request: " + JSON.stringify(e));
                        }
                        try {
                            if (sellerRes.devicetoken != "NA") {
                                APN.create(gcmData, function (err, apn) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in subscription Request: " + JSON.stringify(e));
                        }

                        Notification.find({
                            "where": {
                                "from": ctx.instance.buyerId, "to": ctx.instance.sellerId, "type": "Subscription",
                                "operation": "Subscription_Request"
                            }
                        }, function (err, NotificationR) {
                            //var NotificationRes=NotificationR[0];
                            console.log("NotificationR: " + JSON.stringify(NotificationR));
                            if (NotificationR.length == 0) {
                                console.log("NotificationRLength: " + NotificationR.length);
                                var notificationDetails = {
                                    "from": ctx.instance.buyerId,
                                    "fromType": "buyer",
                                    "buyerName": buyerRes.name,
                                    "to": ctx.instance.sellerId,
                                    "toType": "seller",
                                    "buyerContact": buyerRes.phone,
                                    "sellerContact": sellerRes.phone,
                                    "sellerName": sellerRes.name,
                                    "message": "Please Approve Subscription Request",
                                    "type": "Subscription",
                                    "operation": "Subscription_Request",
                                    "status": "new",
                                    "typeId": ctx.instance.id
                                };
                                Notification.create(notificationDetails, function (err, notificationR) {
                                    console.log("Subscription_Request notificationR: " + JSON.stringify(notificationR));
                                });
                            }
                        });
                    });
                }
                //next();
            });


            var ReferralBuyer = server.models.ReferralBuyer;
            var ReferralSeller = server.models.ReferralSeller;
            var Subscription = server.models.Subscription;
            var referredById;
            if (ctx.instance.referredBy != undefined && ctx.instance.referredBy != null) {
                Buyer.findOne({
                    "where": {
                        "phone": ctx.instance.referredBy
                    }
                }, function (err, buyerR) {
                    if (buyerR) {
                        referredById = buyerR.id;
                        console.log("referredById: " + referredById + "   referredBybuyerPhone: " + buyerR.phone);
                        //}
                        //});
                        Subscription.find({
                            "where": {
                                "sellerId": ctx.instance.sellerId,
                                "buyerId": referredById
                            }
                        }, function (err, subscriptionR) {
                            if (subscriptionR.length > 0) {
                                console.log("subscriptionR: " + subscriptionR.length);
                                Seller.find({"where": {"id": ctx.instance.sellerId}}, function (err, sellerR) {
                                    if (sellerR.length > 0) {
                                        console.log("sellerR: " + sellerR.length);
                                        var sellerRes = sellerR[0];
                                        if (sellerRes.referralActivationStatus == "enabled") {
                                            ReferralSeller.find({"where": {"sellerId": ctx.instance.sellerId}}, function (err, referralSellerR) {
                                                console.log("referralSellerR: " + JSON.stringify(referralSellerR));
                                                if (referralSellerR.length > 0) {
                                                    var referralSellerRes = referralSellerR[0];
                                                    ReferralBuyer.create({
                                                        'referredBuyer': ctx.instance.buyerId,
                                                        'referredBy': referredById,
                                                        'referredByPhone': buyerR.phone,
                                                        'referredByName': buyerR.name,
                                                        'referredForSeller': ctx.instance.sellerId,
                                                        'sellerName': sellerRes.name,
                                                        'sellerPhone': sellerRes.phone,
                                                        'referralPoints': referralSellerRes.referralPoints,
                                                        'status': "InActive"
                                                    }, function (err, ReferralBuyerR) {
                                                        console.log('ReferralBuyer is created: ' + JSON.stringify(ReferralBuyerR));
                                                        next();
                                                    });

                                                } else {
                                                    console.log("subscription after save else 1");
                                                    next();
                                                }
                                            });
                                        } else {
                                            console.log("subscription after save else 2");
                                            next();
                                        }
                                    } else {
                                        console.log("subscription after save else 3");
                                        next();
                                    }
                                });

                            } else {
                                console.log("subscription after save else 4");
                                next();
                            }
                        });
                    } else {
                        console.log("referred by not registerd but referred....subscription after save else 5");
                        next();
                    }
                });
            } else {
                console.log("subscription after save else 6");
                next();
            }
            //next();
        } else {
            console.log("subscription after save else 7");
            next();
        }
    });


    //Subscription.listSubscriptions = function (buyerId, cb) {
    //    var merge = require('merge');
    //    var Business = server.models.Business;
    //    var Seller = server.models.Seller;
    //    Business.find({}, function (err, businesses) {
    //
    //        Subscription.find({where: {"buyerId": buyerId}}, function (err, subscriptions) {
    //
    //            if (err) {
    //                console.log("error: " + err);
    //            }
    //
    //            console.log("subs: " + subscriptions.length);
    //            if (subscriptions == null || subscriptions.length == 0) {
    //                var cResponse = [];
    //                for (bus in businesses) {
    //                    var bDetails = {};
    //                    var bName = businesses[bus].name;
    //                    bDetails['name'] = bName;
    //                    bDetails[bName] = [];
    //                    cResponse.push(bDetails);
    //                }
    //                cb(null, cResponse);
    //            } else {
    //                var ic = 0;
    //                var mSubscriptions = [];
    //                var rSubscriptions = [];
    //                for (ss in subscriptions) {
    //                    var subscription = subscriptions[ss];
    //
    //                    console.log(subscription.sellerId);
    //                    Seller.findById(subscription.sellerId, function (err, seller) {
    //
    //                        var fSubscription = subscriptions[ic];
    //                        console.log(seller);
    //                        console.log(JSON.stringify(fSubscription));
    //                        fSubscription["seller"] = seller;
    //                        console.log(fSubscription);
    //                        mSubscriptions.push(fSubscription);
    //                        ic++;
    //                        if (ic == subscriptions.length) {
    //
    //                            var customResponse = [];
    //
    //                            for (b in businesses) {
    //
    //                                var business = businesses[b];
    //
    //                                var allBusSubs = [];
    //                                var k = 0;
    //                                for (s in mSubscriptions) {
    //
    //                                    var subscription = mSubscriptions[s];
    //
    //                                    if (subscription.businessTypeId == business.id) {
    //                                        allBusSubs.push(subscription);
    //                                    }
    //
    //                                }
    //
    //                                var bName = business.name;
    //
    //                                var bDetails = {};
    //                                bDetails['name'] = bName;
    //                                bDetails[bName] = allBusSubs;
    //                                bDetails['count'] = allBusSubs.length;
    //                                customResponse.push(bDetails);
    //
    //                            }
    //
    //                            cb(null, customResponse);
    //                        }
    //
    //                    });
    //
    //                }
    //
    //            }
    //        });
    //    });
    //
    //};

    Subscription.listSubscriptions = function (buyerId, cb) {

        var Business = server.models.Business;
        var Subscription = server.models.Subscription;
        //newly added for giving subscription details of buyer enabled  businesses
        var BuyerBusiness = server.models.BuyerBusiness;
        var buyerDisabledBusinesses;
        BuyerBusiness.find({where: {"buyerId": buyerId, "status": "disabled"}}, function (err, buyerBusinesses) {
            console.log("buyerDisabledBusinesses: " + JSON.stringify(buyerBusinesses));
            buyerDisabledBusinesses = buyerBusinesses;

            Business.find({}, function (err, businesses) {
                console.log("buyerDisabledBusinesses11: " + buyerDisabledBusinesses.length);
                var bCount = 0;
                var customResponse = [];


                for (var b in businesses) {

                    console.log(b);
                    var business = businesses[b];
                    console.log(JSON.stringify(business));
                    console.log(buyerId + ": " + business.id);
                    //newly added
                    var businessDisabledcount = 0;
                    for (var i = 0; i < buyerDisabledBusinesses.length; i++) {
                        console.log("buyerDisabledBusinesses[i].businessTypeId: " + buyerDisabledBusinesses[i].businessTypeId);
                        console.log("businessDisabledcount: " + businessDisabledcount);
                        if (buyerDisabledBusinesses[i].businessTypeId == business.id) {
                            businessDisabledcount = businessDisabledcount + 1;
                            console.log("businessDisabledcount: " + businessDisabledcount);
                        }
                        if (businessDisabledcount >= 1) {
                            console.log("break out");
                            break;
                        }
                    }
                    //newly added
                    if (businessDisabledcount >= 1) {
                        console.log("ENTERED>>>>");
                        bCount++;
                        if (bCount == businesses.length) {
                            cb(null, customResponse);
                        }
                    } else {
                        //newly added
                        Subscription.find({
                            where: {
                                "buyerId": buyerId,
                                "subscriptionRequestStatus": "true",
                                "blockStatus": "false",
                                "businessTypeId": business.id.toString()
                            }
                        }, business, function (err, subscriptions) {

                            //console.log("subs: "+JSON.stringify(subscriptions));
                            //console.log("bc: "+bCount);
                            var businessR = this.business;
                            //var businessR = businesses[bCount];
                            //console.log(businessR);
                            var bName = businessR.name;
                            var businessImageUrl = businessR.imageUrl;
                            var businessTypeId = businessR.id;

                            var bDetails = {};
                            bDetails['name'] = bName;
                            bDetails['businessTypeId'] = businessTypeId;
                            bDetails['imageUrl'] = businessImageUrl;
                            bDetails[bName] = subscriptions;
                            bDetails['count'] = subscriptions.length;
                            customResponse.push(bDetails);

                            bCount++;

                            if (bCount == businesses.length) {

                                cb(null, customResponse);

                            }

                        }.bind({business: business}));

                    }

                }

            });
        });
        //newly added for giving subscription details of buyer enabled  businesses
        /*
         Business.find({}, function (err, businesses) {
         console.log("buyerDisabledBusinesses11: "+buyerDisabledBusinesses.length);
         var bCount = 0;
         var customResponse = [];



         for(var b in businesses) {

         console.log(b);
         var business = businesses[b];
         console.log(JSON.stringify(business));
         console.log(buyerId+": "+ business.id);
         //newly added
         var businessDisabledcount=0;
         /!*for(var i=0;i<buyerDisabledBusinesses.length;i++){
         console.log("buyerDisabledBusinesses[i].businessTypeId: "+buyerDisabledBusinesses[i].businessTypeId);
         console.log("businessDisabledcount: "+businessDisabledcount);
         if(buyerDisabledBusinesses[i].businessTypeId==business.id){
         businessDisabledcount=businessDisabledcount+1;
         console.log("businessDisabledcount: "+businessDisabledcount);
         }
         if (businessDisabledcount>=1) {
         console.log("break out");
         break;
         }
         }*!/
         //newly added
         if(businessDisabledcount>=1){
         console.log("ENTERED>>>>");
         bCount++;
         if(bCount == businesses.length) {
         cb(null, customResponse);
         }
         } else {
         //newly added
         Subscription.find({
         where: {
         "buyerId": buyerId,
         "subscriptionRequestStatus":false,
         "businessTypeId": business.id.toString()
         }
         }, business, function (err, subscriptions) {

         //console.log("subs: "+JSON.stringify(subscriptions));
         //console.log("bc: "+bCount);
         var businessR = this.business;
         //var businessR = businesses[bCount];
         //console.log(businessR);
         var bName = businessR.name;

         var bDetails = {};
         bDetails['name'] = bName;
         bDetails[bName] = subscriptions;
         bDetails['count'] = subscriptions.length;
         customResponse.push(bDetails);

         bCount++;

         if (bCount == businesses.length) {

         cb(null, customResponse);

         }

         }.bind({business: business}));

         }

         }

         });*/

    };

    Subscription.remoteMethod('listSubscriptions', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'subscriptions',
            type: 'array'
        },
        accepts: {arg: 'buyerId', type: 'string', http: {source: 'query'}},
        http: {
            path: '/list-subscriptions',
            verb: 'get'
        }
    });

    Subscription.customImplementation = function (cb) {

        Subscription.find({}, function (err, subscriptions) {

            for (var s in subscriptions) {

                var subscription = subscriptions[s];

                subscription.sbid = subscription.sellerId.toString(st) + subscription.buyerId.toString();

                //subscription.updateAttribute('sbid', subscription.sbid, function(err, sub){
                //
                //    console.log(sub);
                //
                //});
                subscription.status = "Active";
                subscription.updateAttribute('status', "Active", function (err, sub) {
                    console.log(sub);
                });
                //subscription.save();

            }

        });

    }

    Subscription.remoteMethod('customImplementation', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'subscriptions',
            type: 'array'
        },
        accepts: {http: {source: 'query'}},
        http: {
            path: '/customImplementation',
            verb: 'get'
        }
    });

    Subscription.manageLoyalty = function (buyerId, operationType, value, cb) {

        var cb = cb;

        var Buyer = server.models.Buyer;

        Buyer.findById(buyerId, function (err, buyer) {

            if (err) {
                cb(err, null);
            } else {
                var loyalty = buyer.loyalty;

                var loyaltyPoints = loyalty.totalPoints;

                if (operationType == 'Add') {

                    loyaltyPoints = parseInt(loyaltyPoints) + parseInt(value);

                } else if (operationType == 'Remove') {

                    loyaltyPoints = parseInt(loyaltyPoints) - parseInt(value);

                }

                loyalty.totalPoints = loyaltyPoints;

                buyer.updateAttributes({"loyalty": loyalty}, function (err, buyerUpdate) {

                    if (err) {
                        cb(err, null);
                    } else {
                        cb(null, buyerUpdate);
                    }

                });
            }

        });

    };

    Subscription.remoteMethod('manageLoyalty', {
        description: "please update payment status",
        returns: {
            arg: 'loyalty',
            type: 'array'
        },
        accepts: [{arg: 'buyerId', type: 'string', http: {source: 'query'}},
            {arg: 'operationType', type: 'string', http: {source: 'query'}},
            {arg: 'value', type: 'string', http: {source: 'query'}}
        ],
        http: {
            path: '/manageLoyalty',
            verb: 'get'
        }
    });

    Subscription.requestPriceDisplay = function (subscriptionId, cb) {

        var cb = cb;

        var Subscription = server.models.Subscription;
        var SellerServices = server.models.SellerServices;
        var GCM = server.models.GCM;
        var APN = server.models.APN;
        var Notification = server.models.Notification;

        Subscription.findById(subscriptionId, function (err, subscription) {

            if (subscription) {
                console.log("subscription found....");

                //var serviceList = subscription.sellerservicelist;
                //var serviceListN = [];
                //
                //var pdNew;
                //
                //for(var sl in serviceList){
                //
                //    var eService = serviceList[sl]
                //    if(eService.name == "Price Display"){
                //
                //        //serviceList.pull(sl);
                //        break;
                //
                //    } else {
                //        serviceListN.push(eService);
                //    }
                //
                //}
                //
                //pdNew = {
                //    "name": "Price Display",
                //    "required": "requested",
                //    "id": "569cbda89d2649b85d5e8bdb"
                //};
                //
                //serviceListN.push(pdNew);

                var notificationDetails = {
                    "from": subscription.buyerId,
                    "buyerName": subscription["buyer"].name,
                    "buyerContact": subscription["buyer"].phone,
                    "fromType": "buyer",
                    "to": subscription.sellerId,
                    "toType": "seller",
                    "message": "Please Approve Price Request",
                    "type": "Subscription",
                    "operation": "Price_Request",
                    "status": "new",
                    "typeId": subscription.id
                };
                Notification.create(notificationDetails, function (err, notificationR) {
                    console.log("notificationR: " + JSON.stringify(notificationR));
                });

                subscription.updateAttributes({"priceRequestStatus": true}, function (err, subscriptionU) {

                    var seller_id = subscription.sellerId;


                    var seller_gcm_id;
                    try {
                        if (subscription["seller"].gcm_id != "NA") {
                            seller_gcm_id = subscription["seller"].gcm_id;
                        }
                    } catch (e) {
                        console.log("Error in subscription priceRequestStatus: " + JSON.stringify(e));
                    }
                    try {
                        if (subscription["seller"].devicetoken != "NA") {
                            seller_gcm_id = subscription["seller"].devicetoken;
                        }
                    } catch (e) {
                        console.log("Error in subscription priceRequestStatus: " + JSON.stringify(e));
                    }
                    var gcmData = {
                        "registration_ids": [
                            seller_gcm_id
                        ],
                        "data": {
                            "message": "Please Approve Price Request",
                            "type": "Subscription",
                            "operation": "Price_Request",
                            "buyerName": subscription["buyer"].name,
                            "buyerContact": subscription["buyer"].phone,
                            "buyerId": subscription.buyerId,
                            "fromType": "buyer",
                            "sellerId": subscription.sellerId,
                            "toType": "seller",
                            "id": subscription.id
                        }
                    };
                    try {
                        if (subscription["seller"].gcm_id != "NA") {
                            GCM.create(gcmData, function (err, gcm) {
                            });
                        }
                    } catch (e) {
                        console.log("Error in subscription Price Request: " + JSON.stringify(e));
                    }
                    try {
                        if (subscription["seller"].devicetoken != "NA") {
                            APN.create(gcmData, function (err, apn) {
                            });
                        }
                    } catch (e) {
                        console.log("Error in subscription Price Request: " + JSON.stringify(e));
                    }
                    cb(null, subscriptionU);

                });


            } else {
                cb(new Error("subscription not found"), null);
            }

        });

    };

    Subscription.remoteMethod('requestPriceDisplay', {
        description: "please update payment status",
        returns: {
            arg: 'subscription',
            type: 'array'
        },
        accepts: [{arg: 'subscriptionId', type: 'string', http: {source: 'query'}}
        ],
        http: {
            path: '/requestPriceDisplay',
            verb: 'get'
        }
    });

    Subscription.confirmPriceDisplayRequest = function (subscriptionId, status, cb) {

        var cb = cb;
        var Notification = server.models.Notification;
        var Subscription = server.models.Subscription;
        var SellerServices = server.models.SellerServices;
        var GCM = server.models.GCM;
        var APN = server.models.APN;

        Subscription.findById(subscriptionId, function (err, subscription) {

            if (subscription) {
                var serviceList = subscription.sellerservicelist;
                var serviceListN = [];
                var priceRequestStatus = subscription.priceRequestStatus;
                if (subscription.priceRequestStatus == 1 || subscription.priceRequestStatus == "1") {
                    priceRequestStatus = true;
                } else if (subscription.priceRequestStatus == 0 || subscription.priceRequestStatus == "0") {
                    priceRequestStatus = false;
                }
                var pdNew;

                for (var sl in serviceList) {

                    var eService = serviceList[sl]
                    if (eService.name == "Price Display") {

                        //serviceList.pull(sl);
                        //break;

                    } else {
                        serviceListN.push(eService);
                    }

                }
                // newly added change-- updated required as true instead of status
                if (status) {
                    pdNew = {
                        "name": "Price Display",
                        "required": true,
                        "id": "569cbda89d2649b85d5e8bdb"
                    };

                    serviceListN.push(pdNew);
                }


                subscription.updateAttributes({
                    "sellerservicelist": serviceListN,
                    "priceRequestStatus": false
                }, function (err, subscriptionU) {

                    if (priceRequestStatus) {

                        if (status) {

                            /*Notification.find({
                             "where": {
                             "from": subscription.buyerId, "to": subscription.sellerId, "type": "Subscription",
                             "operation": "Price_Request", "status": "new"
                             }
                             }, function (err, NotificationR) {
                             if (NotificationR.length > 0) {
                             console.log("NotificationRLength: " + NotificationR.length);
                             var NotificationR = NotificationR[0];
                             NotificationR.updateAttributes({"status": "approved"}, function (err, NotificationU) {
                             console.log("notification approved det: " + JSON.stringify(NotificationU));
                             });
                             }
                             });*/

                            var notificationDetails = {
                                "from": subscription.sellerId,
                                "fromType": "seller",
                                "to": subscription.buyerId,
                                "toType": "buyer",
                                "status": "new",
                                "message": "your price request is approved",
                                "type": "Subscription",
                                "operation": "Price_Request_Approval",
                                "buyerName": subscription["buyer"].name,
                                "sellerName": subscription["seller"].name,
                                "sellerContact": subscription["seller"].phone,
                                "buyerContact": subscription["buyer"].phone,
                                "sellerBusinessname": subscription["seller"].businessName,
                                "businessCatgName": subscription["seller"].businessTypeName,
                                "businessTypeId": subscription["seller"].businessTypeId,
                                "sellergcmid": subscription["seller"].gcm_id,
                                "deviceToken": subscription["seller"].devicetoken,
                                "pricerequestdisplaystatus": "true",
                                "subscriptionId": subscription.id,
                                "sellerrating": subscription["seller"].rating.rating,
                                "typeId": subscription.id
                            };
                            Notification.create(notificationDetails, function (err, notificationRes) {
                            });
                            //new

                            var buyer_id = subscription.buyerId;

                            var buyer_gcm_id;
                            try {
                                if (subscription["buyer"].gcm_id != "NA") {
                                    buyer_gcm_id = subscription["buyer"].gcm_id;
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is approved: " + JSON.stringify(e));
                            }
                            try {
                                if (subscription["buyer"].devicetoken != "NA") {
                                    buyer_gcm_id = subscription["buyer"].devicetoken;
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is approved: " + JSON.stringify(e));
                            }


                            var gcmData = {
                                "registration_ids": [
                                    buyer_gcm_id
                                ],
                                "data": {
                                    "message": "your price request is approved",
                                    "type": "Subscription",
                                    "operation": "Price_Request_Approval",
                                    "buyerName": subscription["buyer"].name,
                                    "sellerId": subscription.sellerId,
                                    "buyerId": subscription.buyerId,
                                    "sellerName": subscription["seller"].name,
                                    "sellerContact": subscription["seller"].phone,
                                    "sellerBusinessname": subscription["seller"].businessName,
                                    "businessCatgName": subscription["seller"].businessTypeName,
                                    "businessTypeId": subscription["seller"].businessTypeId,
                                    "sellergcmid": subscription["seller"].gcm_id,
                                    "deviceToken": subscription["seller"].devicetoken,
                                    "pricerequestdisplaystatus": "true",
                                    "subscriptionId": subscription.id,
                                    "sellerrating": subscription["seller"].rating.rating,
                                    "id": subscription.id
                                }
                            };
                            try {
                                if (subscription["buyer"].gcm_id /*!= "NA"*/) {
                                    GCM.create(gcmData, function (err, gcm) {
                                    });
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is approved: " + JSON.stringify(e));
                            }
                            try {
                                if (subscription["buyer"].devicetoken /*!= "NA"*/) {
                                    APN.create(gcmData, function (err, apn) {
                                    });
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is approved: " + JSON.stringify(e));
                            }

                        } else {
                            /*Notification.find({
                             "where": {
                             "from": subscription.buyerId, "to": subscription.sellerId, "type": "Subscription",
                             "operation": "Price_Request", "status": "new"
                             }
                             }, function (err, NotificationR) {
                             if (NotificationR.length > 0) {
                             console.log("NotificationRLength: " + NotificationR.length);
                             var NotificationR = NotificationR[0];
                             NotificationR.updateAttributes({"status": "denied"}, function (err, NotificationU) {
                             console.log("notification approved det: " + JSON.stringify(NotificationU));
                             });
                             }
                             });*/


                            var notificationDetails = {
                                "from": subscription.sellerId,
                                "fromType": "seller",
                                "to": subscription.buyerId,
                                "toType": "buyer",
                                "status": "new",
                                "message": "your price request is denied",
                                "type": "Subscription",
                                "operation": "Price_Request_Denied",
                                "buyerName": subscription["buyer"].name,
                                "sellerName": subscription["seller"].name,
                                "sellerContact": subscription["seller"].phone,
                                "buyerContact": subscription["buyer"].phone,
                                "sellerBusinessname": subscription["seller"].businessName,
                                "businessCatgName": subscription["seller"].businessTypeName,
                                "businessTypeId": subscription["seller"].businessTypeId,
                                "sellergcmid": subscription["seller"].gcm_id,
                                "deviceToken": subscription["seller"].devicetoken,
                                "pricerequestdisplaystatus": "false",
                                "subscriptionId": subscription.id,
                                "sellerrating": subscription["seller"].rating.rating,
                                "typeId": subscription.id
                            };
                            Notification.create(notificationDetails, function (err, notificationRes) {
                            });

                            var buyer_id = subscription.buyerId;

                            var buyer_gcm_id;
                            try {
                                if (subscription["buyer"].gcm_id != "NA") {
                                    buyer_gcm_id = subscription["buyer"].gcm_id;
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is denied: " + JSON.stringify(e));
                            }
                            try {
                                if (subscription["buyer"].devicetoken != "NA") {
                                    buyer_gcm_id = subscription["buyer"].devicetoken;
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is denied: " + JSON.stringify(e));
                            }
                            var gcmData = {
                                "registration_ids": [
                                    buyer_gcm_id
                                ],
                                "data": {
                                    "message": "your price request is denied",
                                    "type": "Subscription",
                                    "operation": "Price_Request_Denied",
                                    "sellerId": subscription.sellerId,
                                    "buyerId": subscription.buyerId,
                                    "buyerName": subscription["buyer"].name,
                                    "sellerName": subscription["seller"].name,
                                    "sellerContact": subscription["seller"].phone,
                                    "sellerBusinessname": subscription["seller"].businessName,
                                    "businessCatgName": subscription["seller"].businessTypeName,
                                    "businessTypeId": subscription["seller"].businessTypeId,
                                    "sellergcmid": subscription["seller"].gcm_id,
                                    "deviceToken": subscription["seller"].devicetoken,
                                    "pricerequestdisplaystatus": "false",
                                    "subscriptionId": subscription.id,
                                    "sellerrating": subscription["seller"].rating.rating,
                                    "id": subscription.id
                                }
                            };
                            try {
                                if (subscription["buyer"].gcm_id != "NA") {
                                    GCM.create(gcmData, function (err, gcm) {
                                    });
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is denied: " + JSON.stringify(e));
                            }
                            try {
                                if (subscription["buyer"].devicetoken != "NA") {
                                    APN.create(gcmData, function (err, apn) {
                                    });
                                }
                            } catch (e) {
                                console.log("Error in subscription your price request is denied: " + JSON.stringify(e));
                            }
                        }

                    }

                    cb(null, subscriptionU);

                });
            } else {
                cb(new Error("subscription not found"), null);
            }

        });

    };

    Subscription.remoteMethod('confirmPriceDisplayRequest', {
        description: "confirm payment status",
        returns: {
            arg: 'subscription',
            type: 'array'
        },
        accepts: [{arg: 'subscriptionId', type: 'string', http: {source: 'query'}},
            {arg: 'status', type: 'boolean', http: {source: 'query'}}
        ],
        http: {
            path: '/confirmPriceDisplayRequest',
            verb: 'get'
        }
    });

    Subscription.ci = function (buyerId, operationType, value, cb) {

        var cb = cb;

        var Buyer = server.models.Buyer;
        var Loyalty = server.models.Loyalty;

        Buyer.find({}, function (err, buyers) {

            for (var b in buyers) {

                var buyer = buyers[b];
                buyer.updateAttributes({
                    "loyalty": {
                        "totalPoints": 0,
                        "points": 0,
                        "amount": 0
                    }
                }, function (err, buyerNext) {

                });

            }

        });

    };

    Subscription.remoteMethod('ci', {
        description: "please update payment status",
        returns: {
            arg: 'loyalty',
            type: 'array'
        },
        accepts: [{arg: 'buyerId', type: 'string', http: {source: 'query'}},
            {arg: 'operationType', type: 'string', http: {source: 'query'}},
            {arg: 'value', type: 'string', http: {source: 'query'}}
        ],
        http: {
            path: '/ci',
            verb: 'get'
        }
    });

    Subscription.confirmSubScriptionRequest = function (subscriptionId, status, blockStatus, cb) {

        var cb = cb;

        var Buyer = server.models.Buyer;
        var Subscription = server.models.Subscription;
        var GCM = server.models.GCM;
        var APN = server.models.APN;
        var Notification = server.models.Notification;

        Subscription.findById(subscriptionId, function (err, subscription) {
            //var subscription = subscriptionR[0];

            if (subscription) {

                if (status == "true" && blockStatus == "false") {

                    /*
                     Notification.find({
                     "where": {
                     "from": ctx.req.body.buyerId, "to": ctx.req.body.sellerId, "type": "Subscription",
                     "operation": "Subscription_Request"
                     }
                     }, function (err, NotificationR) {
                     //var NotificationRes=NotificationR[0];
                     if (NotificationR.length == 0) {
                     console.log("NotificationRLength: " + NotificationR.length);
                     var notificationDetails = {
                     "from": ctx.req.body.buyerId,
                     "fromType": "buyer",
                     "to": ctx.req.body.sellerId,
                     "toType": "seller",
                     "message": "Please Approve Subscription Request",
                     "type": "Subscription",
                     "operation": "Subscription_Request",
                     "status": "new",
                     "typeId": subscriptionId
                     };
                     Notification.create(notificationDetails, function (err, notificationR) {
                     console.log("Subscription_Request notificationR: " + JSON.stringify(notificationR));
                     });

                     }
                     });*/
                    subscription.updateAttributes({"subscriptionRequestStatus": "true"}, function (err, subscriptionU) {

                        /*Notification.find({
                         "where": {
                         "from": subscription.buyerId, "to": subscription.sellerId, "type": "Subscription",
                         "operation": "Subscription_Request"
                         }
                         }, function (err, NotificationR) {
                         if (NotificationR.length > 0) {
                         console.log("NotificationRLength: " + NotificationR.length);
                         var NotificationR = NotificationR[0];
                         NotificationR.updateAttributes({"status": "approved"}, function (err, NotificationU) {
                         console.log("Subscription_Request notification approved det: " + JSON.stringify(NotificationU));
                         });
                         }
                         });*/

                        var notificationDetails = {
                            "from": subscription.sellerId,
                            "fromType": "seller",
                            "to": subscription.buyerId,
                            "toType": "buyer",
                            "status": "new",
                            "message": "Your subscription Request Approved",
                            "type": "Subscription",
                            "operation": "Subscription_Request_Approval",
                            "buyerName": subscription["buyer"].name,
                            "sellerName": subscription["seller"].name,
                            "sellerContact": subscription["seller"].phone,
                            "buyerContact": subscription["buyer"].phone,
                            "sellerBusinessname": subscription["seller"].businessName,
                            "businessCatgName": subscription["seller"].businessTypeName,
                            "businessTypeId": subscription["seller"].businessTypeId,
                            "sellergcmid": subscription["seller"].gcm_id,
                            "deviceToken": subscription["seller"].devicetoken,
                            "subscriptionId": subscription.id,
                            "sellerrating": subscription["seller"].rating.rating,
                            "typeId": subscription.id
                        };
                        Notification.create(notificationDetails, function (err, notificationRes) {
                        });


                        var buyer_gcm_id;
                        var buyer_device_token_id;
                        try {
                            if (subscription["buyer"].gcm_id != "NA") {
                                buyer_gcm_id = subscription["buyer"].gcm_id;
                            }
                        } catch (e) {
                            console.log("Error in subscription subscriptionRequestStatus: " + JSON.stringify(e));
                        }
                        try {
                            if (subscription["buyer"].devicetoken != "NA") {
                                buyer_device_token_id = subscription["buyer"].devicetoken;
                            }
                        } catch (e) {
                            console.log("Error in subscription subscriptionRequestStatus: " + JSON.stringify(e));
                        }
                        console.log("buyer_gcm_id: " + buyer_gcm_id);
                        var gcmData = {
                            "registration_ids": [
                                buyer_gcm_id, buyer_device_token_id
                            ],
                            "data": {
                                "message": "Your subscription Request Approved",
                                "type": "Subscription",
                                "operation": "Subscription_Request_Approval",
                                "subscriptionId": subscription.id,
                                "sellerrating": subscription["seller"].rating.rating,
                                "buyerName": subscription["buyer"].name,
                                "sellerName": subscription["seller"].name,
                                "sellerContact": subscription["seller"].phone,
                                "buyerContact": subscription["buyer"].phone,
                                "sellerBusinessname": subscription["seller"].businessName,
                                "businessCatgName": subscription["seller"].businessTypeName,
                                "businessTypeId": subscription["seller"].businessTypeId,
                                "sellergcmid": subscription["seller"].gcm_id,
                                "deviceToken": subscription["seller"].devicetoken,
                                "id": subscription.id
                            }
                        };

                        try {
                            if (subscription["buyer"].gcm_id != "NA") {
                                GCM.create(gcmData, function (err, gcm) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in subscription android Request: " + JSON.stringify(e));
                        }
                        try {
                            if (subscription["buyer"].devicetoken != "NA") {
                                APN.create(gcmData, function (err, apn) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in subscription ios Request: " + JSON.stringify(e));
                        }
                        cb(null, subscriptionU);

                    });

                } else if (status == "false" && blockStatus == "true") {

                    subscription.updateAttributes({
                        "subscriptionRequestStatus": "false",
                        "blockStatus": "true"
                    }, function (err, subscriptionU) {

                        /*Notification.find({
                         "where": {
                         "from": subscription.buyerId, "to": subscription.sellerId, "type": "Subscription",
                         "operation": "Subscription_Request"
                         }
                         }, function (err, NotificationR) {
                         if (NotificationR.length > 0) {
                         console.log("NotificationRLength: " + NotificationR.length);
                         var NotificationR = NotificationR[0];
                         NotificationR.updateAttributes({"status": "denied"}, function (err, NotificationU) {
                         console.log("Subscription_Request notification denied det: " + JSON.stringify(NotificationU));
                         });
                         }
                         });*/


                        var notificationDetails = {
                            "from": subscription.sellerId,
                            "fromType": "seller",
                            "to": subscription.buyerId,
                            "toType": "buyer",
                            "status": "new",
                            "message": "Your subscription Request Denied",
                            "type": "Subscription",
                            "operation": "Subscription_Request_Denied",
                            "buyerName": subscription["buyer"].name,
                            "sellerName": subscription["seller"].name,
                            "sellerContact": subscription["seller"].phone,
                            "buyerContact": subscription["buyer"].phone,
                            "sellerBusinessname": subscription["seller"].businessName,
                            "businessCatgName": subscription["seller"].businessTypeName,
                            "businessTypeId": subscription["seller"].businessTypeId,
                            "sellergcmid": subscription["seller"].gcm_id,
                            "deviceToken": subscription["seller"].devicetoken,
                            "typeId": subscription.id
                        };
                        Notification.create(notificationDetails, function (err, notificationRes) {
                        });


                        var buyer_gcm_id;
                        var buyer_device_token_id;
                        try {
                            if (subscription["buyer"].gcm_id != "NA") {
                                buyer_gcm_id = subscription["buyer"].gcm_id;
                            }
                        } catch (e) {
                            console.log("Error in subscription subscriptionRequestStatus: " + JSON.stringify(e));
                        }
                        try {
                            if (subscription["buyer"].devicetoken != "NA") {
                                buyer_device_token_id = subscription["buyer"].devicetoken;
                            }
                        } catch (e) {
                            console.log("Error in subscription subscriptionRequestStatus: " + JSON.stringify(e));
                        }
                        console.log("buyer_gcm_id: " + buyer_gcm_id);
                        var gcmData = {
                            "registration_ids": [
                                buyer_gcm_id, buyer_device_token_id
                            ],
                            "data": {
                                "message": "Your subscription Request Denied",
                                "type": "Subscription",
                                "operation": "Subscription_Request_Denied",
                                "subscriptionId": subscription.id,
                                "sellerrating": subscription["seller"].rating.rating,
                                "buyerName": subscription["buyer"].name,
                                "sellerName": subscription["seller"].name,
                                "sellerContact": subscription["seller"].phone,
                                "buyerContact": subscription["buyer"].phone,
                                "sellerBusinessname": subscription["seller"].businessName,
                                "businessCatgName": subscription["seller"].businessTypeName,
                                "businessTypeId": subscription["seller"].businessTypeId,
                                "sellergcmid": subscription["seller"].gcm_id,
                                "deviceToken": subscription["seller"].devicetoken,
                                "id": subscription.id
                            }
                        };

                        try {
                            if (subscription["buyer"].gcm_id != "NA") {
                                GCM.create(gcmData, function (err, gcm) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in subscription android Request: " + JSON.stringify(e));
                        }
                        try {
                            if (subscription["buyer"].devicetoken != "NA") {
                                APN.create(gcmData, function (err, apn) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in subscription ios Request: " + JSON.stringify(e));
                        }
                        cb(null, subscriptionU);

                    });

                } else {
                    cb(new Error("subscription not found"), null);
                }

            } else {
                cb(new Error("subscription not found"), null);
            }

        });


    };

    Subscription.remoteMethod('confirmSubScriptionRequest', {
        description: "confirm subscription request",
        returns: {
            arg: 'subscriptionRequestStatus',
            type: 'string'
        },
        accepts: [{arg: 'subscriptionId', type: 'string', http: {source: 'query'}},
            {arg: 'status', type: 'string', http: {source: 'query'}}, {
                arg: 'blockStatus',
                type: 'string',
                http: {source: 'query'}
            }
        ],
        http: {
            path: '/confirmSubScriptionRequest',
            verb: 'get'
        }
    });
    /*

     Subscription.deleteSellerRecord = function (sellerMobileNo,cb) {
     var Subscription = server.models.Subscription;
     var Seller = server.models.Seller;
     console.log("sellerMobileNo: "+sellerMobileNo);
     Seller.findOne({"where":{"phone":sellerMobileNo}},function(err,sellerDet){
     console.log("sellerId: "+sellerDet.id);
     var sellerId=sellerDet.id;

     Subscription.find({"where":{"sellerId":sellerId}},function(err,subscriptionDetails){
     console.log("subscriptionDetails data length: "+subscriptionDetails.length);
     var count=0;
     for(var i=0;i<subscriptionDetails.length;i++){
     subscriptionSellerId=subscriptionDetails[i].id;
     console.log("subscriptionSellerId: "+subscriptionSellerId);
     Subscription.deleteById(subscriptionSellerId, function(err, sellers){

     });
     count=count+1;

     }
     console.log("count: "+count);
     if(count==subscriptionDetails.length){
     if(err){
     cb(err,null);
     }else{
     console.log("sellerDet: "+JSON.stringify(sellerDet));
     cb(null,sellerDet);
     }
     }


     });



     });



     }
     */
    /*
     Subscription.remoteMethod(
     'deleteSellerRecord',
     {
     accepts:[{arg:'sellerMobileNo',type:'string',http: {source: 'query'}}],
     returns:[{arg:'sellerDet',type:'array'}],
     http: {
     path: '/deleteSellerRecord',
     verb: 'get'
     }
     }
     );*/


    //  buyerDefaultDataAppending
    Subscription.subscriptionDefaultDataAppending = function (searchData, cb) {
        var Subscription = server.models.Subscription;

        var updateCount = 0;
        Subscription.find({}, function (err, subscriptions) {
            for (var i = 0; i < subscriptions.length; i++) {
                var subscriptionDet = subscriptions[i];
                subscriptionDet.updateAttributes({
                    "blockStatus": "false",
                    "subscriptionRequestStatus": "true",
                    "SNo": i + 1
                }, function (err, subscriptionInfoR) {
                    updateCount = updateCount + 1;
                    console.log("updateCount: " + updateCount);
                    if (subscriptions.length == updateCount) {
                        console.log("response given......");
                        cb(null, "completed");
                    }
                });
            }
            console.log("subscriptionsCount: " + subscriptions.length);
        });
    }

    Subscription.remoteMethod(
        'subscriptionDefaultDataAppending',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/subscriptionDefaultDataAppending',
                verb: 'get'
            }
        }
    );


    //  notifyAboutUploadingFrames
    Subscription.notifyAboutUploadingFrames = function (sellerId, buyerId, cb) {
        var Buyer = server.models.Buyer;
        var Seller = server.models.Seller;
        var GCM = server.models.GCM;
        var APN = server.models.APN;
        var SMS = server.models.SMS;
        var Notification = server.models.Notification;
        Seller.findOne({"where": {id: sellerId}}, function (err, seller) {
            Buyer.findOne({"where": {id: buyerId}}, function (err, buyer) {

                if (err) {
                    cb(err, null);
                } else if (seller != undefined) {
                    console.log('Else');
/*
                    Notification.find({
                        "where": {
                            "to": sellerId, "type": "NofitySellerAboutUploadFrames",
                            "operation": "Frames_Upload_Request", "status": "new"
                        }
                    }, function (err, NotificationR) {*/
                        //var NotificationRes=NotificationR[0];
                        //console.log("NotificationR: " + JSON.stringify(NotificationR));
                        //if (NotificationR.length == 0) {
                        // console.log("NotificationRLength: " + NotificationR.length);
                        var notificationDetails = {
                            "from": buyerId,
                            "fromType": "buyer",
                            "buyerName": buyer.name,
                            "to": sellerId,
                            "toType": "seller",
                            "buyerContact": buyer.phone,
                            "sellerContact": seller.phone,
                            "sellerName": seller.name,
                            "sellergcmid": seller.gcm_id,
                            "deviceToken": seller.devicetoken,
                            "message": "Please Upload New Frames",
                            "type": "NofitySellerAboutUploadFrames",
                            "operation": "Frames_Upload_Request",
                            "notifiedNo": NotificationR.length,
                            "status": "new",
                            "typeId": sellerId
                        };
                        Notification.create(notificationDetails, function (err, notificationRes) {
                            console.log("NofitySellerAboutUploadFrames notificationR: ");
                            var seller_gcm_id;

                            if (seller.gcm_id != "NA") {
                                seller_gcm_id = seller.gcm_id;
                            } else if (seller.devicetoken != "NA") {
                                seller_gcm_id = seller.devicetoken;
                            }
                            console.log("sellergcmid: " + seller_gcm_id);
                            // newly added code for sending notification regarding New Frames update
                            var gcmData = {
                                "registration_ids": [
                                    seller_gcm_id
                                ],
                                "data": {
                                    "message": "Please Upload New Frames",
                                    "type": "NofityAboutUploadFrames",
                                    "operation": "Frames_Upload_Request",
                                    "buyerName": buyer.name,
                                    "buyerContact": buyer.phone,
                                    "sellerContact": seller.phone,
                                    "sellerName": seller.name,
                                    "notifiedNo": NotificationR.length,
                                    "sellerId": sellerId,
                                    "buyerId": buyerId,
                                    "id": sellerId
                                }
                            };

                            if (seller.gcm_id != "NA") {
                                GCM.create(gcmData, function (err, gcm) {
                                });
                            } else if (seller.devicetoken != "NA") {
                                APN.create(gcmData, function (err, apn) {
                                });
                            }
                            var sms="Hi I am "+buyer.name+" trying to see images from "+seller.businessName+" on VYULI app. iOS http://apple.co/29eDGxZ for Android http://bit.ly/29bVAEy";
                            var phone=seller.phone;
                            SMS.create({destination:phone, message: sms}, function (err, sms) {});
                            cb(null, "completed");
                        });
                        //}
                    // });
                } else {
                    cb(null, "buyer details not found");
                }
            });
        });
    };

    Subscription.remoteMethod(
        'notifyAboutUploadingFrames',
        {
            accepts: [
                {arg: 'sellerId', type: 'string', http: {source: 'query'}},
                {arg: 'buyerId', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/notifyAboutUploadingFrames',
                verb: 'get'
            }
        }
    );


    //  updatingNotificationsStatus
    Subscription.updatingNotificationsStatus = function (notificationDetails, cb) {
        var Buyer = server.models.Buyer;
        var Seller = server.models.Seller;
        var Notification = server.models.Notification;
        var updateCount = 0;
        console.log("notificationDet: " + JSON.stringify(notificationDetails));
        for (var i = 0; i < notificationDetails.length; i++) {
            var notificationDet = notificationDetails[i];
            console.log("notificationDet: " + JSON.stringify(notificationDet));
            Notification.findById(notificationDet, function (err, notificationR) {
                if (notificationR.operation == "Frames_Upload_Request" && notificationR.status == "new") {
                    notificationR.updateAttributes({"status": "visited"}, function (err, NotificationU) {
                        console.log("notification approved det: " + JSON.stringify(NotificationU));
                    });
                }
                else if (notificationR.operation == "Frame_Update" && notificationR.status == "new") {
                    notificationR.updateAttributes({"status": "visited"}, function (err, NotificationU) {
                        console.log("approvedTestFrame_Update det: " + JSON.stringify(NotificationU));
                    });
                }
                else if (notificationR.operation == "Subscription_Request" && notificationR.status == "new") {
                    notificationR.updateAttributes({"status": "visited"}, function (err, NotificationU) {
                        console.log("approvedTestFrame_Update det: " + JSON.stringify(NotificationU));
                    });
                }
                else if (notificationR.operation == "Price_Request" && notificationR.status == "new") {
                    notificationR.updateAttributes({"status": "visited"}, function (err, NotificationU) {
                        console.log("approvedTestFrame_Update det: " + JSON.stringify(NotificationU));
                    });
                }
                /*                else if (notificationR.operation == "Price_Request" && notificationR.status == "new") {
                 var statusForPriceReq="denied";
                 if(statusForPriceRequest=="true"){
                 statusForPriceReq="approved";
                 }
                 notificationR.updateAttributes({"status": statusForPriceReq}, function (err, NotificationU) {
                 console.log("approvedTestPrice_Request det: " + JSON.stringify(NotificationU));
                 //});
                 Subscription.findById(NotificationU.typeId, function (err, subscription) {

                 if (subscription) {
                 var serviceList = subscription.sellerservicelist;
                 var status=false;
                 if(statusForPriceRequest=="true"){
                 status=true;
                 }

                 var serviceListN = [];
                 var priceRequestStatus = subscription.priceRequestStatus;
                 if (subscription.priceRequestStatus == 1 || subscription.priceRequestStatus == "1") {
                 priceRequestStatus = true;
                 } else if (subscription.priceRequestStatus == 0 || subscription.priceRequestStatus == "0") {
                 priceRequestStatus = false;
                 }
                 var pdNew;

                 for (var sl in serviceList) {

                 var eService = serviceList[sl];
                 if (eService.name == "Price Display") {

                 //serviceList.pull(sl);
                 //break;

                 } else {
                 serviceListN.push(eService);
                 }

                 }
                 // newly added change-- updated required as true instead of status
                 if (status) {
                 pdNew = {
                 "name": "Price Display",
                 "required": true,
                 "id": "569cbda89d2649b85d5e8bdb"
                 };

                 serviceListN.push(pdNew);
                 }


                 subscription.updateAttributes({
                 "sellerservicelist": serviceListN,
                 "priceRequestStatus": false
                 }, function (err, subscriptionU) {

                 if (priceRequestStatus) {

                 if (status) {

                 /!*Notification.find({
                 "where": {
                 "from": subscription.buyerId, "to": subscription.sellerId, "type": "Subscription",
                 "operation": "Price_Request", "status": "new"
                 }
                 }, function (err, NotificationR) {
                 if (NotificationR.length > 0) {
                 console.log("NotificationRLength: " + NotificationR.length);
                 var NotificationR = NotificationR[0];
                 NotificationR.updateAttributes({"status": "approved"}, function (err, NotificationU) {
                 console.log("notification approved det: " + JSON.stringify(NotificationU));
                 });
                 }
                 });*!/
                 var buyer_id = subscription.buyerId;

                 var buyer_gcm_id;
                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 buyer_gcm_id = subscription["buyer"].gcm_id;
                 }
                 } catch (e) {
                 console.log("Error in assigning gcmid for price request is approved: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 buyer_gcm_id = subscription["buyer"].devicetoken;
                 }
                 } catch (e) {
                 console.log("Error in assigning devicetoken for price request is approved: " + JSON.stringify(e));
                 }


                 var gcmData = {
                 "registration_ids": [
                 buyer_gcm_id
                 ],
                 "data": {
                 "message": "your price request is approved",
                 "type": "Subscription",
                 "operation": "Price_Request_Approval",
                 "buyerName": subscription["buyer"].name,
                 "sellerId": subscription.sellerId,
                 "buyerId": subscription.buyerId,
                 "sellerName": subscription["seller"].name,
                 "sellerContact": subscription["seller"].phone,
                 "sellerBusinessname": subscription["seller"].businessName,
                 "businessCatgName": subscription["seller"].businessTypeName,
                 "businessTypeId": subscription["seller"].businessTypeId,
                 "sellergcmid": subscription["seller"].gcm_id,
                 "deviceToken": subscription["seller"].devicetoken,
                 "id": subscription.id
                 }
                 };
                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 GCM.create(gcmData, function (err, gcm) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating gcm your price request is approved: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 APN.create(gcmData, function (err, apn) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating devicetoken your price request is approved: " + JSON.stringify(e));
                 }

                 } else {
                 /!*Notification.find({
                 "where": {
                 "from": subscription.buyerId, "to": subscription.sellerId, "type": "Subscription",
                 "operation": "Price_Request", "status": "new"
                 }
                 }, function (err, NotificationR) {
                 if (NotificationR.length > 0) {
                 console.log("NotificationRLength: " + NotificationR.length);
                 var NotificationR = NotificationR[0];
                 NotificationR.updateAttributes({"status": "denied"}, function (err, NotificationU) {
                 console.log("notification approved det: " + JSON.stringify(NotificationU));
                 });
                 }
                 });*!/
                 var buyer_id = subscription.buyerId;

                 var buyer_gcm_id;
                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 buyer_gcm_id = subscription["buyer"].gcm_id;
                 }
                 } catch (e) {
                 console.log("Error in assigning gcmid  price request is denied: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 buyer_gcm_id = subscription["buyer"].devicetoken;
                 }
                 } catch (e) {
                 console.log("Error in assigning deviceToken  price request is denied: " + JSON.stringify(e));
                 }
                 var gcmData = {
                 "registration_ids": [
                 buyer_gcm_id
                 ],
                 "data": {
                 "message": "your price request is denied",
                 "type": "Subscription",
                 "operation": "Price_Request_Denial",
                 "sellerId": subscription.sellerId,
                 "buyerId": subscription.buyerId,
                 "buyerName": subscription["buyer"].name,
                 "sellerName": subscription["seller"].name,
                 "sellerContact": subscription["seller"].phone,
                 "sellerBusinessname": subscription["seller"].businessName,
                 "businessCatgName": subscription["seller"].businessTypeName,
                 "businessTypeId": subscription["seller"].businessTypeId,
                 "sellergcmid": subscription["seller"].gcm_id,
                 "deviceToken": subscription["seller"].devicetoken,
                 "id": subscription.id
                 }
                 };
                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 GCM.create(gcmData, function (err, gcm) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating gcm price request is denied: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 APN.create(gcmData, function (err, apn) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating gcm price request is denied: " + JSON.stringify(e));
                 }
                 }

                 }

                 //cb(null, subscriptionU);

                 });
                 } else {
                 //cb(new Error("subscription not found"), null);
                 }

                 });
                 });
                 }
                 else if (notificationR.operation == "Subscription_Request" && notificationR.status == "new") {
                 var statusForSubReq="denied";
                 if(statusForSubscriptionRequest=="true"){
                 statusForSubReq="approved";
                 }
                 notificationR.updateAttributes({"status":statusForSubReq}, function (err, NotificationU) {
                 console.log("approvedSubscription_Request det: " + JSON.stringify(NotificationU));
                 //});
                 Subscription.findById(NotificationU.typeId, function (err, subscription) {
                 //var subscription = subscriptionR[0];

                 if (subscription) {

                 if (statusForSubscriptionRequest == "true" && blockstatusForSubscriptionRequest == "false") {


                 subscription.updateAttributes({"subscriptionRequestStatus": "true"}, function (err, subscriptionU) {

                 var buyer_gcm_id;
                 //var buyer_device_token_id;
                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 buyer_gcm_id = subscription["buyer"].gcm_id;
                 }
                 } catch (e) {
                 console.log("Error in assigning gcm subscriptionRequestStatus: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 buyer_gcm_id = subscription["buyer"].devicetoken;
                 }
                 } catch (e) {
                 console.log("Error in assigning devicetoken subscriptionRequestStatus: " + JSON.stringify(e));
                 }
                 //console.log("buyer_gcm_id: " + buyer_gcm_id);
                 var gcmData = {
                 "registration_ids": [
                 buyer_gcm_id
                 ],
                 "data": {
                 "message": "Your subscription Request Approved",
                 "type": "Subscription",
                 "operation": "Subscription_Request",
                 "id": subscription.id
                 }
                 };

                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 GCM.create(gcmData, function (err, gcm) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating gcm android Request: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 APN.create(gcmData, function (err, apn) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating devicetoken ios Request: " + JSON.stringify(e));
                 }
                 //cb(null, subscriptionU);

                 });

                 } else if (statusForSubscriptionRequest == "false" && blockstatusForSubscriptionRequest == "true") {

                 subscription.updateAttributes({
                 "subscriptionRequestStatus": "false",
                 "blockStatus": "true"
                 }, function (err, subscriptionU) {

                 var buyer_gcm_id;
                 //var buyer_device_token_id;
                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 buyer_gcm_id = subscription["buyer"].gcm_id;
                 }
                 } catch (e) {
                 console.log("Error in assigning gcm block subscriptionRequestStatus: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 buyer_gcm_id = subscription["buyer"].devicetoken;
                 }
                 } catch (e) {
                 console.log("Error in assigning devicetoken block subscriptionRequestStatus: " + JSON.stringify(e));
                 }
                 //console.log("buyer_gcm_id: " + buyer_gcm_id);
                 var gcmData = {
                 "registration_ids": [
                 buyer_gcm_id
                 ],
                 "data": {
                 "message": "Your subscription Request Approved",
                 "type": "Subscription",
                 "operation": "Subscription_Request",
                 "id": subscription.id
                 }
                 };

                 try {
                 if (subscription["buyer"].gcm_id != "NA") {
                 GCM.create(gcmData, function (err, gcm) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating gcm block android Request: " + JSON.stringify(e));
                 }
                 try {
                 if (subscription["buyer"].devicetoken != "NA") {
                 APN.create(gcmData, function (err, apn) {
                 });
                 }
                 } catch (e) {
                 console.log("Error in creating devicetoken block ios Request: " + JSON.stringify(e));
                 }
                 //cb(null, subscriptionU);

                 });

                 } else {
                 //cb(new Error("subscription not found"), null);
                 }

                 } else {
                 //cb(new Error("subscription not found"), null);
                 }

                 });
                 });
                 }*/
                console.log("notificationR: " + JSON.stringify(notificationR));
                updateCount = updateCount + 1;
                if (notificationDetails.length == updateCount) {
                    cb(null, "completed");
                }

            });

        }
        //cb(null,"completed");

    };

    /*
     {arg: 'statusForPriceRequest', type: 'string'},
     {arg: 'statusForSubscriptionRequest', type: 'string'},
     {arg: 'blockstatusForSubscriptionRequest', type: 'string'}*/
    Subscription.remoteMethod(
        'updatingNotificationsStatus',
        {
            accepts: [
                {arg: 'notificationDetails', type: 'array'}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/updatingNotificationsStatus',
                verb: 'get'
            }
        }
    );


    //  updatesubScriptionSubscribedDateForAll
    Subscription.updatesubScriptionSubscribedDateForAll = function (searchData, cb) {
        var Subscription = server.models.Subscription;
        Subscription.find({}, function (err, subscriptions) {
            for (var i = 0; i < subscriptions.length; i++) {
                var subscriptionData = subscriptions[i];
                // var registerdate = ;

                var date = '20-Jun-2016'.split("-");
                // var date = registerdate.split("-");
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                for (var j = 0; j < months.length; j++) {
                    if (date[1] == months[j]) {
                        date[1] = months.indexOf(months[j]) + 1;
                    }
                }
                if (date[1] < 10) {
                    date[1] = '0' + date[1];
                }
                var formattedDate = date[2] + "-" + date[1] + "-" + date[0];
                console.log("formattedDate: " + formattedDate);
                subscriptionData.updateAttributes({
                    "subscribedDate": formattedDate,
                    "createdTime": formattedDate + " 00:00:00"
                }, function (err, subscriptionDataU) {

                });
            }
        });
        cb(null, "completed");
    }

    Subscription.remoteMethod(
        'updatesubScriptionSubscribedDateForAll',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/updatesubScriptionSubscribedDateForAll',
                verb: 'get'
            }
        }
    );

};
