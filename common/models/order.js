var server = require('../../server/server');

module.exports = function (Order) {

    Order.observe('after save', function orderDetails(ctx, next) {
        console.log('after save Order details are: ' + JSON.stringify(ctx.instance));
        var ReferralBuyer = server.models.ReferralBuyer;
        var BuyerEarnedPoints = server.models.BuyerEarnedPoints;
        var Order = server.models.Order;
        var Seller = server.models.Seller;

        Seller.findOne({"where": {"id": ctx.instance.sellerId}}, function (err, sellerR) {
            if (sellerR) {
                if (sellerR.referralActivationStatus == "enabled") {
                    Order.find({
                        "where": {
                            "sellerId": ctx.instance.sellerId,
                            "buyerId": ctx.instance.buyerId
                        }
                    }, function (err, orderR) {
                        if (orderR.length == 1) {
                            console.log("orderR: " + orderR.length);
                            //var orderR = orderR[0];
                            ReferralBuyer.find({
                                "where": {
                                    "referredForSeller": ctx.instance.sellerId,
                                    "referredBuyer": ctx.instance.buyerId, "status": "InActive"
                                }
                            }, function (err, referralBuyerR) {
                                if (referralBuyerR.length > 0) {
                                    var referralBuyerRes = referralBuyerR[0];
                                    var referredBuyerGainedPoints = Number(referralBuyerRes.referralPoints);
                                    console.log("Order referralPoints" + JSON.stringify(referralBuyerRes.referralPoints));
                                    console.log("Order referredBy" + JSON.stringify(referralBuyerRes.referredBy));

                                    BuyerEarnedPoints.find({
                                        "where": {
                                            "buyerId": referralBuyerRes.referredBy,
                                            "sellerId": ctx.instance.sellerId
                                        }
                                    }, function (err, buyerEarnedPointsDet) {
                                        if (buyerEarnedPointsDet.length > 0) {
                                            var buyerEarnedPointsUpdate = buyerEarnedPointsDet[0];
                                            var totalLoyaltyPointsUpdate = Number(referredBuyerGainedPoints + Number(buyerEarnedPointsUpdate.totalPoints));
                                            if (totalLoyaltyPointsUpdate <= 0) {

                                                totalLoyaltyPointsUpdate = 0;
                                            }
                                            buyerEarnedPointsUpdate.updateAttributes({"totalPoints": totalLoyaltyPointsUpdate}, function (err, buyerEarnedPointsUpdateDet) {
                                                console.log('Order buyerreferralEarnedPointsUpdateDet:' + JSON.stringify(buyerEarnedPointsUpdateDet));
                                                referralBuyerRes.updateAttributes({"status": "Active"}, function (err, referralBuyerResUpdateDet) {
                                                    console.log("referralBuyerResUpdateDet: " + JSON.stringify(referralBuyerResUpdateDet));
                                                    next();
                                                });

                                            });
                                        } else {
                                            console.log("order after save else1");
                                            BuyerEarnedPoints.create({
                                                'totalPoints': referredBuyerGainedPoints,
                                                'sellerId': ctx.instance.sellerId,
                                                'buyerId': referralBuyerRes.referredBy,
                                                'status': "Active"
                                            }, function (err, BuyerEarnedPointsDet) {
                                                console.log('Order BuyerEarnedPoints is created: ' + JSON.stringify(BuyerEarnedPointsDet));
                                                referralBuyerRes.updateAttributes({"status": "Active"}, function (err, referralBuyerResUpdateDet) {
                                                    console.log("referralBuyerResUpdateDet: " + JSON.stringify(referralBuyerResUpdateDet));
                                                    next();
                                                });
                                                //next();
                                            });

                                        }
                                    });

                                } else {
                                    console.log("order after save else2");
                                    next();
                                }
                            });
                            /*
                             "referredBuyer": "579f0f1cfc5f92480adf850b",
                             "referredBy": "579f17ea197401f41684867b",
                             "referredForSeller": "57a086bd4ffc83c418d6a60c",
                             "status": "InActive",*/
                        } else {
                            console.log("order after save else3");
                            next();
                        }
                    });


                } else {
                    console.log("order after save else4");
                    next();
                }
            } else {
                console.log("order after save else5");
                next();
            }
        });
    });


};
