var server = require('../../server/server');

module.exports = function (Payment) {

    Payment.observe('before save', function (ctx, next) {

        var frameInfo;
        if (ctx.isNewInstance) {
            frameInfo = ctx.instance;
        } else {
            frameInfo = ctx.currentInstance;
        }

        var Frame = server.models.Frame;
        if ((ctx.instance != undefined) || (ctx.data != undefined)) {

            var data = ctx.instance || ctx.data;
            if (data.paymentStatus == true && frameInfo.type == 'NewFrame') {

                var fc = 0;
                Frame.find({"where": {"sellerId":frameInfo.createdBy}}, function (err, framesData) {
                    var framesCount=Number(framesData.length);
                    console.log("frameCount: "+framesCount);
                    if (frameInfo.data['noOfFrames'] == 0) {
                        console.log("zero frames requested");
                        next();
                    }else{
                        for (var i = 0; i < frameInfo.data["noOfFrames"]; i++) {
                            console.log('frames creating....');
                            Frame.create({'sellerId': frameInfo.createdBy, 'status': false,'frameNo':framesCount+i+1, 'visible': true}, function () {
                                fc++;
                               /* if (fc = frameInfo.data["noOfFrames"]) {
                                    next();
                                }*/
                            });

                        }
                        next();
                    }

                });

            } else {
                next();
            }
        } else {
            next();
        }
    });

    Payment.generateHash = function (data, cb) {
        console.log("Payment Info:"+JSON.stringify(data));
        var cb = cb;
        if(data) {
           console.log("Payment Info:"+JSON.stringify(data));
            var generatedHashValues = {
                "paymentHash":"",
                "paymentMobileSdkHash":"",
                "vasMobileSdkHash":""
            };
            var crypto = require('crypto'),
                key = data.key+"|" + data.txnid + "|" + data.amount + "|"+data.productinfo+ "|" + data.firstname + "|" + data.email + "|"+data.udf1+"|"+data.udf2+"|"+data.udf3+"|"+data.udf4+"|"+data.udf5+"||||||WB1fhhk3";
            console.log('Hash Key......'+key);
            var hash = crypto.createHash('sha512');
            hash.update(key);
            var value = hash.digest('hex');
            generatedHashValues.paymentHash = value.toLowerCase();

          /*  var keys = data.user_credentials.split(":");
            var key1= keys[0];
            var value1 = key[1];
            var uc = {key1:value1};
            console.log('Keys:'+keys);
            console.log('Keys1:'+key1);
            console.log('Keys2:'+value1);
            console.log('Keys3:'+JSON.stringify(uc));*/
            //{"vh":"gtkFFX:635120371236}
            var crypto2 =  require('crypto'),
                    key1 = data.key+"|payment_related_details_for_mobile_sdk|"+data.user_credentials+"|WB1fhhk3";

            console.log('key1....'+key1);
            var hash1 = crypto2.createHash('sha512');
            hash1.update(key1);
            var value2 = hash1.digest('hex');
            generatedHashValues.paymentMobileSdkHash = value2.toLowerCase();

            var crypto3 =  require('crypto'),
                key2 = data.key+"|vas_for_mobile_sdk|"+data.user_credentials+"|WB1fhhk3";
            console.log('key2....'+key2);
            var hash2 = crypto3.createHash('sha512');
            hash2.update(key2);
            var value3 = hash2.digest('hex');
            generatedHashValues.vasMobileSdkHash = value3.toLowerCase();
            console.log("Hash Values:"+JSON.stringify(generatedHashValues));
            cb(null, generatedHashValues);
        } else {
            cb(new Error("Data not Correct"), null);
        }


    };

    Payment.remoteMethod('generateHash', {
        description: "Generating Hash key",
        returns: {
            arg: 'Hash',
            type: 'object'
        },
        accepts: [{arg: 'paymentData', type: 'object', http: {source: 'body'}}
        ],
        http: {
            path: '/generateHash',
            verb: 'POST'
        }
    });

    Payment.paymentSuccess = function (data, cb) {
        console.log("Payment Success Info:"+JSON.stringify(data));
        var cb = cb;
        var successInfo = 'Payment Completed Successfully';
        if(data) {
            cb(null, successInfo);
        } else {
            cb(new Error("Something Is Not Right"), null);
        }
    };

    Payment.remoteMethod('paymentSuccess', {
        description: "Payment Success Data",
        returns: {
            arg: 'successInfo',
            type: 'object'
        },
        accepts: [{arg: 'paymentSuccessData', type: 'object', http: {source: 'query'}}
        ],
        http: {
            path: '/paymentSuccess',
            verb: 'get'
        }
    });

    Payment.paymentFailure = function (data, cb) {
        console.log("Payment Success Info:"+JSON.stringify(data));
        var cb = cb;
        if(data) {
            cb(null, data);
        } else {
            cb(new Error("Something Is Not Right"), null);
        }
    };

    Payment.remoteMethod('paymentFailure', {
        description: "Payment Failure Data",
        returns: {
            arg: 'failureInfo',
            type: 'object'
        },
        accepts: [{arg: 'paymentFailureData', type: 'object', http: {source: 'query'}}
        ],
        http: {
            path: '/paymentFailure',
            verb: 'get'
        }
    });
};
