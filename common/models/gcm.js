

module.exports = function(Gcm) {

    Gcm.beforeSave = function(next, gcm) {

        console.log('sending gcm push notifiction');

        var data = {
            "registration_ids": gcm.registration_ids,
            "data": gcm.data
        };

        var https = require('https');

        var options = {
            host: 'android.googleapis.com',
            path: '/gcm/send',
            method:'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'key = AIzaSyDK2DkIfTr4WzX15RnM8E_cLkl476ZGsx4'
            }
        };

        var req = https.request(options, function(res){
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                console.log('Response: ' + chunk);
            });
        });

        req.write(JSON.stringify(data));
        req.end();
        //your logic goes here
        next();
    };

};
