var server = require('../../server/server');
//console.log(server.models.Seller);

module.exports = function (Seller) {

    //Seller.beforeCreate = function(next, seller){
    Seller.observe('before save', function filterProperties(ctx, next) {
        if (ctx.instance) {
            console.log("entering...before save");

            var Frame = server.models.Frame;

            /*var randomstring = require("randomstring");

             var otp = randomstring.generate({
             length: 6,
             charset: 'numeric'
             });

             var SMS = server.models.SMS;

             seller.otp = otp;
             //seller.password = otp;
             seller['password']=otp;
             seller.otpStatus = false;*/
            ctx.instance.appUrl = 'https://play.google.com/store/apps/details?id=com.aaray.ouroneapp';
            var currentDate = new Date().toISOString();
            var splitDate = currentDate.split('-');
            var splitTime = currentDate.split('T');
            var time = splitTime[1];
            var currentTime = time.split('.');
            var orderHour = splitTime[1].split(':');
            var orderDate = splitDate[2].split('T');
            ctx.instance.subScribedDate = splitDate[0] + "-" + splitDate[1] + "-" + orderDate[0];
            ctx.instance.createdTime = splitDate[0] + "-" + splitDate[1] + "-" + orderDate[0] + " " + currentTime[0];
            console.log("SellerData: " + JSON.stringify(ctx.instance));

            /*var smsMessage = 'Your unique verification code for Ouroneapp is '+otp+' Thank you';
             var destination = seller.phone;

             SMS.create({destination: destination, message: smsMessage}, function(err, sms){

             });*/
            /*var mobileNo="9100287823";
             var smsContent="Hi Admin, New seller: "+seller.name+" and Mobile No: "+seller.phone+" is registered with ouroneapp";
             SMS.create({destination: mobileNo, message: smsContent}, function(err, sms){

             });*/
            var Seller = server.models.Seller;
            var sellerstotalCount = 0;
            Seller.count({}, function (err, totalCount) {
                    if (err) {
                        console.log("Total error " + JSON.stringify(err));
                    } else {
                        console.log("Total count " + totalCount);
                        sellerstotalCount = Number(totalCount) + 1;
                        ctx.instance.SNo = sellerstotalCount;
                        if (sellerstotalCount > 0) {
                            next();
                        }
                    }
                }
            );

            // next();
        } else {
            console.log("coming inside before save but not new instance");
            next();
        }

    });

    /*Seller.observe('after save', function subscriptionDetails(ctx, next) {
        console.log("ctx.isNewInstance: " + ctx.isNewInstance);
        var SMS = server.models.SMS;
        if (ctx.isNewInstance) {

           /!* var smsMessage = "Hi! Thank you for downloading and registering on Vyuli App, on your payment realisation your account would be activated to showcase you product offering to your customers.";
            var destination = ctx.instance.phone;

            SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
            });*!/
            var mobileNo = "9100287823";
            var smsContent = "Hi Admin, New seller: " + ctx.instance.name + " and Mobile No: " + ctx.instance.phone + " is registered with Vyuli App";
            SMS.create({destination: mobileNo, message: smsContent}, function (err, sms) {

            });
        }
    });*/
    /*Seller.observe('after save', function sellerDetails(ctx, next) {
     console.log('after save seller details are: '+JSON.stringify(ctx.instance));
     var Account = server.models.Account;
     var userType = 'Seller';
     Account.create({
     'uid': ctx.instance.id,
     'userType': userType,
     'userId': ctx.instance.phone,
     'email': ctx.instance.email,
     'mail': ctx.instance.mail,
     'businessName': "NA",
     'name': "NA",
     'businessTypeId': "NA",
     'businessTypeName': "NA",
     'seller': ctx.instance,
     'frameCount': 50,
     'frameStatus': false
     }, function () {
     //console.log('account is created');
     });
     fileWriteToAWS(ctx, next);
     next();

     });*/

    /*function fileWriteToAWS(ctx, next) {
     var AWS = require('aws-sdk');
     var fs = require('fs');
     var qr = require('qr-image');
     //var base64 = require('node-base64-image');

     AWS.config.loadFromPath('config.json');
     s3Stream = require('s3-upload-stream')(new AWS.S3());
     console.log("excuting image qr code");
     console.log("sellerId: "+JSON.stringify(ctx.instance));

     var qrCodeObject = {
     "sellerId": ctx.instance.id,
     "sellerName": ctx.instance.name
     //"sellerBusinessName": ctx.instance.businessName,
     //"businessTypeId": ctx.instance.businessTypeId,
     //"businessTypeName": ctx.instance.businessTypeName
     };
     console.log(JSON.stringify(qrCodeObject));

     var code = qr.image(JSON.stringify(qrCodeObject), {type: 'png'});

     var upload = s3Stream.upload({
     Bucket: "araay",
     Key: ctx.instance.id + "_" + (new Date()).getTime(),
     ACL: "public-read",
     //StorageClass: "REDUCED_REDUNDANCY",
     ContentType: "png"
     });

     console.log('entered');
     upload.maxPartSize(20971520); // 20 MB
     upload.concurrentParts(5);

     upload.on('error', function (error) {
     console.log("Error: " + error);
     next();
     });

     upload.on('part', function (details) {
     console.log("part" + details);
     });

     upload.on('uploaded', function (details) {
     console.log("uploaded: " + JSON.stringify(details));
     ctx.instance["qrcode"] = details.Location;
     ctx.instance = {
     status: "success",
     message: "",
     data: ctx.instance
     };

     console.log('id: '+ctx.instance.data.id);
     Seller.findById(ctx.instance.data.id, function(err, sellerR){

     console.log("sellerInfo: "+ JSON.stringify(sellerR));
     sellerR.updateAttributes({"qrcode": details.Location}, function(err, uSeller){
     //next();
     });

     });
     //Seller.updateAll({'id': ctx.result.id}, {qrcode: details.Location}, function (ctx, next) {
     //});
     /!* if (ctx.req.baseUrl.toString() == '/api/Accounts' && ctx.req.method.toString() == 'GET') {

     if (ctx.result.data.length > 0) {
     ctx.result.message = "User Already Registered";
     } else {
     ctx.result.message = 'User is Not Registered';
     }

     }*!/
     next();
     });

     code.pipe(upload);


     }*/
    //Seller.afterCreate = function(next, seller){
    //
    //    console.log(JSON.stringify(seller));
    //    var Frame = server.models.Frame;
    //    var fc = 0
    //    for (var i = 0; i < 20; i++) {
    //        //console.log('frame');
    //        Frame.create({'sellerId': seller.id, 'status': false, 'visible':true}, function (err, frame) {
    //            fc++;
    //            if(fc == 20) {
    //                next();
    //            }
    //        });
    //    }
    //
    //}

    Seller.observe('loaded', function (ctx, next) {

        var Frame = server.models.Frame;
        var Rating = server.models.Rating;
        //console.log("accessing: "+ JSON.stringify(ctx.instance));
        if (ctx.instance != undefined) {
            //console.log("sellerName: "+ctx.instance.name+"  sellerPhone: "+ctx.instance.phone);

            if (ctx.instance.businessName != undefined) {
                ctx.instance['shareDetails'] = "Hi! To know more about latest offers and deals on " + ctx.instance.businessName + ", download VYULI and subscribe to " + ctx.instance.businessName + " Or " + ctx.instance.phone + ". iOS http://apple.co/29eDGxZ for Android http://bit.ly/29bVAEy";
            } else {
                ctx.instance['shareDetails'] = "Hi! Check out this VYULI App, here I can view fresh arrivals and offers in my favourite Shops. Download: iOS http://apple.co/29eDGxZ for Android http://bit.ly/29bVAEy";
            }

            Frame.find({"where": {"sellerId": ctx.instance.id}}, function (err, frames) {

                if (frames.length > 0) {
                    //console.log("framesLength: "+frames.length);
                    var frameStatus = false;
                    var frameCount = 0;
                    var fc = 0;
                    for (var f in frames) {

                        var frame = frames[f];

                        //console.log(frame.updateStatus);
                        if (frame.updateStatus == true) {

                            frameCount++;

                            frameStatus = true;

                        }
                        fc++;
                        if (fc == frames.length) {
                            ctx.instance['frameCount'] = frameCount;
                            ctx.instance['frameStatus'] = frameStatus;

                            Rating.find({"where": {"and": [{"whoom": ctx.instance.id}, {"type": "seller"}]}}, function (err, ratings) {

                                var totalR = 0;
                                for (var r in ratings) {

                                    var rating = ratings[r];

                                    totalR = totalR + parseInt(rating.rating);

                                }

                                console.log("ratingsLength: " + ratings.length);
                                console.log("totalR: " + totalR);

                                var avgRating = 0;

                                if (totalR != 0) {
                                    var avgRating = parseInt(totalR / ratings.length);
                                }

                                console.log("avgRating: " + avgRating);

                                var ratingObj = {};

                                ratingObj['rating'] = avgRating;
                                ratingObj['type'] = 'seller';
                                ratingObj['whoom'] = ctx.instance.id;
                                ratingObj['createdBy'] = 'all';

                                ctx.instance['rating'] = ratingObj;
                                next();

                            });

                        }

                    }
                } else {
                    next();
                }


            });
        } else {
            next();
        }

        //next();

    });


    //Seller.afterRemote('**', function(ctx, seller, next) {
    //    //var Frame = server.models.Frame;
    //
    //    var frameCount = 0;
    //    var frameStatus = false;
    //    console.log("frame : "+Frame);
    //    Frame.find({"sellerId": frame.sellerId}, function(err, frames){
    //
    //        if(err) next(err);
    //
    //        var fCount = 0;
    //        for(var f in frames){
    //
    //            var frame = frames[f];
    //
    //            if(frame.updateStatus == true) {
    //
    //                frameCount++;
    //                frameStatus = true;
    //
    //            }
    //            fCount++;
    //
    //            if(fCount == frames.length) {
    //
    //                seller['frameCount'] = frameCount;
    //                seller['frameStatus'] = frameStatus;
    //                next();
    //
    //            }
    //
    //        }
    //
    //    });
    //
    //
    //});

    //Seller.on('dataSourceAttached', function(obj){
    //
    //    var find = Seller.find;
    //    Seller.find = function(filter, cb) {
    //
    //        find.call(Seller, function(err, sellers) {
    //            for(s in sellers ) {
    //                var Frame = server.models.Frame;
    //                var seller = sellers[s];
    //                Frame.find({"where":{"sellerId":seller.id}}, function(err, frames){
    //                    //seller['frameCount'] = frames.length;
    //                    //seller['frameStatus'] = true;
    //                    console.log("s: "+JSON.stringify(seller));
    //                });
    //
    //
    //            }
    //        });
    //
    //        return find.apply(this, arguments);
    //
    //    };
    //});

    //Seller.findById = function(id, cb){
    //    console.log("getter");
    //    var Frame = server.models.Frame;
    //    Seller.findById(id, function(err, seller, cb){
    //        Frame.find({"where":{"sellerId":seller.id}}, function(err, frames, cb){
    //            seller['frameCount'] = frames.length;
    //            seller['frameStatus'] = false;
    //            cb(null, seller);
    //        });
    //    })
    //}
    //
    //Seller.find = function(filter, cb){
    //    console.log("getter");
    //    var Frame = server.models.Frame;
    //    Seller.find(filter, function(err, sellers, cb){
    //        var c= 0;
    //        for(s in sellers){
    //
    //            var seller = sellers[s];
    //
    //            Frame.find({"where":{"sellerId":seller.id}}, function(err, frames, cb){
    //                seller['frameCount'] = frames.length;
    //                seller['frameStatus'] = false;
    //                c++;
    //                if(c == sellers.length){
    //                    cb(null, sellers);
    //                }
    //
    //            });
    //
    //        }
    //
    //    })
    //}

    Seller.enablePriceDisplay = function (sellerId, cb) {

        var cb = cb;

        var Seller = server.models.Seller;
        var SellerServices = server.models.SellerServices;
        var Subscription = server.models.Subscription;

        Seller.findById(sellerId, function (err, seller) {

            if (seller) {
                var serviceList = seller.sellerservicelist;
                var serviceListN = [];

                var pdNew;

                for (var sl in serviceList) {

                    var eService = serviceList[sl]
                    if (eService.name == "Price Display") {

                        //serviceList.pull(sl);
                        //break;

                    } else {
                        serviceListN.push(eService);
                    }

                }

                pdNew = {
                    "name": "Price Display",
                    "required": true,
                    "id": "569cbda89d2649b85d5e8bdb"
                };

                serviceListN.push(pdNew);

                seller.updateAttributes({"sellerservicelist": serviceListN}, function (err, sellerU) {


                    Subscription.find({"where": {"sellerId": sellerId}}, function (err, subscriptions) {

                        var iSubscriptions = [];
                        if (subscriptions.length > 0) {
                            var countS = 0;
                            for (var i in subscriptions) {

                                var subscription = subscriptions[i];

                                Subscription.confirmPriceDisplayRequest(subscription.id, true, function (err, subscriptionN) {

                                    iSubscriptions.push(subscriptionN);
                                    countS++;
                                    if (countS == subscriptions.length) {
                                        sellerU['subscriptions'] = iSubscriptions;
                                        cb(null, sellerU);
                                    }

                                });

                            }
                        } else {
                            cb(null, sellerU);
                        }


                    });


                });

            } else {
                cb(new Error("seller not found"), null);
            }

        });

    };

    Seller.remoteMethod('enablePriceDisplay', {
        description: "please update payment status",
        returns: {
            arg: 'seller',
            type: 'array'
        },
        accepts: [{arg: 'sellerId', type: 'string', http: {source: 'query'}}
        ],
        http: {
            path: '/enablePriceDisplay',
            verb: 'get'
        }
    });

    Seller.disablePriceDisplay = function (sellerId, cb) {

        var cb = cb;

        var Seller = server.models.Seller;
        var SellerServices = server.models.SellerServices;
        var Subscription = server.models.Subscription;

        Seller.findById(sellerId, function (err, seller) {

            if (seller) {
                var serviceList = seller.sellerservicelist;
                var serviceListN = [];

                var pdNew;

                for (var sl in serviceList) {

                    var eService = serviceList[sl]
                    if (eService.name == "Price Display") {

                        //serviceList.pull(sl);
                        //break;

                    } else {
                        serviceListN.push(eService);
                    }

                }

                //pdNew = {
                //    "name": "Price Display",
                //    "required": true,
                //    "id": "569cbda89d2649b85d5e8bdb"
                //};

                //serviceListN.push(pdNew);

                seller.updateAttributes({"sellerservicelist": serviceListN}, function (err, sellerU) {

                    Subscription.find({"where": {"sellerId": sellerId}}, function (err, subscriptions) {

                        var iSubscriptions = [];
                        if (subscriptions.length > 0) {
                            var countS = 0;
                            for (var i in subscriptions) {

                                var subscription = subscriptions[i];

                                Subscription.confirmPriceDisplayRequest(subscription.id, false, function (err, subscriptionN) {

                                    iSubscriptions.push(subscriptionN);
                                    countS++;
                                    if (countS == subscriptions.length) {
                                        sellerU['subscriptions'] = iSubscriptions;
                                        cb(null, sellerU);
                                    }

                                });

                            }
                        } else {
                            cb(null, sellerU);
                        }


                    });

                });
            } else {
                cb(new Error("seller not found"), null);
            }

        });

    };

    Seller.remoteMethod('disablePriceDisplay', {
        description: "please update payment status",
        returns: {
            arg: 'seller',
            type: 'array'
        },
        accepts: [{arg: 'sellerId', type: 'string', http: {source: 'query'}}
        ],
        http: {
            path: '/disablePriceDisplay',
            verb: 'get'
        }
    });

    var Frame = server.models.Frame;

    Seller.customImplementation = function (cb) {

        Seller.find({}, function (err, sellers) {

            for (var s in sellers) {

                var seller = sellers[s];

                Frame.find({"where": {"sellerId": seller.id}}, function (err, frames) {

                    //seller.frameCount = frames.length;

                    //subscription.updateAttribute('sbid', subscription.sbid, function(err, sub){
                    //
                    //    console.log(sub);
                    //
                    //});
                    //seller.FrameStatus = false;
                    seller.updateAttributes({
                        'frameCount': frames.length,
                        'frameStatus': false
                    }, function (err, seller) {
                        //console.log(sub);
                    });

                    //seller.save();

                });


            }

        });

    }

    Seller.remoteMethod('customImplementation', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'sellers',
            type: 'array'
        },
        accepts: {http: {source: 'query'}},
        http: {
            path: '/customImplementation',
            verb: 'get'
        }
    });


    Seller.sellerSMS = function (message, cb) {


        Seller.find({}, function (err, sellers) {
            var SMS = server.models.SMS;
            /*for(var b in buyers) {
             var buyer = buyers[b];
             }*/
            console.log("sellers list: " + JSON.stringify(sellers));
            for (var i = 0; i < sellers.length; i++) {
                var seller = sellers[i];
                //var phone=buyer.phone;

                //var smsMessage = 'Your unique verification code for Ouroneapp is '+otp+' Thank you';
                var destination = seller.phone;
                console.log("Seller phone: " + destination);

                SMS.create({destination: destination, message: message}, function (err, sms) {

                });

            }
            cb(null, "completed");
        });


    }

    Seller.remoteMethod(
        'sellerSMS',
        {
            accepts: [{arg: 'message', type: 'string', http: {source: 'query'}}],
            returns: [{arg: 'sellers', type: 'array'}],
            http: {
                path: '/sendSellerSMS',
                verb: 'get'
            }
        }
    );

    // sending sms for referring to others
    Seller.sellerReferralSMS = function (phone, sellerId, cb) {
        var Smsbulk = server.models.SMSBULK;
        var Seller = server.models.Seller;
        console.log("seller phone: " + phone);
        Seller.findById(sellerId, function (err, seller) {
            if (seller) {
                if (seller.businessName != undefined) {
                    var sms = "Hi! To know more about latest offers and deals on " + seller.businessName + ", download VYULI and subscribe to " + seller.businessName + " Or " + seller.phone + ". iOS http://apple.co/29eDGxZ for Android http://bit.ly/29bVAEy";
                    Smsbulk.create({destination: phone, message: sms}, function (err, sms) {
                        if (err) {
                            cb(err, null);
                        } else {
                            cb(null, "success");
                        }
                    });
                } else {
                    var sms =  "Hi! Check out this VYULI, here I can view fresh arrivals and offers in my favourite Shops. Download: iOS http://apple.co/29eDGxZ for Android http://bit.ly/29bVAEy";
                    Smsbulk.create({destination: phone, message: sms}, function (err, sms) {
                        if (err) {
                            cb(err, null);
                        } else {
                            cb(null, "success");
                        }
                    });
                    cb(null, "seller businessName not found");

                }
            } else {
                cb(null, "seller not found");
            }

        });

    }
    Seller.remoteMethod(
        'sellerReferralSMS',
        {
            accepts: [
                {arg: 'phone', type: 'string', http: {source: 'query'}},
                {arg: 'sellerId', type: 'string', http: {source: 'query'}}
            ],
            //returns:[{arg:'status',type:'string'}],
            http: {
                path: '/sellerReferralSMS',
                verb: 'get'
            }
        }
    );
    Seller.sellerReferralDefaultSMS = function (cb) {
        var message = 'Hi! To get updates on new product arrivals in OURONEAPP Store download Ouroneapp and add OURONEAPP Store. https://play.google.com/store/apps/details?id=com.aaray.ouroneapp';

        cb(null, message);

    }
    Seller.remoteMethod(
        'sellerReferralDefaultSMS',
        {
            returns: [{arg: 'message', type: 'string'}],
            http: {
                path: '/sellerReferralDefaultSMS',
                verb: 'get'
            }
        }
    );

    /*// giving regular expression based result of sellers
     Seller.sellerSearch = function (searchData,cb) {
     var Seller = server.models.Seller;
     var search = searchData;
     console.log("seller search data: "+searchData);
     var pattern = new RegExp('^' + accountName, "i");
     console.log('pattern: '+ pattern);
     var filterString = {where: {businessName: {regexp: pattern}}};
     Seller.find(filterString, function (err, sellers) {
     var sellersCount=sellers.length();
     console.log("sellersCount: "+sellersCount);

     });

     }
     Seller.remoteMethod(
     'sellerSearch',
     {
     accepts:[
     {arg:'searchData',type:'string',http: {source: 'query'}},

     ],
     //returns:[{arg:'status',type:'string'}],
     http: {
     path: '/sellerSearch',
     verb: 'get'
     }
     }
     );*/


//  sellerDefaultDataAppending
    Seller.sellerDefaultDataAppending = function (searchData, cb) {
        /*"paymentStatusAdmin": "true",
         "createdTime": "NA",*/
        var Seller = server.models.Seller;
        var updateCount = 0;
        Seller.find({}, function (err, sellers) {
            for (var i = 0; i < sellers.length; i++) {
                var sellerDet = sellers[i];
                sellerDet.updateAttributes({
                    "houseNo": "42",
                    "area": "Miyapur",
                    "city": "Hyderabad",
                    "state": "Telangana",
                    "country": "India",
                    "pincode": "500050",
                    "longitude": "17.429710",
                    "latitude": "78.552356",
                    "originId": "NA",
                    "profilePic": "NA",
                    "sellerInitial": "NA",
                    "color": "NA",
                    "subscriptionInvitation": "disabled",
                    "loyaltyActivationStatus": "disabled",
                    "referralActivationStatus": "disabled",
                    "alternativeNumber": "NA",
                    "countryCode": "+91",
                    "paymodeId": "NA",
                    "SNo": i + 1
                }, function (err, sellerInfoR) {
                    updateCount = updateCount + 1;
                    console.log("updateCount: " + updateCount);
                    if (sellers.length == updateCount) {
                        console.log("response given......");
                        cb(null, "completed");
                    }
                });
            }
            console.log("sellersCount: " + sellers.length);
        });
    }

    Seller.remoteMethod(
        'sellerDefaultDataAppending',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/sellerDefaultDataAppending',
                verb: 'get'
            }
        }
    );

    //  updatesellerRegisteredDate
    Seller.updatesellerRegisteredDate = function (searchData, cb) {
        var Seller = server.models.Seller;
        Seller.find({}, function (err, sellers) {
            for (var i = 0; i < sellers.length; i++) {
                var sellerData = sellers[i];
                var registerdate = sellerData.servcssubscribedate;
                var paymentStatus = "false";
                if (sellerData.paymentStatus == true || sellerData.paymentStatus == "1") {
                    paymentStatus = "true";
                }
                // var date = '05-Jul-2016'.split("-");
                var date = registerdate.split("-");
                var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                for (var j = 0; j < months.length; j++) {
                    if (date[1] == months[j]) {
                        date[1] = months.indexOf(months[j]) + 1;
                    }
                }
                if (date[1] < 10) {
                    date[1] = '0' + date[1];
                }
                var formattedDate = date[2] + "-" + date[1] + "-" + date[0];
                console.log("formattedDate: " + formattedDate);
                sellerData.updateAttributes({
                    "registeredDate": formattedDate,
                    "paymentStatusAdmin": paymentStatus,
                    "createdTime": formattedDate + " 00:00:00"
                }, function (err, sellerDataU) {

                });
            }
        });
        cb(null, "completed");
    }

    Seller.remoteMethod(
        'updatesellerRegisteredDate',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/updatesellerRegisteredDate',
                verb: 'get'
            }
        }
    );


};


