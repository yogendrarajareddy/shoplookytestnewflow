var server = require('../../server/server');

module.exports = function (Frame) {

    Frame.observe('before save', function (ctx, next) {


        var frameInfo;
        var gcmData = {
            "registration_ids": [],
            "data": {
                "message": "",
                "type": "",
                "operation": "",
                "id": '',
                "sellerId": ''
            }
        };

        var apnData = {};
        var message;

        if (ctx.isNewInstance) {
            ctx.instance.updateStatus = true;
            ctx.instance.visible = true;
            frameInfo = ctx.instance;
            gcmData.data.message = 'New Frame Added';
            gcmData.data.type = 'Frame';
            gcmData.data.operation = 'New_Frame';
            gcmData.data.id = frameInfo.id;
            next();
        }/* else{
         //console.log("frame update else...........executing.....");
         //next();
         if(ctx.data.imageName == undefined){
         console.log("imageName Undefined................");
         next();
         }else{
         console.log("imageName defined................");
         //next();
         //console.log("JSON.stringify(ctx.currentInstance.id): "+JSON.stringify(ctx.currentInstance.id));
         //console.log("JSON.stringify(ctx.currentInstance.id): "+JSON.stringify(ctx.data.imageName));
         thumbNailCreate(ctx,next);
         }
         }*/
        else {
            /* if (ctx.data.thumbNailUrl == undefined && ctx.data.frameImageUrl == undefined ) {*/
            //console.log("ctx.data.image.uploadimagename.........................: "+ctx.data.uploadimagename);
            if (ctx.data.image == undefined) {

                console.log("updating frame url and thumbnailurl");
                next();
            } else {


                ctx.currentInstance.updateStatus = true;
                ctx.currentInstance.visible = true;
                //thumbNailUrl  frameImageUrl
                frameInfo = ctx.currentInstance;
                console.log("frameInfo:before save " + JSON.stringify(ctx));
                gcmData.data.message = 'Frame Updated';
                gcmData.data.type = 'Frame';
                gcmData.data.operation = 'Frame_Update';
                gcmData.data.id = frameInfo.id;

                var sellerId = frameInfo.sellerId;
                var Notification = server.models.Notification;
                var Subscription = server.models.Subscription;
                var sellerName;
                var sellerRating;
                var sellerContact;
                var sellerBusinessname;
                var businessCatgName;
                var businessTypeId;
                var pricedisplayServicestatus;
                var sellergcmid;
                var deviceToken;
                var subscriptionid;
                var gcmDevice = false;
                var apnDevice = false;

                //console.log('Apn Data:' + JSON.stringify(apnData));
                var buyerIds = new Array();
                var countOfSubscriptions = 0;


                Subscription.find({"where": {"sellerId": sellerId}}, function (err, subscriptions) {


                    var buyersGCMIDs = [];
                    console.log("subscriptions: " + subscriptions.length);
                    if (subscriptions.length > 0) {
                        var subscriptionData = subscriptions[0];
                        var serviceList = subscriptionData.sellerservicelist;
                        pricedisplayServicestatus = "false";
                        for (var sl in serviceList) {

                            var eService = serviceList[sl]
                            if (eService.name == "Price Display") {
                                if (eService.required == true) {
                                    pricedisplayServicestatus = "true";
                                }

                            }

                        }
                        sellerName = subscriptionData["seller"].name;
                        sellerContact = subscriptionData["seller"].phone;
                        sellerRating = subscriptionData["seller"].rating.rating;
                        sellerBusinessname = subscriptionData["seller"].businessName;
                        businessCatgName = subscriptionData["seller"].businessTypeName;
                        businessTypeId = subscriptionData["seller"].businessTypeId;
                        sellergcmid = subscriptionData["seller"].gcm_id;
                        deviceToken = subscriptionData["seller"].devicetoken;
                        subscriptionid = subscriptionData.id;
                    }
                    for (var i in subscriptions) {
                        countOfSubscriptions = Number(countOfSubscriptions) + 1;
                        var subscription = subscriptions[i];

                        buyerId = subscription.buyerId;
                        buyerIds.push(subscription.buyerId);
                        console.log("buyerId: " + buyerId);
                        try {
                            if (subscription['buyer'].gcm_id != "NA") {
                                gcmDevice = true;
                                gcmData.registration_ids.push(subscription["buyer"].gcm_id);
                            }
                        } catch (e) {
                            console.log("frames error: " + JSON.stringify(e));
                        }
                        try {
                            if (subscription['buyer'].devicetoken != "NA") {
                                apnDevice = true;
                                apnData.registration_ids.push(subscription["buyer"].devicetoken);
                            }
                        } catch (e) {
                            console.log("frames error: " + JSON.stringify(e));
                        }
                        //buyersGCMIDs.push(subscription["buyer"].gcm_id);

                    }

                    //console.log("buyerIdssssss: " + JSON.stringify(buyerIds.length));
                    if (countOfSubscriptions == subscriptions.length) {

                        for (var j = 0; j < buyerIds.length; j++) {
                            //console.log("enteringn.................................");
                            var notificationDetails = {
                                "from": sellerId,
                                "fromType": "seller",
                                "sellerName": sellerName,
                                "to": buyerIds[j],
                                "toType": "buyer",
                                "message": "Frame Updated",
                                "type": "Frame",
                                "operation": "Frame_Update",
                                "status": "new",
                                "pricedisplayServicestatus": pricedisplayServicestatus,
                                "thumbNailUrl": ctx.currentInstance.thumbNailUrl,
                                "frameImageUrl": ctx.currentInstance.frameImageUrl,
                                "sellerContact": sellerContact,
                                "sellerRating": sellerRating,
                                "sellerBusinessname": sellerBusinessname,
                                "businessCatgName": businessCatgName,
                                "businessTypeId": businessTypeId,
                                "sellergcmid": sellergcmid,
                                "deviceToken": deviceToken,
                                "subscriptionid": subscriptionid,
                                "typeId": frameInfo.id
                            };
                            Notification.create(notificationDetails, function (err, notificationR) {
                                console.log("Subscription_Request notificationR: " + JSON.stringify(notificationR));
                            });


                        }
                        var GCM = server.models.GCM;
                        var APN = server.models.APN;
                        gcmData.data.sellerName = sellerName;
                        gcmData.data.sellerRating = sellerRating;
                        gcmData.data.pricedisplayServicestatus = pricedisplayServicestatus;
                        gcmData.data.sellerContact = sellerContact;
                        gcmData.data.sellerBusinessname = sellerBusinessname;
                        gcmData.data.businessCatgName = businessCatgName;
                        gcmData.data.businessTypeId = businessTypeId;
                        gcmData.data.sellergcmid = sellergcmid;
                        gcmData.data.deviceToken = deviceToken;
                        gcmData.data.subscriptionid = subscriptionid;
                        gcmData.data.thumbNailUrl = ctx.currentInstance.thumbNailUrl;
                        gcmData.data.frameImageUrl = ctx.currentInstance.frameImageUrl;


                        apnData = gcmData;

                        apnData.data.sellerId = sellerId;

                        //apnData.data.sellerName = sellerName;
                        gcmData.data.sellerId = sellerId;
                        try {
                            if (gcmDevice == true) {
                                GCM.create(gcmData, function (err, gcm) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in frames: " + JSON.stringify(e));
                        }
                        try {
                            if (apnDevice == true) {
                                APN.create(apnData, function (err, apn) {
                                });
                            }
                        } catch (e) {
                            console.log("Error in frames: " + JSON.stringify(e));
                        }
                    }
                    next();

                });
                //thumbNailCreate(ctx, next);
            }

        }

        //
        //console.log("before save: "+ ctx.instance);
        //if(ctx.instance != undefined){
        //    ctx.instance.updateStatus = true;
        //}
        //
        //next();

    });
    /*
     Frame.observe('loaded', function (ctx, next) {
     //
     //if(ctx.instancdde != undefined) {
     //    var Frame = server.models.Frame;
     //    console.log("accessing: "+ JSON.stringify(ctx.instance));
     //    if(ctx.instance.visible == false){
     //
     //        ctx.instance = null;
     //
     //    }
     //}

     next();

     });*/

    //Frame.beforeCreate = function(next, frame) {
    //    frame.updateStatus = true;
    //    next();
    //}
    //
    //Frame.beforeUpdate = function(next, frame) {
    //    frame.updateStatus = true;
    //    next();
    //};

    //Frame.beforeUpdate = function (next, inst) {
    //
    //    var Seller = server.models.Seller;
    //
    //    console.log("changed: "+ inst.id);
    //
    //    //Seller.findById(inst.sellerId, function(err, seller){
    //    //
    //    //    var count = seller.frameCount+1;
    //    //    seller.updateAttributes({frameCount: count, frameStatus: true }, function(err, seller){
    //    //        console.log("seller information is updated");
    //    //    });
    //    //
    //    //});
    //
    //
    //    Frame.findById(inst.id, function (err, frame) {
    //
    //        frame.save({"updateStatus":true}, function(err, frame){
    //            console.log("update: "+ frame);
    //        });
    //
    //    });
    //
    //};

    Frame.addNewFrames = function (sellerId, noOfFrames, cb) {

        var cb = cb;

        var Frame = server.models.Frame;

        var cc = 0;
        var frames = [];
        for (var c = 0; c < noOfFrames; c++) {

            Frame.create({"sellerId": sellerId, "status": false}, function (err, frame) {
                frames.push(frame);
                cc++;
                if (err) {
                    cb(null, err);
                }
                if (cc == noOfFrames) {
                    cb(null, frames);
                }

            });

        }

        //var Seller = server.models.Seller;
        ////var SMS = server.models.SMS;
        ////var EmailN = server.models.EmailN;
        //
        //Seller.findOne({"where": {"id": sellerId}}, function (err, seller) {
        //
        //    if (err) {
        //        cb(null, err);
        //    }
        //
        //    //var SMS = server.models.SMS;
        //    //
        //    //var smsMessage = 'Your Frame has been activated please login';
        //    //var destination = seller.phone;
        //
        //
        //
        //
        //
        //    //seller.updateAttributes({"paymentStatus": true}, function (err, sellerNext) {
        //    //
        //    //    if(err){
        //    //        cb(null, err);
        //    //    }
        //    //    //SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
        //    //    //    cb(null, sms);
        //    //    //});
        //    //
        //    //});
        //
        //});

    };

    Frame.remoteMethod('addNewFrames', {
        description: "please update payment status",
        returns: {
            arg: 'frames',
            type: 'array'
        },
        accepts: [{arg: 'sellerId', type: 'string', http: {source: 'query'}},
            {arg: 'noOfFrames', type: 'string', http: {source: 'query'}}],
        http: {
            path: '/addNewFrames',
            verb: 'get'
        }
    });


    Frame.customImplementation = function (cb) {

        var Seller = server.models.Seller;
        Seller.find({}, function (err, sellers) {

            for (var s in sellers) {

                var seller = sellers[s];

                Frame.find({"where": {"sellerId": seller.id}}, function (err, frames) {

                    //seller.frameCount = frames.length;

                    //subscription.updateAttribute('sbid', subscription.sbid, function(err, sub){
                    //
                    //    console.log(sub);
                    //
                    //});1
                    //seller.FrameStatus = false;
                    seller.updateAttributes({
                        'frameCount': frames.length,
                        'frameStatus': false
                    }, function (err, seller) {
                        console.log("seller info : " + seller);
                        //console.log(sub);
                    });

                    //seller.save();

                });


            }

        });

    }

    Frame.remoteMethod('customImplementation', {
        description: "Gets all subscription information bases on business name",
        returns: {
            arg: 'sellers',
            type: 'array'
        },
        accepts: {http: {source: 'query'}},
        http: {
            path: '/customImplementation',
            verb: 'get'
        }
    });

    // for updating frame no of seller
    Frame.updatingFrameNo = function (searchData, cb) {
        var Seller = server.models.Seller;
        var Frame = server.models.Frame;
        var sellersList = [];
        var sellersCount = 0;
        var sellersFramesUpdatedCount = 0;

        var count = 0;
        Seller.find({}, function (err, sellers) {
            for (var i = 0; i < sellers.length; i++) {
                sellerDet = sellers[i];
                sellersList.push(sellerDet.id);
                count = count + 1;
            }
            console.log("sellersDet: " + JSON.stringify(sellersList));
            console.log("sellersListCount: " + sellersList.length);
            console.log("sellersCount: " + sellers.length);
            sellersCount = sellers.length;
            var updateCount = 0;
            if (count == sellersList.length) {
                for (var j = 0; j < sellersList.length; j++) {
                    //var sellerData=sellersList[j];
                    console.log("sellersList[j]: " + sellersList[j]);
                    Frame.find({"where": {"sellerId": sellersList[j]}}, function (err, framesR) {
                        sellersFramesUpdatedCount = sellersFramesUpdatedCount + Number(framesR.length);
                        for (var k = 0; k < framesR.length; k++) {
                            var frameInfo = framesR[k];
                            if (frameInfo.image != undefined && frameInfo.thumbNailUrl == undefined && frameInfo.frameImageUrl == undefined) {
                                console.log("frameInfo: " + frameInfo.image.uploadimagename);
                                frameInfo.updateAttributes({"imageName": frameInfo.image.uploadimagename}, function (err, frameInfo) {
                                    updateCount = updateCount + 1;
                                    console.log("updateCount: " + updateCount);

                                });
                            } else {

                            }
                            //console.log("frameInfo: "+frameInfo.image.uploadimagename);
                            /*frameInfo.updateAttributes({"frameNo":k+1}, function (err, frameInfo) {
                             updateCount=updateCount+1;
                             console.log("updateCount: "+updateCount);

                             });*/
                        }
                    });
                }
            }

        });


        //if(updateCount==sellersFramesUpdatedCount){
        cb(null, "completed");
        //}


    }
    Frame.remoteMethod(
        'updatingFrameNo',
        {
            accepts: [
                {arg: 'searchData', type: 'string', http: {source: 'query'}}
            ],
            returns: [{arg: 'status', type: 'string'}],
            http: {
                path: '/updatingFrameNo',
                verb: 'get'
            }
        }
    );


    Frame.thumbNailCreation = function (s3ImageUrl, thumbNailName, frameId, cb) {

        var Jimp = require("jimp");
        var uploadImage = thumbNailName;
        var frameImageSplit = uploadImage.split('.');
        var frameName = frameImageSplit[0];
        var Frame = server.models.Frame;
        //var s3ImageUrl="/imgs/frame.jpg";
        console.log("frameName: " + frameName);
        // s3 bucket link /*https://s3-ap-southeast-1.amazonaws.com/araay/20141123_164955.jpg*/imgs/" + uploadImage + ""
        Jimp.read(s3ImageUrl, function (err, frameName) {
            console.log("s3ImageUrl: " + s3ImageUrl);
            console.log("uploadImage: " + uploadImage);
            console.log("frameName: " + frameName);
            if (err) {
                //cb(err,null);
            } else {
                //console.log("image: "+uploadImage);
                frameName.resize(120, 120)            // resize
                    .quality(100)                 // set JPEG quality
                    .write("/.../thumbNails/" + uploadImage + "");          // save
                console.log("resized");
                imageUpload(uploadImage);
            }
        });

        var imageUpload = function (uploadImage) {
            var AWS = require('aws-sdk');
            var fs = require('fs');
            //aws credentials
            AWS.config = new AWS.Config();
            AWS.config.accessKeyId = "AKIAJBPDSJ47N7DAPPLQ";
            AWS.config.secretAccessKey = "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";
            AWS.config.region = "ap-southeast-1";
            var s3 = new AWS.S3();

            var bodystream = fs.createReadStream('/.../thumbNails/' + uploadImage + '');

            var params = {
                'Bucket': 'araay',
                'Key': 'thumbnails/' + uploadImage + '',
                'Body': bodystream,
                'ContentEncoding': 'base64',
                'ContentType ': 'image/jpeg'
            };

            //also tried with s3.putObject
            s3.upload(params, function (err, data) {
                if (err) {
                    cb(err, null);
                } else {
                    console.log("data: " + JSON.stringify(data));

                    //cb(null, "success");
                    if (data.Location) {

                        Frame.findById(frameId, function (err, frameR) {

                            console.log("frameR: " + JSON.stringify(frameR));
                            frameR.updateAttributes({
                                "thumbNailUrl": data.Location,
                                "frameImageUrl": s3ImageUrl
                            }, function (err, frameRes) {

                                cb(null, frameRes);

                            });

                        });
                        try {
                            cleanUp(uploadImage);
                            /* setTimeout(function () {
                             console.log("setTimeout: It's been 30 seconds!");
                             cleanUp(uploadImage);
                             }, 30000);*/
                        } catch (err) {
                            console.log("setTimeOutError: " + err);
                        }
                    } else {
                        console.log("failure of upload into s3 bucket: " + JSON.stringify(data));
                    }

                }
            })

        }
        var cleanUp = function (cleanImageName) {
            console.log("called for clean up " + cleanImageName);

            try {
                var fs = require('fs');
                var filePath = "/.../thumbNails/" + cleanImageName + "";
                fs.unlink(filePath);
                //cb(null, "success");
                console.log("cleaned");
            } catch (e) {
                console.log("cleanUp error: " + e);
            }
        }

    }
    Frame.remoteMethod(
        'thumbNailCreation',

        {
            description: "provide thumbnail image name with extension and frame s3 bucket url",
            accepts: [{arg: 's3ImageUrl', type: 'string'}, {arg: 'thumbNailName', type: 'string'}, {
                arg: 'frameId',
                type: 'string'
            }],
            returns: [{type: 'object', root: true}],
            http: {
                path: '/thumbNailCreation',
                verb: 'get'
            }
        });


    /*function thumbNailCreate(ctx, next) {

     // start.. code for thumbNail creation of frame image uploaded by seller
     var thumbNailName = ctx.data.image.uploadimagename;
     //console.log("frameInfo: " + JSON.stringify(ctx));
     console.log("thumbNailName: " + thumbNailName);
     var s3ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/araay/" + thumbNailName;
     console.log("s3ImageUrl: " + s3ImageUrl);
     var frameId = ctx.currentInstance.id;
     console.log("frameId: " + frameId);
     var Jimp = require("jimp");
     var uploadImage = thumbNailName;
     var frameImageSplit = uploadImage.split('.');
     var frameName = frameImageSplit[0];
     var Frame = server.models.Frame;
     //var s3ImageUrl="/imgs/frame.jpg";
     console.log("frameName: " + frameName);
     // s3 bucket link /!*https://s3-ap-southeast-1.amazonaws.com/araay/20141123_164955.jpg*!/imgs/" + uploadImage + ""
     Jimp.read(s3ImageUrl, function (err, frameName) {
     console.log("s3ImageUrl: " + s3ImageUrl);
     console.log("uploadImage: " + uploadImage);
     console.log("frameName: " + frameName);
     if (err) {
     //cb(err,null);
     } else {
     //console.log("image: "+uploadImage);
     frameName.resize(120, 120)            // resize
     .quality(100)                 // set JPEG quality
     .write("../thumbNails/" + uploadImage + "");          // save
     console.log("resized");
     imageUpload(uploadImage);
     }
     });
     try {

     var imageUpload = function (uploadImage) {
     var AWS = require('aws-sdk');
     var fs = require('fs');
     //aws credentials
     AWS.config = new AWS.Config();
     AWS.config.accessKeyId = "AKIAJBPDSJ47N7DAPPLQ";
     AWS.config.secretAccessKey = "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";
     AWS.config.region = "ap-southeast-1";
     var s3 = new AWS.S3();

     var bodystream = fs.createReadStream('../thumbNails/' + uploadImage + '');

     var params = {
     'Bucket': 'araay',
     'Key': 'thumbnails/' + uploadImage + '',
     'Body': bodystream,
     'ContentEncoding': 'base64',
     'ContentType ': 'image/jpeg'
     };

     //also tried with s3.putObject
     s3.upload(params, function (err, data) {
     if (err) {
     //cb(err, null);
     console.log("error in upload of thumbNail");
     next();
     } else {
     console.log("data: " + JSON.stringify(data));

     //cb(null, "success");
     if (data.Location) {

     Frame.findById(frameId, function (err, frameR) {
     console.log("frameR: " + JSON.stringify(frameR));
     frameR.updateAttributes({
     "thumbNailUrl": data.Location,
     "frameImageUrl": s3ImageUrl
     }, function (err, frameRes) {

     //cb(null, frameRes);

     });

     });
     try {
     cleanUp(uploadImage);
     /!* setTimeout(function () {
     console.log("setTimeout: It's been 30 seconds!");
     cleanUp(uploadImage);
     }, 30000);*!/
     } catch (err) {
     console.log("setTimeOutError: " + err);
     }
     next();
     } else {
     console.log("failure of upload into s3 bucket: " + JSON.stringify(data));
     next();
     }

     }
     })

     }
     } catch (err) {
     console.log("imageUploadError: " + err);
     }
     var cleanUp = function (cleanImageName) {
     console.log("called for clean up " + cleanImageName);

     try {
     var fs = require('fs');
     var filePath = "../thumbNails/" + cleanImageName + "";
     fs.unlink(filePath);
     //cb(null, "success");
     console.log("cleaned");
     } catch (e) {
     console.log("cleanUp error: " + e);
     }
     }
     //next();
     // end.. code for thumbNail creation of frame image uploaded by seller
     }*/

    function thumbNailCreate(ctx, next) {

        // start.. code for thumbNail creation of frame image uploaded by seller
        //var thumbNailName = ctx.req.body.image.uploadimagename;
        var thumbNailName = ctx.data.imageName;
        //console.log("frameInfo: " + JSON.stringify(ctx.req.body));
        console.log("thumbNailName: " + thumbNailName);
        var s3ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/araay/" + thumbNailName;
        console.log("s3ImageUrl: " + s3ImageUrl);
        console.log("frameId: " + ctx.currentInstance.id);
        var frameId = ctx.currentInstance.id;
        var Jimp = require("jimp");
        var uploadImage = thumbNailName;
        var frameImageSplit = uploadImage.split('.');
        var frameName = frameImageSplit[0];
        var Frame = server.models.Frame;
        //var s3ImageUrl="/imgs/frame.jpg";
        console.log("frameName: " + frameName);
        // s3 bucket link /*https://s3-ap-southeast-1.amazonaws.com/araay/20141123_164955.jpg*/imgs/" + uploadImage + ""
        Jimp.read(s3ImageUrl, function (err, frameName) {
            console.log("s3ImageUrl: " + s3ImageUrl);
            console.log("uploadImage: " + uploadImage);
            if (err) {
                //cb(err,null);
                next();
            } else {
                //console.log("image: "+uploadImage);
                frameName.resize(120, 120)            // resize
                    .quality(100)                 // set JPEG quality
                    .write("thumbNails/" + uploadImage + "", function (err, writeR) {
                        if (err) {
                            console.log("error in writing to file");
                            next();
                        } else {
                            console.log("writed Response: ");
                            imageUpload(uploadImage);
                        }

                    });          // save
                console.log("resized");

            }
        });
        try {

            var imageUpload = function (uploadImage) {
                var AWS = require('aws-sdk');
                var fs = require('fs');
                //aws credentials
                AWS.config = new AWS.Config();
                AWS.config.accessKeyId = "AKIAJBPDSJ47N7DAPPLQ";
                AWS.config.secretAccessKey = "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";
                AWS.config.region = "ap-southeast-1";
                var s3 = new AWS.S3();

                var bodystream = fs.createReadStream('thumbNails/' + uploadImage + '');

                var params = {
                    'Bucket': 'araay',
                    'Key': 'thumbnails/' + uploadImage + '',
                    'Body': bodystream,
                    'ContentEncoding': 'base64',
                    'ContentType ': 'image/jpeg'
                };

                //also tried with s3.putObject
                s3.upload(params, function (err, data) {
                    if (err) {
                        //cb(err, null);
                        console.log("error in upload of thumbNail");
                        next();
                    } else {
                        console.log("data: " + JSON.stringify(data));

                        //cb(null, "success");
                        if (data.Location) {

                            Frame.findById(frameId, function (err, frameR) {
                                console.log("frameR: " + JSON.stringify(frameR));
                                frameR.updateAttributes({
                                    "thumbNailUrl": data.Location,
                                    "frameImageUrl": s3ImageUrl
                                }, function (err, frameRes) {

                                    /*//cb(null, frameRes);
                                     try {
                                     //cleanUp(uploadImage);
                                     /!* setTimeout(function () {
                                     console.log("setTimeout: It's been 30 seconds!");
                                     cleanUp(uploadImage);
                                     }, 30000);*!/
                                     } catch (err) {
                                     console.log("setTimeOutError: " + err);
                                     }*/

                                });

                            });
                            cleanUp(uploadImage);
                            next();
                        } else {
                            console.log("failure of upload into s3 bucket: " + JSON.stringify(data));
                            next();
                        }

                    }
                })

            }
        } catch (err) {
            console.log("imageUploadError: " + err);
        }
        var cleanUp = function (cleanImageName) {
            console.log("called for clean up " + cleanImageName);

            try {
                var fs = require('fs');
                var filePath = "thumbNails/" + cleanImageName + "";
                fs.unlink(filePath);
                //cb(null, "success");
                console.log("cleaned");
            } catch (e) {
                console.log("cleanUp error: " + e);
            }
        }
        //next();
        // end.. code for thumbNail creation of frame image uploaded by seller
    }


};
