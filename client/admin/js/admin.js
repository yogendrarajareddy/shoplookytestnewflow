/**
 * Created by Nibble on 3/11/2016.
 */

//var myApp = angular.module('myApp', ['ng-admin','restangular']);

var myApp = angular.module('myApp', ['ng-admin', 'restangular']);

myApp.config(['RestangularProvider', function (RestangularProvider) {

    RestangularProvider.addFullRequestInterceptor(function (element, operation, what, url, headers, params) {
        console.log("params at interceptor: " + JSON.stringify(params))
        if (operation == "getList") {

            //console.log(url);
            // custom pagination params
            console.log(params);

            var queryString = {};

            if (params._page) {
                params.skip = (params._page - 1) * params._perPage;
                queryString['skip'] = params.skip;
                params.limit = params._perPage;
                queryString['limit'] = params.limit;
            }
            delete params.skip;
            delete params.limit;
            delete params._page;
            delete params._perPage;
            // custom sort params
            if (params._sortField) {
                //params._sort = params._sortField;
                //params._order = params._sortDir;
                //filterString = filterString + "{order:" + ":'" +params._sortField + " " + params._sortDir+"'}";
                queryString['order'] = params._sortField + ' ' + params._sortDir;
                delete params._sortField;
                delete params._sortDir;
            }
            // custom filters
            if (params._filters) {
                console.log("filters in filters: " + JSON.stringify(params._filters));
                var filterString = {};
                for (var filter in params._filters) {

                    if (filter.search("_") != -1) {
                        var strarr = filter.split("_");

                        if (strarr[1] != null) {

                            if (strarr[1] == "gt") {
                                filterString[strarr[0]] = {gt: params._filters[filter] + '.*', options: 'i'};
                            } else if (strarr[1] == "lt") {
                                filterString[strarr[0]] = {lt: params._filters[filter] + '.*', options: 'i'};
                            } else if (strarr[1] == "lte") {
                                filterString[strarr[0]] = {lte: params._filters[filter] + '.*', options: 'i'};
                            } else if (strarr[1] == "gte") {
                                filterString[strarr[0]] = {gte: params._filters[filter]};
                            } else if (strarr[1] == "id") {
                                filterString[strarr[0]] = params._filters[filter];
                            } else if (strarr[1] == "inq") {
                                console.log("filter in inq: " + params._filters[filter]);
                                var olddate = params._filters[filter];
                                if (olddate != null) {
                                    var olddatearr = olddate.split("-");
                                    console.log("year: " + olddatearr[0]);
                                    console.log("month: " + olddatearr[1]);
                                    console.log("date: " + olddatearr[2]);
                                    var newdate = olddatearr[1] + '/' + olddatearr[2] + '/' + olddatearr[0];
                                    filterString[strarr[0]] = {inq: [{"dates": newdate}]};
                                }
                            } else if (strarr[1] == "between") {
                                //console.log("filter in between: "+params._filters[filter]);
                                var sedates = params._filters[filter];
                                var sedatesArr = sedates.split(",");
                                var sday = new Date(sedatesArr[0]);
                                var eday = new Date(sedatesArr[1]);

                                var em = Number(eday.getMonth());
                                var emonth = '' + (em + 1);
                                var edate = '' + eday.getDate();
                                if (emonth.length < 2) {
                                    emonth = "0" + emonth;
                                }
                                if (edate.length < 2) {
                                    edate = "0" + edate;
                                }
                                var convertedEndDate = eday.getFullYear() + "-" + emonth + "-" + edate + "T59:59:59.000Z";
                                filterString[strarr[0]] = {between: [sday, convertedEndDate]};
                                console.log("filterString[strarr[0]]: " + JSON.stringify(filterString[strarr[0]]));
                            }
                        }
                    } else {
                        console.log("paymentFilter: " + filter);
                        //filter={"paymentStatus":true};
                        //console.log("paymentFilter: "+filter);
                        var str = ".*" + params._filters[filter] + ".*";
                        filterString[filter] = {like: params._filters[filter] + '.*', options: 'i'};
                        //params[filter] = params._filters[filter];
                    }
                }

                // if()
                queryString['where'] = filterString;
                console.log("query string: " + queryString);
                delete params._filters;
            }

            params['filter'] = queryString;
            console.log("params: " + JSON.stringify(params))
        }
        return {params: params};
    });
    //response interceptor start
    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
        var extractedData;
        // .. to look for getList operations
        //    console.log('data:'+JSON.stringify(data));
        // .. and handle the data and meta data
        extractedData = data.data;
        // extractedData.meta = data.data.meta;

        return extractedData;
    });
    //response interceptor end
}]);

myApp.config(['NgAdminConfigurationProvider', function (NgAdminConfigurationProvider) {
    var nga = NgAdminConfigurationProvider;
    var customHeaderTemplate =
        '<div class="navbar-header">' +
        '<a class="navbar-brand" href="#" ng-click="appController.displayHome()">' +
        '<h1 style="margin: -7px">OneApp Admin</h1>' +
        '</a>' +
        '</div>' +
        '<p class="navbar-text navbar-right">' +
        '<a href="https://github.com/marmelab/ng-admin/blob/master/examples/blog/config.js">' +
        '<a href="#" onclick="logout()">Logout</a>' +
        '</a>' +
        '</p>';

    //var admin = nga.application('OneApp Admin').baseApiUrl('http://139.162.24.167:3636/api/');
    var admin = nga.application('OneApp Admin').baseApiUrl('http://104.196.99.177:7896/api/');
    // var admin = nga.application('OneApp Admin').baseApiUrl('http://ouroneapp.com/api/');
    //var admin = nga.application('OneApp Admin').baseApiUrl('http://localhost:7896/api/');


    admin.header(customHeaderTemplate);
    nga.menu().autoClose(true)
    admin.menu(nga.menu()
        .addChild(nga.menu().title('Businesses').link('/Businesses/list'))
        .addChild(nga.menu().title('SellerServices').link('/SellerServices/list'))
        .addChild(nga.menu().title('Accounts')
            .addChild(nga.menu().title('Seller Accounts').link('/Accounts/list?search={"userType":"Seller"}'))
            .addChild(nga.menu().title('Buyer Accounts').link('/Accounts/list?search={"userType":"Buyer"}')))
        .addChild(nga.menu().title('Sellers')
            .addChild(nga.menu().title('Paid Sellers').link('/Sellers/list?search={"paymentStatusAdmin":"true"}'))
            .addChild(nga.menu().title('UnPaid Sellers').link('/Sellers/list?search={"paymentStatusAdmin":"false"}')))
        .addChild(nga.menu().title('Buyers')
            .addChild(nga.menu().title('Buyers List').link('/Buyers/list')))
        //.addChild(nga.menu().title('Frames Purchase Details')
        .addChild(nga.menu().title('Paid Frames Details').link('/Payments/list'))
        ///*.addChild(nga.menu().title('PaymentPending').link('/Sellers/list?search={"paymentStatus":false}'))*/)
        .addChild(nga.menu().title('Subscription')
            .addChild(nga.menu().title('Subscription Details').link('/Subscriptions/list')))
        .addChild(nga.menu().title('Frames')
            .addChild(nga.menu().title('Frame List').link('/Frames/list')))
        .addChild(nga.menu().title('SalesManDetails').link('/SalesMans/list'))
        .addChild(nga.menu().title('FeedBack').link('/Feedbacks/list'))
        .addChild(nga.menu().title('Contact Info').link('/ContactInfos/list'))
        .addChild(nga.menu().title('Country').link('/Countries/list'))
        .addChild(nga.menu().title('State').link('/States/list'))
        .addChild(nga.menu().title('City').link('/Cities/list'))
        .addChild(nga.menu().title('SubscriptionPlans').link('/SubscriptionPlans/list'))
        .addChild(nga.menu().title('PaymodeTypes').link('/PaymodeTypes/list'))
        //.addChild(nga.menu().title('GCMs').link('/GCMs/list'))
        .addChild(nga.menu().title('Reports')
            .addChild(nga.menu().title('Seller Details').link('/sellerDetails'))
            .addChild(nga.menu().title('Buyer Details').link('/buyerDetails'))
            .addChild(nga.menu().title('Business Details').link('/businessDetails'))
            .addChild(nga.menu().title('Seller Frames Details').link('/sellersInfo'))
            .addChild(nga.menu().title('SalesMans Refferal Details').link('/salesMansRefferalDet')))
    );
    var buyers = nga.entity('Buyers');
    var accounts = nga.entity('Accounts');
    var subscriptions = nga.entity('Subscriptions');
    var frames = nga.entity('Frames');
    var sellerservices = nga.entity('SellerServices');
    var payment = nga.entity('Payments');
    var pendingframes = nga.entity('Payments');
    var sellerdata = nga.entity('Sellers');
    var seller = nga.entity('Sellers').label('PaymentPendingList');
    var gcm = nga.entity('GCMs');
    var feedback = nga.entity('Feedbacks');
    var salesman = nga.entity('SalesMans');
    var business = nga.entity('Businesses');
    var contactInfo = nga.entity('ContactInfos');
    var country = nga.entity('Countries');
    var state = nga.entity('States');
    var city = nga.entity('Cities');
    var subscriptionPlan = nga.entity('SubscriptionPlans');
    var paymodeType = nga.entity('PaymodeTypes');

    var fieldSet = [
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('descritpion'),
        nga.field('imageUrl').defaultValue('https://s3-us-west-2.amazonaws.com/ouroneapp/image.jpg')
    ];
    //business.listView().fields(fieldSet);
    business.listView().sortField('name').fields(fieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('name')
    ]);
    business.creationView().fields(fieldSet);
    business.editionView().title('Edit :{{ entry.values.name }}').fields(fieldSet);
    admin.addEntity(business);


    var countryFieldSet = [
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('code')
    ];
    country.listView().sortField('name').fields(countryFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('name')
    ]);
    country.creationView().fields(countryFieldSet);
    country.editionView().title('Edit :{{ entry.values.name }}').fields(countryFieldSet);
    admin.addEntity(country);

    var stateFieldSet = [
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('countryId', 'reference')
            .isDetailLink(false)
            .label('Country')
            .targetEntity(country)
            .targetField(nga.field('name'))
    ];
    state.listView().sortField('name').fields(stateFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('name')
    ]);
    state.creationView().fields(stateFieldSet);
    state.editionView().title('Edit :{{ entry.values.name }}').fields(stateFieldSet);
    admin.addEntity(state);

    var cityFieldSet = [
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('stateId', 'reference')
            .isDetailLink(false)
            .label('State')
            .targetEntity(state)
            .targetField(nga.field('name'))
    ];
    city.listView().sortField('name').fields(cityFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('name')
    ]);
    city.creationView().fields(cityFieldSet);
    city.editionView().title('Edit :{{ entry.values.name }}').fields(cityFieldSet);
    admin.addEntity(city);

    var subscriptionPlanFieldSet = [
        //nga.field('id').editable(false),
        nga.field('subscriptionAmount'),
        nga.field('subscriptionPeriod'),
        nga.field('type'),
        nga.field('countryId', 'reference')
            .isDetailLink(false)
            .label('Country')
            .targetEntity(country)
            .targetField(nga.field('name'))
    ];
    subscriptionPlan.listView().sortField('countryId').fields(subscriptionPlanFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('subscriptionAmount'),
        nga.field('type'),
        nga.field('subscriptionPeriod'),
        nga.field('countryId', 'reference')
            .isDetailLink(false)
            .label('Country')
            .targetEntity(country)
            .targetField(nga.field('name'))
    ]);
    subscriptionPlan.creationView().fields(subscriptionPlanFieldSet);
    subscriptionPlan.editionView().title('Edit :{{ entry.values.type }}').fields(subscriptionPlanFieldSet);
    admin.addEntity(subscriptionPlan);


    var paymodeTypeFieldSet = [
        //nga.field('id').editable(false),
        nga.field('name'),
        nga.field('description')
    ];
    paymodeType.listView().sortField('countryId').fields(paymodeTypeFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('name'),
        nga.field('description')
    ]);
    paymodeType.creationView().fields(paymodeTypeFieldSet);
    paymodeType.editionView().title('Edit :{{ entry.values.name }}').fields(paymodeTypeFieldSet);
    admin.addEntity(paymodeType);


    /*
     //sales team add

     var salesman = nga.entity('SalesMans');

     var salesManFieldSet = [
     //nga.field('id').editable(false),
     nga.field('name'),
     nga.field('email'),
     nga.field('phone'),
     nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
     ];

     salesman.listView().fields(salesManFieldSet);
     salesman.creationView().fields(salesManFieldSet);
     salesman.editionView().fields(salesManFieldSet);

     admin.addEntity(salesman);*/

    //sales team add

    //var salesman = nga.entity('SalesMans');

    var salesManFieldSet = [
        //nga.field('id').editable(false),
        nga.field('name')
            .validation({required: true}),
        nga.field('email').validation({required: false}),
        nga.field('phone')


    ];
    salesman.listView().fields(salesManFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]);
    salesman.creationView().fields(salesManFieldSet, [nga.field('password').label('Type').defaultValue('salesman').editable(false)]);
    salesman.editionView().fields(salesManFieldSet);
    admin.addEntity(salesman);

    var contactInfoFieldSet = [
        //nga.field('id').editable(false),
        nga.field('companyInfo.name').label("Name"),
        nga.field('companyInfo.email').label("Email").validation({required: false}),
        nga.field('companyInfo.contact').label("Phone"),
        nga.field('companyInfo.address').label("Address"),
        nga.field('shareForSeller'),
        nga.field('shareForBuyer')
    ];
    var template = '<ma-show-button entry="entry" entity="entity" size="sm"></ma-show-button>' +
        '<ma-delete-button entry="entry" entity="entity" size="sm"></ma-delete-button>' +
        '<my-custom-directive entry="entry"></my-custom-directive>' +
        '<ma-back-button></ma-back-button>';
    contactInfo.listView().fields(contactInfoFieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>'
            + '<ma-show-button entry="entry" entity="entity" size="xs"></ma-show-button>'),
    ]);
    contactInfo.creationView().fields(contactInfoFieldSet);
    contactInfo.showView().title('Detail View {{entry.values.companyInfo.name}}').fields(contactInfoFieldSet);
    contactInfo.editionView().actions(template).title('Edit View {{ entry.values.companyInfo }}').fields(contactInfoFieldSet);
    admin.addEntity(contactInfo);


////

    //var salestrack = nga.entity('Sellers').label('SalesTrack');
    //
    //var salesfieldSet = [
    //   /* nga.field('id'),*/
    //    nga.field('name'),
    //    nga.field('phone'),
    //    nga.field('businessTypeName'),
    //    nga.field('Referral.name'),
    //    nga.field('Referral.contact'),
    //    nga.field('bankname'),
    //    nga.field('chequenum'),
    //    nga.field('paymentStatus', 'boolean')
    //        .choices([
    //            { value: null, label: 'not yet decided' },
    //            { value: true, label: 'enabled' },
    //            { value: false, label: 'disabled' }
    //        ]),
    //    nga.field('paymodedesc'),
    //    nga.field('subsrcibeamount'),
    //    nga.field('servcssubscribedate','date'),
    //    nga.field('FramesList', 'referenced_list') // display list of related comments
    //        .targetEntity(nga.entity('Frames'))
    //        .targetReferenceField('sellerId')
    //        .targetFields([
    //            nga.field('id'),
    //            nga.field('imageDescription'),
    //            nga.field('image.uploadimagename'),
    //            nga.field('status'),
    //            nga.field('updateStatus'),
    //            nga.field('item price')
    //
    //        ]),
    //    nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    //];
    //salestrack.listView().fields(salesfieldSet);
    //salestrack.creationView().fields(salesfieldSet);
    //salestrack.editionView().fields(salesfieldSet);
    //admin.addEntity(salestrack);

////application filed
    /*var application = nga.entity('applications');
     var fieldSet1 = [
     nga.field('id'),
     nga.field('realm'),
     nga.field('name'),
     nga.field('description'),
     nga.field('icon'),
     nga.field('owner'),
     //nga.field('collaborators[0].'),
     nga.field('email'),
     nga.field('emailVerified', 'boolean')
     .choices([
     { value: null, label: 'not yet decided' },
     { value: true, label: 'enabled' },
     { value: false, label: 'disabled' }
     ]),
     nga.field('url'),
     /!* nga.field('callbackUrls'),
     nga.field('permissions'),*!/
     nga.field('pushSettings'),
     nga.field('authenticationEnabled','boolean')
     .choices([
     { value: null, label: 'not yet decided' },
     { value: true, label: 'enabled' },
     { value: false, label: 'disabled' }
     ]),
     nga.field('anonymousAllowed','boolean')
     .choices([
     { value: null, label: 'not yet decided' },
     { value: true, label: 'enabled' },
     { value: false, label: 'disabled' }
     ]),
     //nga.field('authenticationSchemes'),
     nga.field('status', 'boolean')
     .choices([
     { value: null, label: 'not yet decided' },
     { value: true, label: 'enabled' },
     { value: false, label: 'disabled' }
     ]),
     nga.field('created','datetime'),
     nga.field('modified','datetime'),
     nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
     ];
     application.listView().fields(fieldSet1);
     application.creationView().fields(fieldSet1);
     application.editionView().fields(fieldSet1);
     admin.addEntity(application);*/


    //feedback

    //var feedback = nga.entity('Feedbacks');
    var fieldSet3 = [
        //nga.field('id'),
        nga.field('rating', 'number'),
        nga.field('comment'),
        nga.field('createdBy')
    ];
    //feedback.listView().fields(fieldSet3);
    feedback.listView().sortField('name').fields(fieldSet3).filters([
        nga.field('rating'),
        nga.field('comment')
    ]);
    feedback.creationView().fields(fieldSet3);
    feedback.editionView().fields(fieldSet3);
    admin.addEntity(feedback);


    //GCM


    var fieldSet5 = [
        //nga.field('id'),
        //nga.field('registration_ids'),
        nga.field('message'),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')

    ];
    gcm.listView().fields(fieldSet5);
    gcm.creationView().fields(fieldSet5);
    gcm.editionView().fields(fieldSet5);
    admin.addEntity(gcm);


    //payment pending list


    var fieldSet7 = [
        nga.field('id'),
        nga.field('businessName').label('BusinessName'),
        nga.field('name'),
        nga.field('phone'),
        nga.field('businessTypeName').label('BusinessType'),
        /*nga.field('paymentStatus', 'choice')
         .choices([
         { value: null, label: 'not yet decided' },
         { value: true, label: 'true' },
         { value: false, label: 'false' }
         ]),*/
        nga.field('paymentStatus', 'choice')
            .choices([
                {value: true, label: 'true'},
                {value: false, label: 'false'}
            ]),
        nga.field('frameCount').label('Frames Count'),
        nga.field('Referral.name').label('Refferal Name'),
        nga.field('Referral.contact').label('Refferal Phone'),
        nga.field('bankname'),
        nga.field('chequenum'),
        nga.field('paymodedesc'),
        nga.field('subScribedDate', 'date').defaultValue(new Date()),
        nga.field('subsrcibeamount').label('Subscribe Amount'),
        nga.field('servcssubscribedate').label('Subscribe StartDate'),
        nga.field('servcexpiredate', 'datetime').label('Subscribe EndDate'),
        nga.field('address'),
        nga.field('createdTime')
        //nga.field('paymentStatusAdmin')

        /* nga.field('FramesList', 'referenced_list') // display list of related comments
         .targetEntity(nga.entity('Frames'))
         .targetReferenceField('sellerId')
         .targetFields([
         nga.field('name').isDetailLink(true)
         /!*nga.field('sellerId','reference')
         .isDetailLink(false)
         .label('Seller Name')
         .targetEntity(seller)
         .targetField(nga.field('name')).isDetailLink(true)*!/

         ]),*/
        /* nga.field('transactionId'),*/
        /*nga.field('Buyers', 'referenced_list') // display list of related orders
         .targetEntity(nga.entity('Buyers'))
         .targetReferenceField('customerId')
         .targetFields([
         nga.field('id').isDetailLink(false),
         // nga.field('packageId').label('package id'),
         nga.field('packageId', 'reference')
         .label('Package Name')
         .targetEntity(packages)
         .targetField(nga.field('packageName')),
         //.singleApiCall(ids => ({'id': ids })),
         nga.field('orderstatus').label('order status')
         ])*/

        /*nga.field('FramesList', 'reference_many').isDetailLink(false)
         .targetEntity(nga.entity('Frames'))
         .targetField('id')*/
        /* nga.field('Frames', 'reference_many').label('Frames').isDetailLink(false)
         .targetEntity(nga.entity('Frames'))
         .targetField(nga.field('id'))*/
        /*nga.field('Buyers', 'referenced_list') // display list of related comments
         .targetEntity(nga.entity('Subscriptions'))
         .targetReferenceField('sellerId')
         .targetFields([
         //nga.field('sellerId').isDetailLink(true)
         nga.field('buyerId','reference')
         .isDetailLink(false)
         .label('Buyer Name')
         .targetEntity(buyers)
         .targetField(nga.field('name')).isDetailLink(false),
         nga.field('buyerId','reference')
         .isDetailLink(false)
         .label('Phone Number')
         .targetEntity(buyers)
         .targetField(nga.field('phone'))
         ])*/
    ];

    seller.listView().sortField('createdTime').sortDir('DESC').fields(fieldSet7, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('businessName'),
        nga.field('businessTypeName'),
        //nga.field('paymentStatus', 'ch'),
        nga.field('paymodedesc'),
        nga.field('paymentStatusAdmin', 'choice').label('PaymentStatus')
            .choices([
                {value: 'true', label: 'true'},
                {value: 'false', label: 'false'}
            ]),
        nga.field('name'),
        nga.field('Referral.name').label('Refferal Name'),
        nga.field('phone'),
        nga.field('subScribedDate', 'date').defaultValue(new Date()),
        nga.field('createdTime')
    ]);
    seller.creationView().fields(fieldSet7);
    seller.editionView().title('Edit :{{ entry.values.businessName }}').fields(fieldSet7, [
        nga.field('Buyers', 'referenced_list') // display list of related comments
            .targetEntity(nga.entity('Subscriptions'))
            .targetReferenceField('sellerId')
            .targetFields([
                //nga.field('sellerId').isDetailLink(true)
                nga.field('buyerId', 'reference')
                    .isDetailLink(false)
                    .label('Buyer Name')
                    .targetEntity(buyers)
                    .targetField(nga.field('name')).isDetailLink(false),
                nga.field('buyerId', 'reference')
                    .isDetailLink(false)
                    .label('Phone Number')
                    .targetEntity(buyers)
                    .targetField(nga.field('phone'))
            ]),
        /*nga.field('Frames', 'referenced_list') // display list of related comments
         .targetEntity(frames)
         .targetReferenceField('sellerId')
         .targetFields([
         //nga.field('sellerId')
         nga.field('sellerId','reference')
         .isDetailLink(false)
         .label('seller')
         .targetEntity(seller)
         .targetField(nga.field('name'))
         ])*/
        nga.field('frames', 'referenced_list') // Define a 1-N relationship with the comment entity
            .targetEntity(frames) // Target the comment Entity
            .targetReferenceField('sellerId') // Each comment with post_id = post.id (the identifier) will be displayed
            .targetFields([ // which comment fields to display in the datagrid
                nga.field('id').label('ID')

            ])


    ]);

    admin.addEntity(seller);

    //seller


    var sellerfieldSet7 = [
        nga.field('id'),
        nga.field('name'),
        nga.field('phone'),
        nga.field('businessTypeName'),
        nga.field('businessTypeId', 'reference')
            .isDetailLink(false)
            .label('Business Details')
            .targetEntity(business)
            .targetField(nga.field('id')),
        nga.field('businessName'),
        nga.field('qrcode', 'template').label('')
            .template('<img src="{{ entry.values.qrcode }}" height="42" width="42" />'),
        //nga.field('qrcode'),
        nga.field('referral.points', 'number'),
        nga.field('referral.amount', 'number'),
        nga.field('referral.totalPoints'),
        //nga.field('email'),
        //nga.field('servcexpiredate','datetime'),
        //nga.field('transactionId'),
        //nga.field('bankname'),
        //nga.field('chequenum'),
        nga.field('loyality.amount'),
        nga.field('loyality.points'),
        nga.field('loyality.totalPoints'),
        //nga.field('Referral.contact'),
        //nga.field('Referral.name'),
        nga.field('paymentStatus', 'choice')
            .choices([
                {value: null, label: 'not yet decided'},
                {value: true, label: 'success'},
                {value: false, label: 'failure'}
            ]),
        nga.field('paymodedesc'),
        nga.field('subsrcibeamount'),
        nga.field('servcssubscribedate'),
        //nga.field('sellerservicelist','json'),
        //nga.field('address'),
        //nga.field('loyaltPoint'),
        //nga.field('frameCount'),
        nga.field('frameStatus', 'boolean')
            .choices([
                {value: null, label: 'not yet decided'},
                {value: true, label: 'enabled'},
                {value: false, label: 'disabled'}
            ]),
        nga.field('FramesList', 'referenced_list') // display list of related comments
            .targetEntity(nga.entity('Frames'))
            .targetReferenceField('sellerId')
            .targetFields([
                nga.field('id'),
                nga.field('imageDescription'),
                nga.field('image.uploadimagename'),
                nga.field('status'),
                nga.field('updateStatus'),
                nga.field('item price')

            ]),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ];
    // sellerdata.listView().fields(sellerfieldSet7);
    sellerdata.listView().sortField('name').fields(sellerfieldSet7).filters([
        nga.field('paymentStatus'),
        nga.field('name'),
        nga.field('phone')
    ]);
    sellerdata.creationView().fields(sellerfieldSet7);
    sellerdata.editionView().title('Edit: {{ entry.values.name }}').fields(sellerfieldSet7);

    admin.addEntity(sellerdata);

    ///Frame Pending


    var pendingframefieldSet = [
        //nga.field('id'),
        nga.field('createdBy', 'reference')
            .isDetailLink(false)
            .label('Seller Name')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('createdBy', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName')),
        nga.field('createdBy', 'reference')
            .isDetailLink(false)
            .label('Phone Number')
            .targetEntity(seller)
            .targetField(nga.field('phone')),
        nga.field('type'),
        nga.field('status', 'choice')
            .choices([
                {value: true, label: 'true'},
                {value: false, label: 'false'}
            ]),
        nga.field('paymentStatus', 'choice')
            .choices([
                {value: true, label: 'true'},
                {value: false, label: 'false'}
            ]),
        nga.field('paymentType'),
        nga.field('data.noOfFrames').label('No of Frames'),
        nga.field('chequeNumber'),
        nga.field('transactId'),
        nga.field('bankName')

    ];
    //pendingframes.listView().fields(pendingframefieldSet);
    pendingframes.listView().sortField('status').fields(pendingframefieldSet, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('createdBy', 'reference')
            .isDetailLink(false)
            .label('Seller Name')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('createdBy', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName')),
        nga.field('createdBy', 'reference')
            .isDetailLink(false)
            .label('Phone Number')
            .targetEntity(seller)
            .targetField(nga.field('phone')),
        nga.field('paymentType', 'choice')
            .choices([
                {value: 'Cheque', label: 'Cheque'},
                {value: 'Online', label: 'Online'}
            ]),
        nga.field('status', 'choice')
            .choices([
                {value: true, label: 'true'},
                {value: false, label: 'false'}
            ]),
        nga.field('paymentStatus', 'choice')
            .choices([
                {value: true, label: 'true'},
                {value: false, label: 'false'}
            ])
    ]);
    pendingframes.creationView().fields(pendingframefieldSet);
    pendingframes.editionView().title('Edit: {{ entry.values.createdBy }}').fields(pendingframefieldSet);
    admin.addEntity(pendingframes);

    //payment


    var fieldSet6 = [
        //nga.field('id'),
        nga.field('type'),
        nga.field('createdBy'),
        nga.field('paymentStatus'),
        nga.field('paymentType'),
        nga.field('data.noOfFrames').label('No of Frames'),
        nga.field('chequeNumber', 'number'),
        nga.field('transactId'),
        nga.field('bankName'),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')

    ];
    //payment.listView().fields(fieldSet6);
    payment.listView().sortField('status').fields(fieldSet6).filters([
        nga.field('paymentType'),
        nga.field('paymentStatus'),
        nga.field('type')
    ]);
    payment.creationView().fields(fieldSet6);
    payment.editionView().fields(fieldSet6);
    admin.addEntity(payment);


    var fieldSet8 = [
        //nga.field('id'),
        nga.field('name'),
        nga.field('required', 'boolean')
            .choices([
                {value: null, label: 'not yet decided'},
                {value: true, label: 'enabled'},
                {value: false, label: 'disabled'}
            ]),
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ];
    //sellerservices.listView().fields(fieldSet8);
    sellerservices.listView().sortField('name').fields(fieldSet8).filters([
        nga.field('name')
    ]);
    sellerservices.creationView().fields(fieldSet8);
    sellerservices.editionView().fields(fieldSet8);
    admin.addEntity(sellerservices);


    ////Frames


    var fieldSet4 = [
        //nga.field('id'),
        nga.field('name'),
        nga.field('imageDescription').label('Image Description'),
        nga.field('image.uploadimagename').label('Image Url'),
        //.template('<img src="https://s3-us-west-2.amazonaws.com/ouroneapp/"+{{ entry.values.image.uploadimagename }}  height="42" width="42" />'),
        //    .template('<img src="https://s3-us-west-2.amazonaws.com/ouroneapp/{{entry.values.image.uploadimagename}}" height="42" width="42" />'),entry.values.s3imageUrl

        //nga.field('sellerId'),
        //nga.field('suburl.submainurl', 'template').label('Image')
        //    .template('<img src="https://s3-us-west-2.amazonaws.com/ouroneapp/entry.values[field.name(suburl.submainurl)]" height="42" width="42" />'),
        nga.field('sellerId', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName')),
        nga.field('sellerId', 'reference')
            .isDetailLink(false)
            .label('Seller')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('sellerId', 'reference')
            .isDetailLink(false)
            .label('BusinessTypeName')
            .targetEntity(seller)
            .targetField(nga.field('businessTypeName')),
        nga.field('item price')
        //nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ];
    /*var framesFieldSet=[
     //nga.field('id'),
     nga.field('name'),
     nga.field('imageDescription').label('Image Description'),
     nga.field('sellerId', 'reference')
     .isDetailLink(false)
     .label('Business Name')
     .targetEntity(seller)
     .targetField(nga.field('businessName')),
     nga.field('sellerId', 'reference')
     .isDetailLink(false)
     .label('Seller')
     .targetEntity(seller)
     .targetField(nga.field('name'))
     //nga.field('item price'),



     ];*/
    //frames.listView().fields(fieldSet4).perPage(10);
    frames.listView().sortField('name').fields(fieldSet4, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('FrameName'),
        nga.field('sellerId_id', 'reference')
            .isDetailLink(false)
            .label('Seller')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('sellerId_id', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName')),
        nga.field('sellerId_id', 'reference')
            .isDetailLink(false)
            .label('BusinessTypeName')
            .targetEntity(seller)
            .targetField(nga.field('businessTypeName'))
    ]);
    frames.creationView().fields(fieldSet4);
    frames.editionView().fields(fieldSet4).title('Edit :{{ entry.values.name }}');
    admin.addEntity(frames);

    ////Buyers

    //var buyers = nga.entity('Buyers');
    var fieldSet2 = [
        //nga.field('id'),
        nga.field('name'),
        nga.field('phone', 'template').label('Phone')
            .isDetailLink(true)
            .template('{{ entry.values.phone }}'),
        nga.field('registeredDate', 'date'),
        nga.field('gcm_flag', 'choice')
            .choices([
                {value: true, label: 'true'},
                {value: false, label: 'false'}
            ]),

        nga.field('referral.points', 'number').format('0,0.00'),
        nga.field('referral.amount', 'number').format('0,0.00'),
        nga.field('referral.totalPoints', 'number').format('0,0.00'),
        nga.field('loyalty.totalPoints', 'number').format('0,0.00'),
        nga.field('loyalty.points', 'number').format('0,0.00'),
        nga.field('loyalty.amount', 'number').format('0,0.00')
        //nga.field('registerdate','date').format('dd-MMM-yyyy'),
        //nga.field('createdTime')

        /*nga.field('SubscriptionsList', 'referenced_list') // display list of related comments
         .targetEntity(nga.entity('Subscriptions'))
         .targetReferenceField('sellerId')
         .targetFields([
         nga.field('id')
         ]),*/


    ];
    buyers.listView().sortField('createdTime').fields(fieldSet2, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        nga.field('name'),
        nga.field('phone'),
        nga.field('RegisteredDate', 'date'),
        nga.field('createdTime')
    ]);
    //buyers.listView();
    buyers.creationView().fields(fieldSet2);
    buyers.editionView().title('Edit :{{ entry.values.name }}').fields(fieldSet2, [
        nga.field('SellersList', 'referenced_list') // display list of related comments
            .targetEntity(nga.entity('Subscriptions'))
            .targetReferenceField('buyerId')
            .targetFields([
                //nga.field('sellerId').isDetailLink(true)
                nga.field('sellerId', 'reference')
                    .isDetailLink(false)
                    .label('Seller Name')
                    .targetEntity(seller)
                    .targetField(nga.field('name')).isDetailLink(true),
                nga.field('sellerId', 'reference')
                    .isDetailLink(false)
                    .label('Business Name')
                    .targetEntity(seller)
                    .targetField(nga.field('businessName')).isDetailLink(true),
                nga.field('sellerId', 'reference')
                    .isDetailLink(false)
                    .label('Phone Number')
                    .targetEntity(seller)
                    .targetField(nga.field('phone')).isDetailLink(true)


            ])
    ]);
    admin.addEntity(buyers);


    var fieldSet9 = [
        //nga.field('id'),
        nga.field('buyerId', 'reference')
            .isDetailLink(false)
            .label('BuyerName')
            .targetEntity(buyers)
            .targetField(nga.field('name')),
        nga.field('buyerId', 'reference')
            .isDetailLink(false)
            .label('BuyerPhone')
            .targetEntity(buyers)
            .targetField(nga.field('phone')),
        nga.field('sellerId', 'reference')
            .isDetailLink(false)
            .label('SellerName')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('sellerId', 'reference')
            .isDetailLink(false)
            .label('SellerPhone')
            .targetEntity(seller)
            .targetField(nga.field('phone')),
        nga.field('sellerId', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName')),
        nga.field('businessTypeName'),
        //nga.field('seller.name').label("Seller Name1"),
        /*nga.field('businessTypeId','reference')
         .isDetailLink(false)
         .label('BusinessDetails')
         .targetEntity(business)
         .targetField(nga.field('name')),*/

        //nga.field('seller.phone').label("Seller Phone"),

        //nga.field('seller.name').label("Seller Name"),
        /*nga.field('sellerservicelist','json'),
         nga.field('seller.paymentStatus').label("PaymentStatus"),
         nga.field('seller.paymodedesc').label("Paymodedesc"),
         nga.field('seller.subScribedDate','date').defaultValue(new Date()).label('SubScribedDate'),
         nga.field('seller.paymentStatusAdmin', 'choice').label('PaymentStatus')
         .choices([
         { value: 'true', label: 'true' },
         { value: 'false', label: 'false'}
         ]),
         nga.field('seller.Referral.name').label('RefferalName'),*/
        nga.field('priceRequestStatus').label("Price Request Status")
        /*nga.field('status'),
         nga.field('referral.points'),
         nga.field('referral.amount'),
         nga.field('referral.totalPoints')*/

    ];
    //subscriptions.listView().fields(fieldSet9);
    subscriptions.listView().sortField('name').fields(fieldSet9, [
        nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
    ]).filters([
        //nga.field('seller.name').label("SellerName"),
        /* nga.field('seller.phone').label("SellerPhone"),
         nga.field('seller.businessName').label("BusinessName"),
         nga.field('businessTypeName').label("BusinessTypeName"),
         nga.field('buyer.name').label("BuyerName"),
         nga.field('buyer.phone').label("BuyerPhone"),
         nga.field('seller.paymodedesc').label("Paymodedesc"),
         nga.field('seller.subScribedDate','date').defaultValue(new Date()).label('SubScribedDate'),
         nga.field('seller.paymentStatusAdmin', 'choice').label('PaymentStatus')
         .choices([
         { value: 'true', label: 'true' },
         { value: 'false', label: 'false'}
         ]),
         nga.field('seller.Referral.name').label('RefferalName')*/
        nga.field('businessTypeName').label('BusinessType Name'),
        nga.field('sellerId_id', 'reference')
            .isDetailLink(false)
            .label('Seller Name')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('sellerId_id', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName')),
        nga.field('sellerId_id', 'reference')
            .isDetailLink(false)
            .label('Seller Phone')
            .targetEntity(seller)
            .targetField(nga.field('phone')),
        nga.field('buyerId', 'reference')
            .isDetailLink(false)
            .label('BuyerName')
            .targetEntity(buyers)
            .targetField(nga.field('name')),
        nga.field('buyerId', 'reference')
            .isDetailLink(false)
            .label('BuyerPhone')
            .targetEntity(buyers)
            .targetField(nga.field('phone'))
    ]);
    subscriptions.creationView().fields(fieldSet9);
    subscriptions.editionView().title('Edit Subscriptions Details').fields(fieldSet9);
    admin.addEntity(subscriptions);


    var fieldSet22 = [
        //nga.field('id'),

        nga.field('name').label('Name'),
        nga.field('userId').label("Phone"),
        nga.field('userType').label('UserType'),
        nga.field('businessName').label('BusinessName'),
        nga.field('businessTypeName').label('BusinessTypeName')
        //nga.field('email').label('email')
        //nga.field('seller.name').label('First'),
        //nga.field('buyer.name').label('Buyer Name'),
        /* nga.field('uid','reference')
         .isDetailLink(false)
         .label('Buyer Name')
         .targetEntity(buyers)
         .targetField(nga.field('name')),*/
        /*nga.field('uid','reference')
         .isDetailLink(false)
         .label('Seller Name')
         .targetEntity(seller)
         .targetField(nga.field('name')),
         nga.field('uid','reference')
         .isDetailLink(false)
         .label('Business Name')
         .targetEntity(seller)
         .targetField(nga.field('businessName')),*/

    ];

    accounts.listView().sortField('name').fields(fieldSet22, [
            nga.field('actions', 'template').template('<ma-edit-button entry="entry" entity="entity" size="xs"></ma-edit-button>')
        ]
    ).filters([

        nga.field('userType', 'choice').label('user Type')
            .choices([
                {value: "Buyer", label: 'Buyer'},
                {value: "Seller", label: 'Seller'}
            ]),
        nga.field('name').label('Name'),
        nga.field('userId').label("Phone"),
        //nga.field('userType').label('User Type'),
        nga.field('businessName').label('Business Name'),
        nga.field('businessTypeName').label('Business Type Name')


    ]);
    //accounts.creationView().fields(fieldSet22);
    accounts.editionView().title('Edit: {{ entry.values.userId }}').fields(/*fieldSet22*/[
        nga.field('userId').label("Phone"),
        nga.field('userType'),
        nga.field('seller.name').label('First'),
        nga.field('buyer.name').label('Buyer Name'),
        /* nga.field('uid','reference')
         .isDetailLink(false)
         .label('Buyer Name')
         .targetEntity(buyers)
         .targetField(nga.field('name')),*/
        nga.field('businessName'),
        nga.field('uid', 'reference')
            .isDetailLink(false)
            .label('Seller Name')
            .targetEntity(seller)
            .targetField(nga.field('name')),
        nga.field('uid', 'reference')
            .isDetailLink(false)
            .label('Business Name')
            .targetEntity(seller)
            .targetField(nga.field('businessName'))
    ]);
    /*  accounts.showView().fields([
     nga.field('userId').label("Phone"),
     nga.field('userType'),
     nga.field('uid','reference')
     .isDetailLink(false)
     .label('Buyer Name')
     .targetEntity(buyers)
     .targetField(nga.field('name'))
     /!*nga.field('uid','reference')
     .isDetailLink(false)
     .label('Seller Name')
     .targetEntity(seller)
     .targetField(nga.field('name')),
     nga.field('uid','reference')
     .isDetailLink(false)
     .label('Business Name')
     .targetEntity(seller)
     .targetField(nga.field('businessName'))*!/

     ]);*/
    admin.addEntity(accounts);

    nga.configure(admin);
}]);


/*// seller Details
 myApp.config(function ($stateProvider) {
 $stateProvider.state('sellerDetails', {
 parent: 'main',
 url: '/sellerDetails',
 params: {},
 controller: 'sellerDetailsController',
 templateUrl: 'sellerDet.html'
 });
 });
 myApp.controller('sellerDetailsController',function($scope,Restangular) {

 //var sellersList=new Array();
 var subscriptionList=new Array();
 Restangular
 .setBaseUrl('http://ouroneapp.com/api')
 //.setBaseUrl('http://104.196.99.177:7896/api')
 .all('sellers')
 .getList()
 .then(function(sellers) {
 //console.log("sellerLength: "+sellers.data.length);

 for (var j = 0; j < sellers.data.length; j++) {
 var sellerInfo = sellers.data[j];
 var sellerId=sellerInfo.id;

 var subscriptionJson = {};
 subscriptionJson['sellerDt'] = sellerInfo;
 subscriptionJson['subscriptionDet'] = [];

 Restangular
 .setBaseUrl('http://ouroneapp.com/api')
 //.setBaseUrl('http://104.196.99.177:7896/api')
 .all('Subscriptions')
 .getList({_filters: {"sellerId_id":sellerId}})
 .then(function (Subscriptions) {
 //console.log("subscriptionLength: " + Subscriptions.data.length);
 //console.log("subscriptionJson: " + JSON.stringify(Subscriptions.data));
 subscriptionJson['subscriptionDet'].push(Subscriptions.data);
 // subscriptionList.push(Subscriptions.data);
 subscriptionList.push(subscriptionJson);
 console.log("subscriptionList: "+JSON.stringify(subscriptionList.length));

 });
 }

 });
 console.log("subscriptionList length: "+JSON.stringify(subscriptionList.length));
 $scope.subDetails = subscriptionList;

 });*/

// start

myApp.config(function ($stateProvider) {
    $stateProvider.state('sellerDetails', {
        parent: 'main',
        url: '/sellerDetails',
        params: {},
        controller: 'sellerDetailsController',
        templateUrl: 'sellerDet.html'
    });
});


myApp.controller('sellerDetailsController', function ($scope, Restangular, Excel, $timeout) {
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }
    //var buyersCount=new Array();
    var subscriptionList = new Array();
    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:3636/api')
        .all('sellers')
        .getList()
        .then(function (sellers) {
            //console.log("sellerDetailsLength: " + sellers.data.length);
            //for (var i = 0; i < sellers.data.length; i++) {
            var buyersCount = function (i) {
                var sellerDetId = sellers.data[i].id;
                console.log("sellerDetId: " + sellerDetId);
                var buyersJson = {};
                var sellersInformation = sellers.data[i];
                //var buyersCount = 0;
                Restangular
                    .setBaseUrl('http://ouroneapp.com/api')
                    //.setBaseUrl('http://104.196.99.177:3636/api')
                    .all('Subscriptions')
                    .getList({_filters: {"sellerId_id": sellerDetId}})
                    .then(function (Subscriptions) {
                        console.log("buyersCount: " + Subscriptions.data);
                        buyersJson['sellersDetails'] = sellersInformation;
                        buyersJson['buyerDetails'] = Subscriptions.data;
                        buyersJson['buyersCount'] = Subscriptions.data.length;
                        subscriptionList.push(buyersJson);
                        if (i < sellers.data.length) {
                            i++;
                            buyersCount(i);
                        }
                        //buyersCount.push(Subscriptions.data.length);
                    });
            };
            buyersCount(0);
            //$scope.noOfBuyers = buyersCount;
            //$scope.sellerDet = sellers.data;
            $scope.sellerDet = subscriptionList;
        });
});
myApp.config(function ($stateProvider) {
    $stateProvider.state('sellerInformation', {
        parent: 'main',
        url: '/{sellerId}/{sellerName}/sellerInformation',
        params: {},
        controller: 'subscribedBuyersOfSellerController',
        templateUrl: 'subscribedBuyersOfSeller.html'
    });
});


myApp.controller('subscribedBuyersOfSellerController', function ($stateParams, $scope, Restangular, Excel, $timeout) {
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }
    console.log("sellerId: " + $stateParams.sellerId);
    var sellerName = $stateParams.sellerName;
    var sellerId = $stateParams.sellerId;
    var framesDet;
    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:3636/api')
        .all('Subscriptions')
        .getList({_filters: {"sellerId_id": sellerId}})
        .then(function (Subscriptions) {

            console.log("Subscriptions: " + Subscriptions.data.length);
            console.log("framesDetails: " + JSON.stringify(framesDet));
            $scope.framesDetails = framesDet;
            $scope.buyersCount = Subscriptions.data.length;
            $scope.buyerInformation = Subscriptions.data;
            $scope.sellerName = sellerName;
        });
    /*Restangular
     .setBaseUrl('http://ouroneapp.com/api')
     //.setBaseUrl('http://104.196.99.177:3636/api')
     .all('frames')
     .getList({_filters: {"sellerId_id": sellerId}})
     .then(function (Frames) {
     console.log("frames length: " + Frames.data.length);
     framesDet=Frames.data;
     console.log("frames data: " + JSON.stringify(Frames.data));
     $scope.framesDetails=framesDet;
     $scope.width=200;
     $scope.height=200;

     });*/

});

// end


// start of business

myApp.config(function ($stateProvider) {
    $stateProvider.state('businessDetails', {
        parent: 'main',
        url: '/businessDetails',
        params: {},
        controller: 'businessDetailsController',
        templateUrl: 'businessDetails.html'
    });
});


myApp.controller('businessDetailsController', function ($scope, Restangular, Excel, $timeout) {

    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }
    //var buyersCount=new Array();
    var subscriptionList = new Array();
    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:7896/api')
        .all('businesses')
        .getList()
        .then(function (businesses) {
            console.log("businessesLength121: " + JSON.stringify(businesses.data));
            //for (var i = 0; i < sellers.data.length; i++) {
            var sellersCount = function (i) {
                var businessDetId = businesses.data[i].id;
                //var businessestotalLength=businesses.data.length;
                console.log("businessDetId: " + businessDetId);
                var sellersJson = {};
                var businessesInformation = businesses.data[i];
                //var buyersCount = 0;
                Restangular
                    .setBaseUrl('http://ouroneapp.com/api')
                    //.setBaseUrl('http://104.196.99.177:7896/api')
                    .all('sellers')
                    .getList({_filters: {"businessTypeId_id": businessDetId}})
                    .then(function (sellers) {
                        console.log("sellersCount: " + sellers.data.length);
                        sellersJson['businessDetails'] = businessesInformation;
                        sellersJson['sellersCount'] = sellers.data.length;
                        subscriptionList.push(sellersJson);
                        if (i < (businesses.data.length - 1)) {
                            i++;
                            sellersCount(i);
                        }
                        //buyersCount.push(Subscriptions.data.length);
                    });
            };
            sellersCount(0);
            //$scope.noOfBuyers = buyersCount;
            //$scope.sellerDet = sellers.data;
            //$scope.businessDetails = businesses.data;
            $scope.businessDetails = subscriptionList;
        });
});

myApp.config(function ($stateProvider) {
    $stateProvider.state('businessInformation', {
        parent: 'main',
        url: '/{businessId}/{businessName}/businessInformation',
        params: {},
        controller: 'sellersOfBusinessController',
        templateUrl: 'sellersOfBusiness.html'
    });
});


myApp.controller('sellersOfBusinessController', function ($stateParams, $scope, Restangular, Excel, $timeout) {
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }
    console.log("businessId: " + $stateParams.businessId);
    var businessName = $stateParams.businessName;
    var businessId = $stateParams.businessId;
    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:7896/api')
        .all('sellers')
        .getList({_filters: {"businessTypeId_id": businessId}})
        .then(function (sellers) {
            console.log("sellersLength: " + sellers.data.length);
            $scope.sellersCount = sellers.data.length;
            $scope.sellerInformation = sellers.data;
            $scope.businessName = businessName;
        });

});


// end

/*// start of sales man reports

 myApp.config(function ($stateProvider) {
 $stateProvider.state('salesMansRefferalDet', {
 parent: 'main',
 url: '/salesMansRefferalDet',
 params: {},
 controller: 'salesMansRefferalController',
 templateUrl: 'salesMansRefferalDet.html'
 });
 });


 myApp.controller('salesMansRefferalController', function ($scope, Restangular) {
 var salesMansRefferalDet = [];
 Restangular
 .setBaseUrl('http://ouroneapp.com/api')
 //.setBaseUrl('http://104.196.99.177:7896/api')
 .all('sellers')
 .getList()
 .then(function (sellers) {
 console.log("sellersCount2323: " + sellers.data.length);

 Restangular
 .setBaseUrl('http://ouroneapp.com/api')
 //.setBaseUrl('http://104.196.99.177:7896/api')
 .all('SalesMans')
 .getList()
 .then(function (SalesMans) {
 console.log("SalesMansSize: " + SalesMans.data.length);


 /!*var SalesMansRefferalCount = function(i) {*!/
 for(var i=0;i<SalesMans.data.length;i++){
 var salesManDet = SalesMans.data[i];
 var salesManJson = {};
 salesManJson['salesMansDet'] = salesManDet;
 var referralAmount500Count = 0;
 var referralAmount1200Count = 0;
 var referralAmount1800Count = 0;
 for (var j = 0; j < sellers.data.length; j++) {
 var sellerDet = sellers.data[j];
 console.log("sellerDetReferralphone: " + sellerDet.Referral.contact);
 console.log("salesManContactNo: " + salesManDet.phone);
 if (sellerDet.Referral.contact == salesManDet.phone) {
 console.log("sellerDetReferralphone: " + sellerDet.Referral.contact);
 if (sellerDet.subsrcibeamount == "500.0") {
 referralAmount500Count = referralAmount500Count + 1;
 } else if (sellerDet.subsrcibeamount == "1200.0") {
 referralAmount1200Count = referralAmount1200Count + 1;
 } else if (sellerDet.subsrcibeamount == "1800.0") {
 referralAmount1800Count = referralAmount1800Count + 1;
 }
 }
 }
 console.log("referralAmount500Count: " + referralAmount500Count);
 console.log("referralAmount1200Count: " + referralAmount1200Count);
 console.log("referralAmount1800Count: " + referralAmount1800Count);
 salesManJson['referralAmount5Count'] = referralAmount500Count;
 salesManJson['referralAmount12Count'] = referralAmount1200Count;
 salesManJson['referralAmount18Count'] = referralAmount1800Count;
 salesMansRefferalDet.push(salesManJson);
 /!*if( i < SalesMans.data.length){
 i++;
 SalesMansRefferalCount(i);
 }*!/
 }
 //SalesMansRefferalCount(0);
 //buyersCount.push(Subscriptions.data.length);
 $scope.salesmanDetails = salesMansRefferalDet;
 });

 });

 });*/


// start of sales man reports
myApp.config(function ($stateProvider) {
    $stateProvider.state('salesMansRefferalDet', {
        parent: 'main',
        url: '/salesMansRefferalDet',
        params: {},
        controller: 'salesMansRefferalController',
        templateUrl: 'salesMansRefferalDet.html'
    });
});

myApp.controller('salesMansRefferalController', function (Restangular, $scope, $filter, Excel, $timeout) {


    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }
    //multi datepicker start

    $('#startDatePicker').multiDatesPicker({
        dateFormat: "yy-mm-dd",
        maxPicks: 1
    });
    $('#endDatePicker').multiDatesPicker({
        dateFormat: "yy-mm-dd",
        maxPicks: 1
    });

    //multi datepicker end


    $scope.submit = function () {
        if (document.getElementById("startDatePicker").value == "") {
            $('#errMsg').css("color", "red");
            $("#errMsg").text("Please select start date");
        } else if (document.getElementById("endDatePicker").value == "") {
            $('#errMsg').css("color", "red");
            $("#errMsg").text("Please select end date");
        } else {
            $("#errMsg").text("");
            var betDates = document.getElementById("startDatePicker").value + ',' + document.getElementById("endDatePicker").value;
            var salesMansRefferalDet = [];
            Restangular
                .setBaseUrl('http://ouroneapp.com/api')
                //.setBaseUrl('http://104.196.99.177:7896/api')
                .all('sellers')
                .getList({_filters: {"subScribedDate_between": betDates}})
                .then(function (sellers) {
                    //console.log("sellersCount2323: " + sellers.data.length);

                    Restangular
                        .setBaseUrl('http://ouroneapp.com/api')
                        //.setBaseUrl('http://104.196.99.177:7896/api')
                        .all('SalesMans')
                        .getList()
                        .then(function (SalesMans) {
                            console.log("countofsellers: " + sellers.data.length);
                            //console.log("SalesMansSize: " + SalesMans.data.length);
                            for (var i = 0; i < SalesMans.data.length; i++) {
                                var salesManDet = SalesMans.data[i];
                                var salesManJson = {};
                                salesManJson['salesMansDet'] = salesManDet;
                                var registrationsCountWithOutReferral500 = 0;
                                var registrationsAmountWithOutReferral500 = 0;
                                var registrationsCountWithOutReferral1000 = 0;
                                var registrationsAmountWithOutReferral1000 = 0;
                                var registrationsCountWithOutReferral1200 = 0;
                                var registrationsAmountWithOutReferral1200 = 0;
                                var registrationsCountWithOutReferral1800 = 0;
                                var registrationsAmountWithOutReferral1800 = 0;
                                var registrationsCountWithOutReferralZero = 0;
                                var referralAmount500Count = 0;
                                var referralAmount1200Count = 0;
                                var referralAmount1800Count = 0;
                                var referralAmount1000Count = 0;
                                var referralAmount0Count = 0;
                                var Total500referralAmount = 0;
                                var Total1200referralAmount = 0;
                                var Total1800referralAmount = 0;
                                var Total1000referralAmount = 0;
                                var totalReferralCount = 0;
                                var totalReferralAmount = 0;
                                for (var j = 0; j < sellers.data.length; j++) {
                                    var sellerDet = sellers.data[j];

                                    //console.log("sellerDetReferralphone: " + sellerDet.Referral.contact);
                                    //console.log("salesManContactNo: " + salesManDet.phone);
                                    //if (sellerDet.Referral.contact){
                                    if (i + 1 == SalesMans.data.length) {
                                        if (sellerDet.Referral.contact == "0000000000" || sellerDet.Referral.contact == "NA") {
                                            if (sellerDet.subsrcibeamount == "500.0") {
                                                registrationsCountWithOutReferral500 = registrationsCountWithOutReferral500 + 1;
                                                registrationsAmountWithOutReferral500 = Number(registrationsAmountWithOutReferral500) + 500;
                                            } else if (sellerDet.subsrcibeamount == "1200.0") {
                                                registrationsCountWithOutReferral1200 = registrationsCountWithOutReferral1200 + 1;
                                                registrationsAmountWithOutReferral1200 = Number(registrationsAmountWithOutReferral1200) + 1200;
                                            } else if (sellerDet.subsrcibeamount == "1800.0") {
                                                registrationsCountWithOutReferral1800 = registrationsCountWithOutReferral1800 + 1;
                                                registrationsAmountWithOutReferral1800 = Number(registrationsAmountWithOutReferral1800) + 1800;
                                            } else if (sellerDet.subsrcibeamount == "1000.0") {
                                                registrationsCountWithOutReferral1000 = registrationsCountWithOutReferral1000 + 1;
                                                registrationsAmountWithOutReferral1000 = Number(registrationsAmountWithOutReferral1000) + 1000;
                                            } else if (sellerDet.subsrcibeamount == "0.00") {
                                                console.log("registrationsCountWithOutReferralZero: " + registrationsCountWithOutReferralZero);
                                                registrationsCountWithOutReferralZero = registrationsCountWithOutReferralZero + 1;
                                            }
                                        }

                                    } else if (sellerDet.Referral.contact == salesManDet.phone) {
                                        console.log("MATCHED");
                                        if (sellerDet.subsrcibeamount == "500.0") {
                                            referralAmount500Count = referralAmount500Count + 1;
                                            Total500referralAmount = Number(Total500referralAmount) + 500;
                                        } else if (sellerDet.subsrcibeamount == "1200.0") {
                                            referralAmount1200Count = referralAmount1200Count + 1;
                                            Total1200referralAmount = Number(Total1200referralAmount) + 1200;
                                        } else if (sellerDet.subsrcibeamount == "1800.0") {
                                            referralAmount1800Count = referralAmount1800Count + 1;
                                            Total1800referralAmount = Number(Total1800referralAmount) + 1800;
                                        } else if (sellerDet.subsrcibeamount == "1000.0") {
                                            referralAmount1000Count = referralAmount1000Count + 1;
                                            Total1000referralAmount = Number(Total1000referralAmount) + 1000;
                                        } else if (sellerDet.subsrcibeamount == "0.00") {
                                            referralAmount0Count = referralAmount0Count + 1;
                                        }
                                    }
                                    //}
                                }


                                //console.log("referralAmount500Count: " + referralAmount500Count);
                                //console.log("referralAmount1200Count: " + referralAmount1200Count);
                                //console.log("referralAmount1800Count: " + referralAmount1800Count);
                                totalReferralCount = referralAmount500Count + referralAmount1200Count + referralAmount1800Count + referralAmount0Count + referralAmount1000Count;
                                totalReferralAmount = Total500referralAmount + Total1200referralAmount + Total1800referralAmount + Total1000referralAmount;
                                salesManJson['referralAmount5Count'] = referralAmount500Count;
                                salesManJson['referralAmount12Count'] = referralAmount1200Count;
                                salesManJson['referralAmount18Count'] = referralAmount1800Count;
                                salesManJson['referralAmount10Count'] = referralAmount1000Count;
                                salesManJson['referralAmount0Count'] = referralAmount0Count;
                                salesManJson['totalReferralCount'] = totalReferralCount;
                                salesManJson['totalReferralAmount'] = totalReferralAmount;
                                salesMansRefferalDet.push(salesManJson);
                            }
                            $scope.salesmanDetails = salesMansRefferalDet;
                            $scope.registrationsCountWithOutReferral500 = registrationsCountWithOutReferral500;
                            $scope.registrationsAmountWithOutReferral500 = registrationsAmountWithOutReferral500;
                            $scope.registrationsCountWithOutReferral1000 = registrationsCountWithOutReferral1000;
                            $scope.registrationsAmountWithOutReferral1000 = registrationsAmountWithOutReferral1000;
                            $scope.registrationsCountWithOutReferral1200 = registrationsCountWithOutReferral1200;
                            $scope.registrationsAmountWithOutReferral1200 = registrationsAmountWithOutReferral1200;
                            $scope.registrationsCountWithOutReferral1800 = registrationsCountWithOutReferral1800;
                            $scope.registrationsAmountWithOutReferral1800 = registrationsAmountWithOutReferral1800;
                            $scope.totalRegistrationsCountWithOutReferral = registrationsCountWithOutReferral500 + registrationsCountWithOutReferral1000
                                + registrationsCountWithOutReferral1200 + registrationsCountWithOutReferral1800 + registrationsCountWithOutReferralZero;
                            $scope.totalRegistrationsAmountWithOutReferral = registrationsAmountWithOutReferral500 + registrationsAmountWithOutReferral1000
                                + registrationsAmountWithOutReferral1200 + registrationsAmountWithOutReferral1800;
                            $scope.registrationsCountWithOutReferralZero = registrationsCountWithOutReferralZero;
                        });

                });
        }
    };

});

// end of sales man reports

// start of frames display of seller dashboard

myApp.config(function ($stateProvider) {
    $stateProvider.state('sellersInfo', {
        parent: 'main',
        url: '/sellersInfo',
        params: {},
        controller: 'sellersInfo',
        templateUrl: 'sellersInfo.html'
    });
});


myApp.controller('sellersInfo', function ($scope, Restangular, $timeout, Excel) {
    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }

    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:3636/api')
        .all('sellers')
        .getList()
        .then(function (sellers) {
            var sellersInformation = sellers.data;
            $scope.sellerDetails = sellersInformation;
        });
});
myApp.config(function ($stateProvider) {
    $stateProvider.state('framesInformation', {
        parent: 'main',
        url: '/{sellerId}/{sellerName}/framesInformation',
        params: {},
        controller: 'framesOfSeller',
        templateUrl: 'framesOfSeller.html'
    });
});


myApp.controller('framesOfSeller', function ($stateParams, $scope, Restangular, Excel, $timeout) {

    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'sheet name');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }
    console.log("sellerId: " + $stateParams.sellerId);
    var sellerName = $stateParams.sellerName;
    var sellerId = $stateParams.sellerId;
    var framesDet;
    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:3636/api')
        .all('frames')
        .getList({_filters: {"sellerId_id": sellerId}})
        .then(function (Frames) {
            console.log("frames length: " + Frames.data.length);
            framesDet = Frames.data;
            console.log("frames data: " + JSON.stringify(Frames.data));
            $scope.framesDetails = framesDet;
            $scope.width = 200;
            $scope.height = 200;
            $scope.sellerName = sellerName;
            $scope.sellerId = sellerId;

        });

});

// end

// start of buyer details with subscribed sellers

myApp.config(function ($stateProvider) {
    $stateProvider.state('buyerDetails', {
        parent: 'main',
        url: '/buyerDetails',
        params: {},
        controller: 'buyerDetailsController',
        templateUrl: 'buyerDetails.html'
    });
});


myApp.controller('buyerDetailsController', function ($scope, Restangular, Excel, $timeout) {

    $scope.exportToExcel = function (tableId) { // ex: '#my-table'
        var exportHref = Excel.tableToExcel(tableId, 'buyerDetails');
        $timeout(function () {
            location.href = exportHref;
        }, 100); // trigger download
    }

    //var buyersCount=new Array();
    var subscriptionList = new Array();
    Restangular
        .setBaseUrl('http://ouroneapp.com/api')
        //.setBaseUrl('http://104.196.99.177:3636/api')
        .all('buyers')
        .getList()
        .then(function (buyers) {
            var buyersCount = function (i) {
                var buyerDetId = buyers.data[i].id;
                console.log("buyerDetId: " + buyerDetId);
                var buyersJson = {};
                var buyersInformation = buyers.data[i];
                //var buyersCount = 0;
                Restangular
                    .setBaseUrl('http://ouroneapp.com/api')
                    //.setBaseUrl('http://104.196.99.177:3636/api')
                    .all('Subscriptions')
                    .getList({_filters: {"buyerId_id": buyerDetId}})
                    .then(function (Subscriptions) {
                        console.log("sellersCount: " + Subscriptions.data.length);
                        buyersJson['buyersDetails'] = buyersInformation;
                        buyersJson['sellersDetails'] = Subscriptions.data;
                        buyersJson['sellersCount'] = Subscriptions.data.length;
                        subscriptionList.push(buyersJson);
                        if (i < buyers.data.length) {
                            i++;
                            buyersCount(i);
                        }
                        //buyersCount.push(Subscriptions.data.length);
                    });
            };
            buyersCount(0);
            //$scope.noOfBuyers = buyersCount;
            //$scope.sellerDet = sellers.data;
            $scope.buyerDet = subscriptionList;
        });
});
/*myApp.config(function ($stateProvider) {
 $stateProvider.state('buyerInformation', {
 parent: 'main',
 url: '/{buyerId}/{buyerName}/buyerInformation',
 params: {},
 controller: 'buyerSubscribedSellersController',
 templateUrl: 'buyersSubscribedSellers.html'
 });
 });



 myApp.controller('buyerSubscribedSellersController',function($stateParams,$scope,Restangular){
 console.log("buyerId: "+$stateParams.buyerId);
 var buyerName=$stateParams.buyerName;
 var buyerId=$stateParams.buyerId;
 var framesDet;
 Restangular
 .setBaseUrl('http://ouroneapp.com/api')
 //.setBaseUrl('http://104.196.99.177:3636/api')
 .all('Subscriptions')
 .getList({_filters: {"buyerId_id":buyerId}})
 .then(function(Subscriptions) {

 console.log("Subscriptions: "+Subscriptions.data.length);
 console.log("framesDetails: "+JSON.stringify(framesDet));
 $scope.framesDetails=framesDet;
 $scope.buyersCount=Subscriptions.data.length;
 $scope.buyerInformation=Subscriptions.data;
 $scope.sellerName=sellerName;
 });


 });*/

// end


myApp.factory('Excel', function ($window) {
    var uri = 'data:application/vnd.ms-excel;base64,',
        template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
        base64 = function (s) {
            return $window.btoa(unescape(encodeURIComponent(s)));
        },
        format = function (s, c) {
            return s.replace(/{(\w+)}/g, function (m, p) {
                return c[p];
            })
        };
    return {
        tableToExcel: function (tableId, worksheetName) {
            var table = $(tableId),
                ctx = {worksheet: worksheetName, table: table.html()},
                href = uri + base64(format(template, ctx));
            return href;
        }
    };
});
