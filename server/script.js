/**
 * Created by Chakradhar on 1/9/2016.
 */
var ds = app.dataSources.db;
BusinessModel = ds.createModel('business', {
    _id: { type: ds.ObjectID, id: true }
});


SellerModel = ds.createModel('seller', {
    _id: { type: ds.ObjectID, id: true }
});