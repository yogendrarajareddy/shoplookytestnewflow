module.exports = function (server) {


    var Business = server.models.Business;
    var remotes = server.remotes();
    // Set X-Total-Count for all search requests
    remotes.after('*.find', function (ctx, next) {
        var filter;
        if (ctx.args && ctx.args.filter) {
            filter = JSON.parse(ctx.args.filter).where;
        }

        if (!ctx.res._headerSent) {
            this.count(filter, function (err, count) {
                ctx.res.set('X-Total-Count', count);
                ctx.res.set('Access-Control-Expose-Headers', 'x-total-count');
                next();
            });
        } else {
            next();
        }
    });

    //x-total count end
    var Account = server.models.Account;
    var Frame = server.models.Frame;
    var Seller = server.models.Seller;
    var Buyer = server.models.Buyer;
    var Subscription = server.models.Subscription;
    var merge = require('merge');
    var extend = require('extend');
    var find = Seller.find;


    remotes.before('**', function (ctx, next) {
        //var loyaltyPasscode="found";
        console.log(ctx.req.originalUrl);
        console.log(ctx.req.method.toString());
        if ((ctx.req.baseUrl.toString() == '/api/Sellers' || ctx.req.baseUrl.toString() == '/api/Buyers') && ctx.req.method.toString() == 'PUT') {
            if (ctx.req.body.email != undefined) {
                ctx.req.body.mail = ctx.req.body.email;
                delete ctx.req.body.email;
            }
            if (ctx.req.baseUrl.toString() == '/api/Sellers') {
                // console.log("ctx.req.body.sellerservicelist1111: " + JSON.stringify(ctx));
                if (ctx.req.body.sellerservicelist != undefined) {
                    var sellerServicesList = ctx.req.body.sellerservicelist;
                    //console.log("ctx.req.body.sellerservicelist: "+JSON.stringify(ctx.req.body.sellerservicelist));
                    if (sellerServicesList.length > 0) {
                        for (var i = 0; i < sellerServicesList.length; i++) {
                            var sellerServicesData = sellerServicesList[i];
                            if (sellerServicesData.required == 1 || sellerServicesData.required == "1") {
                                sellerServicesData.required = true;
                                console.log("sellerrequired: " + sellerServicesData.required);
                            } else if (sellerServicesData.required == 0 || sellerServicesData.required == "0") {
                                sellerServicesData.required = false;
                                console.log("sellerrequiredfalse: " + sellerServicesData.required);
                            }
                        }

                    }

                } else if (ctx.req.body.paymentStatus) {

                    if (ctx.req.body.paymentStatus == 1) {
                        ctx.req.body.paymentStatus = true;
                        ctx.req.body.paymentStatusAdmin = true + "";
                    } else if (ctx.req.body.paymentStatus == 0) {
                        ctx.req.body.paymentStatus = false;
                        ctx.req.body.paymentStatusAdmin = false + "";
                    } else if (ctx.req.body.paymentStatus == true) {
                        ctx.req.body.paymentStatusAdmin = true + "";
                    } else if (ctx.req.body.paymentStatus == false) {
                        ctx.req.body.paymentStatusAdmin = false + "";
                    }
                }
            }
            if (ctx.req.baseUrl.toString() == '/api/Buyers') {
                console.log('Buyer Update.....' + JSON.stringify(ctx.req.body));
            }
            next();
        } else if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/logout') > -1) {
            next();
        } else if (ctx.req.baseUrl.toString() == '/api/Frames' && ctx.req.method.toString() == 'PUT') {

            if (ctx.req.body.image == undefined) {
                console.log("entered....");
                next();
            } else {
                console.log("Frames Put entered into thumNailCreate........");
                thumbNailCreate(ctx, next);
                //next();
            }
        } else if (ctx.req.baseUrl.toString() == '/api/SalesMans' && ctx.req.method.toString() == 'POST') {

            var randomstring = require("randomstring");

            var uniqueId = randomstring.generate({
                length: 6,
                charset: 'alphanumeric',
                capitalization:'uppercase'
            });

            if (ctx.req.body.email == undefined) {
                ctx.req.body.email = ctx.req.body.phone + "@ouroneapp.com";
                ctx.req.body.mail = ctx.req.body.email;
                ctx.req.body.uniqueId = uniqueId;
                ctx.req.body.password = ctx.req.body.phone + uniqueId + "";
                // console.log("ctx.req.body.password: " + ctx.req.body.password);
                next();
            } else {
                ctx.req.body.mail = ctx.req.body.email;
                ctx.req.body.email = ctx.req.body.phone + "@ouroneapp.com";
                ctx.req.body.password = ctx.req.body.phone + uniqueId + "";
                ctx.req.body.uniqueId = uniqueId;
                // console.log("ctx.req.body.password: " + ctx.req.body.password);
                next();
            }
        }
        else if (ctx.req.baseUrl.toString() == '/api/Orders' && ctx.req.method.toString() == 'POST') {
            var LoyaltyBuyer = server.models.LoyaltyBuyer;
            /*  if(ctx.req.body.type=="default"){
             next();
             }else{*/
            LoyaltyBuyer.find({
                "where": {
                    "buyerId": ctx.req.body.buyerId,
                    "sellerId": ctx.req.body.sellerId, "passcode": ctx.req.body.passcode
                }
            }, function (err, loyaltyBuyerR) {
                if (loyaltyBuyerR.length > 0) {
                    console.log("Loyalty buyer passcode found");
                    next();
                } else {
                    console.log("Loyalty buyer passcode not found");
                    //loyaltyPasscode="notfound";
                    //ctx.res.send({"status":422, "message": "passcode wrong"});
                    var error = new Error('passcode wrong');
                    next(error);
                }
            });
            //}
        } else if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/login') > -1 || ctx.req.originalUrl.toString().indexOf('/api/Buyers/login') > -1) {
            console.log("body : " + JSON.stringify(ctx.req.body));
            if (ctx.req.body.email == undefined) {
                //console.log(ctx.req.body.phone);
                var queryString = {where: {phone: ctx.req.body.phone}};
                //console.log("qr:  "+queryString);
                if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/login') > -1) {

                    Seller.find(queryString, function (err, sellers) {
                        //console.log(accounts);
                        if (sellers.length > 0) {
                            ctx.req.body.email = sellers[0].email;
                        }
                        next();
                    });

                } else if (ctx.req.originalUrl.toString().indexOf('/api/Buyers/login') > -1) {

                    Buyer.find(queryString, function (err, buyers) {
                        console.log();
                        if (err) {
                            console.log('Error...' + JSON.stringify(err));
                            next();
                        } else if (buyers.length > 0) {
                            console.log('Buyers.' + JSON.stringify(buyers));
                            ctx.req.body.email = buyers[0].email;
                        }
                        next();
                    });

                }

            } else {

                var queryString = {where: {phone: ctx.req.body.phone}};
                //console.log("qr:  "+queryString);
                if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/login') > -1) {

                    Seller.find(queryString, function (err, sellers) {
                        //console.log(accounts);
                        if (sellers.length > 0) {
                            ctx.req.body.email = sellers[0].email;
                        }
                        next();
                    });

                } else if (ctx.req.originalUrl.toString().indexOf('/api/Buyers/login') > -1) {

                    Buyer.find(queryString, function (err, buyers) {
                        //console.log(accounts);
                        if (buyers.length > 0) {
                            ctx.req.body.email = buyers[0].email;
                        }
                        next();
                    });

                }

            }
        } else if ((ctx.req.baseUrl.toString() == '/api/Sellers' || ctx.req.baseUrl.toString() == '/api/Buyers') && ctx.req.method.toString() == 'POST') {
            //console.log("entering");
            // newly added
            if ((ctx.req.baseUrl.toString() == '/api/Sellers') && ctx.req.method.toString() == 'POST') {
                console.log("before in new response");
                var randomstring = require("randomstring");

                var otp = randomstring.generate({
                    length: 6,
                    charset: 'numeric'
                });

                var SMS = server.models.SMS;

                ctx.req.body['otp'] = otp;
                ctx.req.body['otpStatus'] = false;
                // ctx.req.body['password'] = otp;
                ctx.req.body['subscriptionInvitation'] = "disabled";
                ctx.req.body['loyaltyActivationStatus'] = "disabled";
                ctx.req.body['referralActivationStatus'] = "disabled";
                ctx.req.body['paymentStatus'] = false;
                ctx.req.body['alternativeNumber'] = "NA";
                var smsMessage = 'Hi! '+otp+' is your One Time Password. Vyuli view online buy offline';
                var destination = ctx.req.body.phone;
                console.log("seller Phone: " + destination);
                SMS.create({destination: destination, message: smsMessage}, function (err, sms) {

                    if (err) {
                        console.log("error in sms..............................." + JSON.stringify(err));
                    } else {
                        console.log("error in sms..............................." + JSON.stringify(err));
                    }
                });

                //console.log("sellerservicelistSize: " + ctx.req.body.sellerservicelist.length);
                if (ctx.req.body.paymentStatus == 1) {
                    ctx.req.body.paymentStatus = true;
                    ctx.req.body.paymentStatusAdmin = true + "";
                } else if (ctx.req.body.paymentStatus == 0) {
                    ctx.req.body.paymentStatus = false;
                    ctx.req.body.paymentStatusAdmin = false + "";
                } else if (ctx.req.body.paymentStatus == true) {
                    ctx.req.body.paymentStatusAdmin = true + "";
                } else if (ctx.req.body.paymentStatus == false) {
                    ctx.req.body.paymentStatusAdmin = false + "";
                }
                /*var sellerServicesList = ctx.req.body.sellerservicelist;
                 console.log("ctx.req.body.sellerservicelist: "+JSON.stringify(ctx.req.body.sellerservicelist));
                 if (sellerServicesList.length > 0) {
                 for (var i = 0; i < sellerServicesList.length; i++) {
                 var sellerServicesData = sellerServicesList[i];
                 if (sellerServicesData.required == 1 || sellerServicesData.required == "1") {
                 sellerServicesData.required = true;
                 console.log("sellerrequired: " + sellerServicesData.required);
                 } else if (sellerServicesData.required == 0 || sellerServicesData.required == "0") {
                 sellerServicesData.required = false;
                 console.log("sellerrequiredfalse: " + sellerServicesData.required);
                 }
                 }

                 }
                 console.log("ctx.req.body.sellerservicelist1111: "+JSON.stringify(ctx.req.body.sellerservicelist));*/
            }
            // newly added
            if (ctx.req.body.email == undefined) {
                ctx.req.body.email = ctx.req.body.phone + "@ouroneapp.com";
                ctx.req.body.mail = ctx.req.body.email;

            } else {
                ctx.req.body.mail = ctx.req.body.email;
                ctx.req.body.email = ctx.req.body.phone + "@ouroneapp.com";
            }
            if (ctx.req.originalUrl.toString() == '/api/Sellers') {
                ctx.req.body.frameCount = 0;
                ctx.req.body.frameStatus = false;
                next();
            } else {
                next();
            }
        }
        else if (ctx.req.baseUrl.toString() == '/api/Subscriptions' && ctx.req.method.toString() == 'POST') {

            var Subscription = server.models.Subscription;
            Subscription.find({
                "where": {
                    "sellerId": ctx.req.body.sellerId, "buyerId": ctx.req.body.buyerId,
                    "blockStatus": "true"
                }
            }, function (err, subscriptionR) {
                if (subscriptionR.length > 0) {
                    var error = new Error("buyer blocked");
                    next(error);
                } else {
                    /*
                     }
                     });*/
                    var GCM = server.models.GCM;
                    var APN = server.models.APN;
                    var BuyerBusiness = server.models.BuyerBusiness;
                    //var Notification=server.models.Notification;
                    /*            var ReferralBuyer = server.models.ReferralBuyer;
                     var ReferralSeller=server.models.ReferralSeller;*/
                    //Subscription.validateUniquenessOf('', { scopedTo: ['sellerId', 'buyerId'] });
                    //var subscriptionStatus = false;
                    //Subscription.find({"where":{"sellerId":ctx.req.body.sellerId, "buyerId": ctx.req.body.buyerId}}, function(err, subscriptions){
                    //   console.log("existing subscriptions"+ subscriptions.length);
                    //   if(subscriptions.length > 0) {
                    //       subscriptionStatus = true;
                    //   } else {
                    //   }
                    //
                    //});
                    //
                    //if(subscriptionStatus){
                    //    ctx.res.send({"status": 422,"message":"subscription already exist in db"});
                    //}
                    var buyerBusinessStatus = "";
                    Seller.findOne({"where": {id: ctx.req.body.sellerId}}, function (err, seller) {
                        console.log("entering into seller findone");
                        if (err) {
                            console.log("error in subscription newresponse: " + err);
                            next();
                        } else {
                            console.log("entering into seller found: " + seller.businessTypeId);
                            if (seller.businessTypeId != undefined && seller.businessTypeId != null && seller.businessTypeId != "NA") {
                                console.log("sellerbusinessTypeId: " + seller.businessTypeId);
                                BuyerBusiness.find({
                                    "where": {
                                        "businessTypeId": seller.businessTypeId,
                                        "buyerId": ctx.req.body.buyerId, "status": "disabled"
                                    }
                                }, function (err, buyerBusinesses) {
                                    console.log("buyerBusinesses: " + JSON.stringify(buyerBusinesses));
                                    if (buyerBusinesses.length > 0) {
                                        buyerBusinessStatus = "disabled";
                                        console.log("buyerBusinessStatus disabled");
                                        if (ctx.req.body.businessStatus != undefined && ctx.req.body.businessStatus == "enabled") {
                                            //BuyerBusiness.find({"where":{}}, function(err, BuyerBusinessInfo) {
                                            var buyerBusinessInfo = buyerBusinesses[0];
                                            buyerBusinessInfo.updateAttributes({"status": "enabled"}, function (err, BuyerBusinessesInfo) {


                                                var subscriptionId = ctx.req.body.sellerId.toString() + ctx.req.body.buyerId.toString();
                                                ctx.req.body.sbid = ctx.req.body.sellerId.toString() + ctx.req.body.buyerId.toString();
                                                ctx.req.body.status = "Active";
                                                ctx.req.body.blockStatus = "false";
                                                ctx.req.body.subscriptionRequestStatus = "true";
                                                ctx.req.body.priceRequestStatus = false;
                                                console.log("seller: " + JSON.stringify(seller));
                                                /*var seller_gcm_id;
                                                 var seller_device_token_id;
                                                 try {
                                                 if (seller.gcm_id != "NA") {
                                                 seller_gcm_id = seller.gcm_id;
                                                 }
                                                 } catch (e) {
                                                 console.log("Error in subscription request in newresponse: " + JSON.stringify(e));
                                                 }
                                                 try {
                                                 if (seller.devicetoken != "NA") {
                                                 seller_device_token_id = seller.devicetoken;
                                                 }
                                                 } catch (e) {
                                                 console.log("Error in subscription request in newresponse: " + JSON.stringify(e));
                                                 }*/
                                                // newly added code for Approving Subscription Request
                                                if (seller.subscriptionInvitation != undefined && seller.subscriptionInvitation == "enabled") {
                                                    ctx.req.body.subscriptionRequestStatus = "false";
                                                    //ctx.req.body.status = "InActive";


                                                    /*if (seller_gcm_id != undefined) {
                                                     var gcmData = {
                                                     "registration_ids": [
                                                     seller_gcm_id
                                                     ],
                                                     "data": {
                                                     "message": "Please Approve Subscription Request",
                                                     "type": "Subscription",
                                                     "operation": "Subscription_Request",
                                                     "id": subscriptionId
                                                     }
                                                     };

                                                     try {
                                                     if (seller.gcm_id) {
                                                     GCM.create(gcmData, function (err, gcm) {
                                                     });
                                                     }
                                                     } catch (e) {
                                                     console.log("Error in subscription android request in newresponse: " + JSON.stringify(e));
                                                     }
                                                     }*/
                                                    /*if (seller_device_token_id != undefined) {
                                                     var appleNotificationData = {
                                                     "registration_ids": [
                                                     seller_device_token_id
                                                     ],
                                                     "data": {
                                                     "message": "Please Approve Subscription Request",
                                                     "type": "Subscription",
                                                     "operation": "Subscription_Request",
                                                     "id": subscriptionId
                                                     }
                                                     };
                                                     try {
                                                     if (seller.devicetoken) {
                                                     APN.create(appleNotificationData, function (err, apn) {
                                                     });
                                                     }
                                                     } catch (e) {
                                                     console.log("Error in subscription ios request in newresponse: " + JSON.stringify(e));
                                                     }
                                                     }*/
                                                }
                                                ctx.req.body['businessTypeId'] = seller.businessTypeId;
                                                ctx.req.body['businessTypeName'] = seller.businessTypeName;
                                                ctx.req.body['businessName'] = seller.businessName;
                                                ctx.req.body['referral'] = seller['referral'];
                                                ctx.req.body['loyalty'] = seller['loyalty'];
                                                ctx.req.body['sellerservicelist'] = seller['sellerservicelist'];
                                                next();

                                            });
                                            //});
                                        } else {
                                            console.log("category disabled....");
                                            ctx.res.send({"status": 422, "message": "category disabled"});
                                            /* var error=new Error("category disabled");
                                             next(error);*/
                                        }
                                    } else {
                                        buyerBusinessStatus = "enabled";
                                        console.log("buyerBusinessStatus enabled");
                                        //ctx.res.send({"status": 200, "message": "subscription possible"});
                                        console.log("enter into else: " + buyerBusinessStatus);
                                        var subscriptionId = ctx.req.body.sellerId.toString() + ctx.req.body.buyerId.toString();
                                        ctx.req.body.sbid = ctx.req.body.sellerId.toString() + ctx.req.body.buyerId.toString();
                                        ctx.req.body.status = "Active";
                                        ctx.req.body.blockStatus = "false";
                                        ctx.req.body.subscriptionRequestStatus = "true";
                                        ctx.req.body.priceRequestStatus = false;
                                        console.log("seller: " + JSON.stringify(seller));
                                        /*var seller_gcm_id;
                                         var seller_devicetoken;
                                         try {
                                         if (seller.gcm_id != "NA") {
                                         seller_gcm_id = seller.gcm_id;
                                         }
                                         } catch (e) {
                                         console.log("Error in subscription request in newresponse: " + JSON.stringify(e));
                                         }
                                         try {
                                         if (seller.devicetoken != "NA") {
                                         seller_devicetoken = seller.devicetoken;
                                         }
                                         } catch (e) {
                                         console.log("Error in subscription request in newresponse: " + JSON.stringify(e));
                                         }*/
                                        // newly added code for Approving Subscription Request
                                        if (seller.subscriptionInvitation != undefined && seller.subscriptionInvitation == "enabled") {
                                            ctx.req.body.subscriptionRequestStatus = "false";
                                            //ctx.req.body.status = "InActive";
                                            /*Notification.find({
                                             "where":{"from":ctx.req.body.buyerId,"to":ctx.req.body.sellerId,"type":"Subscription",
                                             "operation":"Subscription_Request"}},function(err,NotificationR){
                                             //var NotificationRes=NotificationR[0];
                                             if(NotificationR.length==0){
                                             console.log("NotificationRLength: " + NotificationR.length);
                                             var notificationDetails = {
                                             "from": ctx.req.body.buyerId,
                                             "fromType": "buyer",
                                             "to": ctx.req.body.sellerId,
                                             "toType": "seller",
                                             "message": "Please Approve Subscription Request",
                                             "type": "Subscription",
                                             "operation": "Subscription_Request",
                                             "status": "new",
                                             "typeId": subscriptionId
                                             };
                                             Notification.create(notificationDetails, function (err, notificationR) {
                                             console.log("Subscription_Request notificationR: " + JSON.stringify(notificationR));
                                             });

                                             }
                                             });*/
                                            /*var gcmData = {
                                             "registration_ids": [
                                             seller_gcm_id, seller_devicetoken
                                             ],
                                             "data": {
                                             "message": "Please Approve Subscription Request",
                                             "type": "Subscription",
                                             "operation": "Subscription_Request",
                                             "id": subscriptionId
                                             }
                                             };
                                             try {
                                             if (seller.gcm_id) {
                                             GCM.create(gcmData, function (err, gcm) {
                                             });
                                             }
                                             } catch (e) {
                                             console.log("Error in subscription android request in newresponse: " + JSON.stringify(e));
                                             }
                                             try {
                                             if (seller.devicetoken) {
                                             APN.create(gcmData, function (err, apn) {
                                             });
                                             }
                                             } catch (e) {
                                             console.log("Error in subscription ios request in newresponse: " + JSON.stringify(e));
                                             }*/
                                        }
                                        ctx.req.body['businessTypeId'] = seller.businessTypeId;
                                        ctx.req.body['businessTypeName'] = seller.businessTypeName;
                                        ctx.req.body['businessName'] = seller.businessName;
                                        ctx.req.body['referral'] = seller['referral'];
                                        ctx.req.body['loyalty'] = seller['loyalty'];
                                        ctx.req.body['sellerservicelist'] = seller['sellerservicelist'];
                                        next();
                                        //ctx.req.body['seller'] = seller;

                                        //Buyer.findOne({"where": {id: ctx.req.body.buyerId}}, function (err, buyer) {
                                        //    delete buyer.password;
                                        //    ctx.req.body['buyer'] = buyer;
                                        //    console.log(ctx.req.body);
                                        //    //console.log(actualObject);
                                        //    next();
                                        //});


                                    }
                                });
                            } else {
                                ctx.res.send({"status": 422, "message": "update of seller business details pending"});
                                /* var error=new Error("update of seller business details pending");
                                 next(error);*/
                            }
                        }
                    });
                    /*if (buyerBusinessStatus=="disabled") {
                     ctx.res.send({"status": 422, "message": "subscription not possible because you disable this category"});
                     }else{
                     console.log("enter into else: "+buyerBusinessStatus);
                     var subscriptionId = ctx.req.body.sellerId.toString() + ctx.req.body.buyerId.toString();
                     ctx.req.body.sbid = ctx.req.body.sellerId.toString() + ctx.req.body.buyerId.toString();
                     ctx.req.body.status = "Active";
                     ctx.req.body.subscriptionRequestStatus = false;
                     ctx.req.body.priceRequestStatus = false;


                     Seller.findOne({"where": {id: ctx.req.body.sellerId}}, function (err, seller) {
                     //console.log("seller: "+ JSON.stringify(seller));
                     if (err) {
                     next();
                     } else {
                     ////var actualObject = ctx.req.body;
                     ////extend(true, actualObject, ctx.req.body, seller);
                     //////ctx.req.body = merge(true, ctx.req.body, seller);
                     //console.log(JSON.stringify(seller));
                     //for (var key in seller) {
                     //    if (seller.hasOwnProperty(key)) {
                     //        console.log(key);
                     //        //if(key.toString() != 'password'){
                     //        //    ctx.req.body[key] = seller[key];
                     //        //}
                     //    }
                     //    //console.log(key);
                     //}
                     //delete seller.password;
                     var seller_gcm_id;
                     //console.log("sellergcmid: "+seller_gcm_id);
                     try {
                     if (seller.gcm_id != "NA") {
                     seller_gcm_id = seller.gcm_id;
                     }
                     } catch (e) {
                     console.log("Error in subscription request in newresponse: " + JSON.stringify(e));
                     }
                     try {
                     if (seller.devicetoken != "NA") {
                     seller_gcm_id = seller.devicetoken;
                     }
                     } catch (e) {
                     console.log("Error in subscription request in newresponse: " + JSON.stringify(e));
                     }
                     // newly added code for Approving Subscription Request
                     if (seller.subscriptionInvitation != undefined && seller.subscriptionInvitation == true) {
                     var gcmData = {
                     "registration_ids": [
                     seller_gcm_id
                     ],
                     "data": {
                     "message": "Please Approve Subscription Request",
                     "type": "Subscription",
                     "operation": "Subscription_Request",
                     "id": subscriptionId
                     }
                     };
                     try {
                     if (seller.gcm_id/!*!="NA"*!/) {
                     GCM.create(gcmData, function (err, gcm) {
                     });
                     }
                     } catch (e) {
                     console.log("Error in subscription android request in newresponse: " + JSON.stringify(e));
                     }
                     try {
                     if (seller.devicetoken/!*!="NA"*!/) {
                     APN.create(gcmData, function (err, apn) {
                     });
                     }
                     } catch (e) {
                     console.log("Error in subscription ios request in newresponse: " + JSON.stringify(e));
                     }
                     }
                     ctx.req.body['businessTypeId'] = seller.businessTypeId;
                     ctx.req.body['businessTypeName'] = seller.businessTypeName;
                     ctx.req.body['referral'] = seller['referral'];
                     ctx.req.body['loyalty'] = seller['loyalty'];
                     ctx.req.body['sellerservicelist'] = seller['sellerservicelist'];
                     next();
                     //ctx.req.body['seller'] = seller;

                     //Buyer.findOne({"where": {id: ctx.req.body.buyerId}}, function (err, buyer) {
                     //    delete buyer.password;
                     //    ctx.req.body['buyer'] = buyer;
                     //    console.log(ctx.req.body);
                     //    //console.log(actualObject);
                     //    next();
                     //});
                     }

                     });
                     //next();
                     }*/
                }
            });
        }
        else {
            next();
        }
    });

    function fileWriteToAWS(ctx, next) {
        var AWS = require('aws-sdk');
        var fs = require('fs');
        var qr = require('qr-image');
        //var base64 = require('node-base64-image');

        AWS.config.loadFromPath('config.json');
        s3Stream = require('s3-upload-stream')(new AWS.S3());
        console.log("excuting image qr code");
        console.log(ctx.result.id);

        var qrCodeObject = {
            "sellerId": ctx.result.id,
            "sellerName": ctx.result.name,
            "sellerBusinessName": ctx.result.businessName,
            "businessTypeId": ctx.result.businessTypeId,
            "businessTypeName": ctx.result.businessTypeName
        };
        console.log(JSON.stringify(qrCodeObject));

        var code = qr.image(JSON.stringify(qrCodeObject), {type: 'png'});

        var upload = s3Stream.upload({
            Bucket: "araay",
            Key: ctx.result.id + "_" + (new Date()).getTime(),
            ACL: "public-read",
            //StorageClass: "REDUCED_REDUNDANCY",
            ContentType: "png"
        });

        console.log('entered');
        upload.maxPartSize(20971520); // 20 MB
        upload.concurrentParts(5);

        upload.on('error', function (error) {
            console.log("Error: " + error);
            next();
        });

        upload.on('part', function (details) {
            console.log("part" + details);
        });

        upload.on('uploaded', function (details) {
            console.log("uploaded: " + JSON.stringify(details));
            ctx.result["qrcode"] = details.Location;
            ctx.result = {
                status: "success",
                message: "",
                data: ctx.result
            };

            console.log('id: ' + ctx.result.data.id);
            Seller.findById(ctx.result.data.id, function (err, sellerR) {

                console.log("sellerInfo: " + JSON.stringify(sellerR));
                sellerR.updateAttributes({"qrcode": details.Location}, function (err, uSeller) {

                });

            });
            //Seller.updateAll({'id': ctx.result.id}, {qrcode: details.Location}, function (ctx, next) {
            //});
            if (ctx.req.baseUrl.toString() == '/api/Sellers' && ctx.req.method.toString() == 'PUT') {
                console.log("qr image sellers put method..changing message data");
                ctx.result.message = "Updated";
                

            }
            if (ctx.req.baseUrl.toString() == '/api/Accounts' && ctx.req.method.toString() == 'GET') {

                if (ctx.result.data.length > 0) {
                    ctx.result.message = "User Already Registered";
                } else {
                    ctx.result.message = 'User is Not Registered';
                }

            }
            next();
        });

        code.pipe(upload);


    }

    remotes.after('**', function (ctx, next) {
        console.log(ctx.req.baseUrl);
        console.log(ctx.req.originalUrl);
        var qrFlag = false;

        if (ctx.req.originalUrl.toString().indexOf('/buyerExists') > -1) {

            if (ctx.result.buyer.otpStatus == false) {

                ctx.result = {
                    status: "success",
                    message: "New Buyer",
                    data: ctx.result
                };
                next();

            } else {

                ctx.result = {
                    status: "success",
                    message: "Existed Buyer",
                    data: ctx.result
                };
                next();

            }

        }
        else if (ctx.req.originalUrl.toString().indexOf('/api/Buyers/login') > -1) {
            console.log("buyerLOGIn: ");

            Buyer.findById(ctx.result.userId, function (err, buyer) {

                //if (buyer["otpStatus"] == false) {
                //    ctx.result = {
                //        status: "success",
                //        message: "OTP is not verified",
                //        data: {}
                //    };
                //
                //    next();
                //} else {
                ctx.result.buyer = buyer;
                ctx.result = {
                    status: "success",
                    message: "Account activation is done",
                    data: ctx.result
                };
                next();
                //}
            });

        } else if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/login') > -1) {

            Seller.findById(ctx.result.userId, function (err, seller) {
                /*if(err){
                 console.log("sellerLogin Error: "+JSON.stringify(err));
                 next();
                 }else{*/
                ctx.result["seller"] = seller;
                //console.log("password: " + ctx.req.body.password);
                if (seller["otpStatus"] ==false) {
                // if (seller["otp"] == ctx.req.body.password) {
                    ctx.result = {
                        status: "success",
                        message: "OTP is not verified",
                        // message: "Registration completed",
                        data: ctx.result
                    };

                    next();
                } else {
                    // newly added for validating cheque status
                    if (seller["paymentStatus"] == false && (seller["paymodedesc"] == "Cheque" || seller["paymodedesc"] =="CHEQUE")) {
                        ctx.result = {
                            status: "success",
                            message: "Account activation is pending",
                            sellerName: seller.name,
                            data: ctx.result
                        };
                        next();

                    } else if (seller["paymentStatus"] == false) {
                        ctx.result = {
                            status: "success",
                            message: "Payment is pending",
                            sellerName: seller.name,
                            data: ctx.result
                        };
                        next();
                    } else {
                        ctx.result.appUrl = "https://play.google.com/store/apps/details?id=com.aaray.ouroneapp";
                        ctx.result = {
                            status: "success",
                            message: "Account activation is done",
                            data: ctx.result
                        };
                        next();
                    }


                }


            });

        }
        else if (ctx.req.baseUrl.toString() == '/api/Orders' && ctx.req.method.toString() == 'POST') {
            /*if(ctx.req.body.type=="default") {
             console.log("default execution with out passcode");
             var LoyaltySeller = server.models.LoyaltySeller;
             var LoyaltyBuyer = server.models.LoyaltyBuyer;
             var BuyerEarnedPoints = server.models.BuyerEarnedPoints;
             console.log("ctx.req.body.sellerId: " + ctx.req.body.sellerId);
             LoyaltySeller.findOne({"where": {"sellerId": ctx.req.body.sellerId}}, function (err, loyaltySellerDet) {
             console.log("loyaltySellerDet: " + JSON.stringify(loyaltySellerDet));
             var sellerAccumulationAmount = Number(loyaltySellerDet.accumulationAmount);
             console.log("sellerAccumulationAmount: " + sellerAccumulationAmount);

             var sellerAccumulationPoints = Number(loyaltySellerDet.accumulationPoints);
             console.log("sellerAccumulationPoints: " + sellerAccumulationPoints);
             var sellerRedemptionAmount = loyaltySellerDet.redemptionAmount;
             var sellerRedemptionPoints = loyaltySellerDet.redemptionPoints;
             var sellerCurrency = loyaltySellerDet.currency;
             var buyerRedeemPoints = Number(ctx.req.body.redeemPoints);
             var buyerRedeemAmount = Number(ctx.req.body.redeemAmount);
             var sellerProductSaleAmount = Number(ctx.req.body.productSaleAmount);
             var paidAmountByBuyer = Number(sellerProductSaleAmount - buyerRedeemAmount);
             var accumulatedPointsGainedByBuyer = Number((paidAmountByBuyer * sellerAccumulationPoints) / (sellerAccumulationAmount));
             console.log("accumulatedPointsGainedByBuyer: " + accumulatedPointsGainedByBuyer);
             console.log("sellerAccumulationAmount: " + sellerAccumulationAmount);
             console.log("sellerAccumulationPoints: " + sellerAccumulationPoints);
             console.log("buyerRedeemAmount: " + buyerRedeemAmount);
             console.log("sellerProductSaleAmount: " + sellerProductSaleAmount);
             console.log("paidAmountByBuyer: " + paidAmountByBuyer);
             console.log("sellerRedemptionAmount: " + sellerRedemptionAmount);
             console.log("sellerRedemptionPoints: " + sellerRedemptionPoints);
             LoyaltyBuyer.create({
             'accumulatedPoints': accumulatedPointsGainedByBuyer,
             'redeemedAmount': buyerRedeemAmount,
             'redeemedPoints': buyerRedeemPoints,
             'orderId': ctx.result.id,
             'sellerId': ctx.req.body.sellerId,
             'buyerId': ctx.req.body.buyerId,
             'status': "Active"
             }, function (err, loyaltyBuyerDet) {
             console.log("LoyaltyBuyer is created: " + JSON.stringify(loyaltyBuyerDet) + "err: " + err);
             BuyerEarnedPoints.find({
             "where": {
             "buyerId": ctx.req.body.buyerId,
             "sellerId": ctx.req.body.sellerId
             }
             }, function (err, buyerEarnedPointsDet) {
             if (buyerEarnedPointsDet.length > 0) {
             var buyerEarnedPointsUpdate = buyerEarnedPointsDet[0];
             var totalLoyaltyPointsUpdate = Number((accumulatedPointsGainedByBuyer + Number(buyerEarnedPointsUpdate.totalPoints)) - buyerRedeemPoints);
             if (totalLoyaltyPointsUpdate <= 0) {

             totalLoyaltyPointsUpdate = 0;
             }
             buyerEarnedPointsUpdate.updateAttributes({"totalPoints": totalLoyaltyPointsUpdate}, function (err, buyerEarnedPointsUpdateDet) {
             console.log('buyerEarnedPointsUpdateDet:' + JSON.stringify(buyerEarnedPointsUpdateDet));

             next();
             });
             } else {
             BuyerEarnedPoints.create({
             'totalPoints': accumulatedPointsGainedByBuyer,
             'sellerId': ctx.req.body.sellerId,
             'buyerId': ctx.req.body.buyerId,
             'status': "Active"
             }, function (err, BuyerEarnedPointsDet) {
             console.log('BuyerEarnedPoints is created: ' + JSON.stringify(BuyerEarnedPointsDet));
             next();
             });

             }
             });

             });


             //next();
             });
             }else{*/
            Seller.findOne({"where": {"id": ctx.req.body.sellerId}}, function (err, sellerR) {
                if (sellerR.loyaltyActivationStatus == "enabled" && sellerR.loyaltyActivationStatus != undefined) {
                    console.log("loyaltyActivationStatus enabled with passcode");
                    var LoyaltySeller = server.models.LoyaltySeller;
                    var LoyaltyBuyer = server.models.LoyaltyBuyer;
                    var BuyerEarnedPoints = server.models.BuyerEarnedPoints;
                    console.log("ctx.req.body.sellerId: " + ctx.req.body.sellerId);
                    LoyaltySeller.findOne({"where": {"sellerId": ctx.req.body.sellerId}}, function (err, loyaltySellerDet) {
                        console.log("loyaltySellerDet: " + JSON.stringify(loyaltySellerDet));
                        var sellerAccumulationAmount = Number(loyaltySellerDet.accumulationAmount);
                        console.log("sellerAccumulationAmount: " + sellerAccumulationAmount);

                        var sellerAccumulationPoints = Number(loyaltySellerDet.accumulationPoints);
                        console.log("sellerAccumulationPoints: " + sellerAccumulationPoints);
                        var sellerRedemptionAmount = loyaltySellerDet.redemptionAmount;
                        var sellerRedemptionPoints = loyaltySellerDet.redemptionPoints;
                        var sellerCurrency = loyaltySellerDet.currency;
                        var buyerRedeemPoints = Number(ctx.req.body.redeemPoints);
                        var buyerRedeemAmount = Number(ctx.req.body.redeemAmount);
                        var sellerProductSaleAmount = Number(ctx.req.body.productSaleAmount);
                        var paidAmountByBuyer = Number(sellerProductSaleAmount - buyerRedeemAmount);
                        var accumulatedPointsGainedByBuyer = 0;
                        if (paidAmountByBuyer == 0) {
                            accumulatedPointsGainedByBuyer = 0;
                        } else if (paidAmountByBuyer > 0) {
                            accumulatedPointsGainedByBuyer = Number((paidAmountByBuyer * sellerAccumulationPoints) / (sellerAccumulationAmount));
                        }

                        console.log("accumulatedPointsGainedByBuyer: " + accumulatedPointsGainedByBuyer);
                        console.log("sellerAccumulationAmount: " + sellerAccumulationAmount);
                        console.log("sellerAccumulationPoints: " + sellerAccumulationPoints);
                        console.log("buyerRedeemAmount: " + buyerRedeemAmount);
                        console.log("sellerProductSaleAmount: " + sellerProductSaleAmount);
                        console.log("paidAmountByBuyer: " + paidAmountByBuyer);
                        console.log("sellerRedemptionAmount: " + sellerRedemptionAmount);
                        console.log("sellerRedemptionPoints: " + sellerRedemptionPoints);
                        LoyaltyBuyer.find({
                            "where": {
                                "buyerId": ctx.req.body.buyerId,
                                "sellerId": ctx.req.body.sellerId, "passcode": ctx.req.body.passcode
                            }
                        }, function (err, loyaltyBuyerR) {
                            if (loyaltyBuyerR.length > 0) {
                                var loyaltyBuyerRes = loyaltyBuyerR[0];

                                loyaltyBuyerRes.updateAttributes({
                                        'accumulatedPoints': accumulatedPointsGainedByBuyer,
                                        'redeemedAmount': buyerRedeemAmount,
                                        'redeemedPoints': buyerRedeemPoints,
                                        'orderId': ctx.result.id,
                                        'sellerId': ctx.req.body.sellerId,
                                        'buyerId': ctx.req.body.buyerId,
                                        'status': "Active"
                                    },
                                    function (err, loyaltyBuyerDetUpdate) {
                                        console.log('loyaltyBuyerDetUpdate:' + JSON.stringify(loyaltyBuyerDetUpdate));
                                        BuyerEarnedPoints.find({
                                            "where": {
                                                "buyerId": ctx.req.body.buyerId,
                                                "sellerId": ctx.req.body.sellerId
                                            }
                                        }, function (err, buyerEarnedPointsDet) {
                                            if (buyerEarnedPointsDet.length > 0) {
                                                var buyerEarnedPointsUpdate = buyerEarnedPointsDet[0];
                                                var totalLoyaltyPointsUpdate = Number((accumulatedPointsGainedByBuyer + Number(buyerEarnedPointsUpdate.totalPoints)) - buyerRedeemPoints);
                                                if (totalLoyaltyPointsUpdate <= 0) {

                                                    totalLoyaltyPointsUpdate = 0;
                                                }
                                                buyerEarnedPointsUpdate.updateAttributes({"totalPoints": totalLoyaltyPointsUpdate}, function (err, buyerEarnedPointsUpdateDet) {
                                                    console.log('buyerEarnedPointsUpdateDet:' + JSON.stringify(buyerEarnedPointsUpdateDet));

                                                    next();
                                                });
                                            } else {
                                                BuyerEarnedPoints.create({
                                                    'totalPoints': accumulatedPointsGainedByBuyer,
                                                    'sellerId': ctx.req.body.sellerId,
                                                    'buyerId': ctx.req.body.buyerId,
                                                    'status': "Active"
                                                }, function (err, BuyerEarnedPointsDet) {
                                                    console.log('BuyerEarnedPoints is created: ' + JSON.stringify(BuyerEarnedPointsDet));
                                                    next();
                                                });

                                            }
                                        });
                                    });
                            }
                        });


                    });
                } else {
                    next();
                }
            });
            //}


        }
        /* else if(ctx.req.baseUrl.toString() == '/api/Orders' && ctx.req.method.toString() == 'POST') {

         var LoyaltySeller = server.models.LoyaltySeller;
         var LoyaltyBuyer = server.models.LoyaltyBuyer;
         var BuyerEarnedPoints = server.models.BuyerEarnedPoints;
         console.log("ctx.req.body.sellerId: " + ctx.req.body.sellerId);
         LoyaltySeller.findOne({"where": {"sellerId": ctx.req.body.sellerId}}, function (err, loyaltySellerDet) {
         console.log("loyaltySellerDet: " + JSON.stringify(loyaltySellerDet));
         var sellerAccumulationAmount = Number(loyaltySellerDet.accumulationAmount);
         console.log("sellerAccumulationAmount: " + sellerAccumulationAmount);

         var sellerAccumulationPoints = Number(loyaltySellerDet.accumulationPoints);
         console.log("sellerAccumulationPoints: " + sellerAccumulationPoints);
         var sellerRedemptionAmount = loyaltySellerDet.redemptionAmount;
         var sellerRedemptionPoints = loyaltySellerDet.redemptionPoints;
         var sellerCurrency = loyaltySellerDet.currency;
         var buyerRedeemPoints = Number(ctx.req.body.redeemPoints);
         var buyerRedeemAmount = Number(ctx.req.body.redeemAmount);
         var sellerProductSaleAmount = Number(ctx.req.body.productSaleAmount);
         var paidAmountByBuyer = Number(sellerProductSaleAmount - buyerRedeemAmount);
         var accumulatedPointsGainedByBuyer = Number((paidAmountByBuyer * sellerAccumulationPoints) / (sellerAccumulationAmount));
         console.log("accumulatedPointsGainedByBuyer: " + accumulatedPointsGainedByBuyer);
         console.log("sellerAccumulationAmount: " + sellerAccumulationAmount);
         console.log("sellerAccumulationPoints: " + sellerAccumulationPoints);
         console.log("buyerRedeemAmount: " + buyerRedeemAmount);
         console.log("sellerProductSaleAmount: " + sellerProductSaleAmount);
         console.log("paidAmountByBuyer: " + paidAmountByBuyer);
         console.log("sellerRedemptionAmount: " + sellerRedemptionAmount);
         console.log("sellerRedemptionPoints: " + sellerRedemptionPoints);
         LoyaltyBuyer.find({
         "where": {
         "buyerId": ctx.req.body.buyerId,
         "sellerId": ctx.req.body.sellerId, "passcode": ctx.req.body.passcode
         }
         }, function (err, loyaltyBuyerR) {
         if (loyaltyBuyerR.length > 0) {
         var loyaltyBuyerRes = loyaltyBuyerR[0];

         loyaltyBuyerRes.updateAttributes({
         'accumulatedPoints': accumulatedPointsGainedByBuyer,
         'redeemedAmount': buyerRedeemAmount,
         'redeemedPoints': buyerRedeemPoints,
         'orderId': ctx.result.id,
         'sellerId': ctx.req.body.sellerId,
         'buyerId': ctx.req.body.buyerId,
         'status': "Active"
         },
         function (err, loyaltyBuyerDetUpdate) {
         console.log('loyaltyBuyerDetUpdate:' + JSON.stringify(loyaltyBuyerDetUpdate));
         BuyerEarnedPoints.find({
         "where": {
         "buyerId": ctx.req.body.buyerId,
         "sellerId": ctx.req.body.sellerId
         }
         }, function (err, buyerEarnedPointsDet) {
         if (buyerEarnedPointsDet.length > 0) {
         var buyerEarnedPointsUpdate = buyerEarnedPointsDet[0];
         var totalLoyaltyPointsUpdate = Number((accumulatedPointsGainedByBuyer + Number(buyerEarnedPointsUpdate.totalPoints)) - buyerRedeemPoints);
         if (totalLoyaltyPointsUpdate <= 0) {

         totalLoyaltyPointsUpdate = 0;
         }
         buyerEarnedPointsUpdate.updateAttributes({"totalPoints": totalLoyaltyPointsUpdate}, function (err, buyerEarnedPointsUpdateDet) {
         console.log('buyerEarnedPointsUpdateDet:' + JSON.stringify(buyerEarnedPointsUpdateDet));

         next();
         });
         } else {
         BuyerEarnedPoints.create({
         'totalPoints': accumulatedPointsGainedByBuyer,
         'sellerId': ctx.req.body.sellerId,
         'buyerId': ctx.req.body.buyerId,
         'status': "Active"
         }, function (err, BuyerEarnedPointsDet) {
         console.log('BuyerEarnedPoints is created: ' + JSON.stringify(BuyerEarnedPointsDet));
         next();
         });

         }
         });
         });


         }


         });


         });




         }*/
        /* else if(ctx.req.originalUrl.toString().indexOf('/sellerExists')> -1) {

         if (ctx.result.seller == "seller not found") {

         ctx.result = {
         status: "success",
         message: "seller not found",
         data:""
         };
         next();

         } else {

         ctx.result = {
         status: "success",
         message: "seller found",
         data: ctx.result
         };
         next();

         }

         }*/
        else if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/logout') > -1) {


        } else if ((ctx.req.baseUrl.toString() == '/api/Sellers' || ctx.req.baseUrl.toString() == '/api/Buyers') && ctx.req.method.toString() == 'POST') {
            var Account = server.models.Account;
            var userType = '';
            if (ctx.req.baseUrl.toString() == '/api/Sellers') {
                userType = 'Seller';
                //var businessId = ctx.req.body.businessTypeId;
                //console.log("businessTypeId: "+ctx.req.body.businessTypeId);
                /*Business.findOne({"where": {"id": businessId}}, function (err, business) {
                 console.log("business.name: "+business.name);
                 Account.create({
                 'uid': ctx.result.id,
                 'userType': userType,
                 'userId': ctx.req.body.phone,
                 'email': ctx.req.body.email,
                 'mail': ctx.req.body.mail,
                 'businessName': ctx.req.body.businessName,
                 'name': ctx.req.body.name,
                 'businessTypeId': ctx.req.body.businessTypeId,
                 'businessTypeName': business.name,
                 'seller': ctx.result,
                 'frameCount': 50,
                 'frameStatus': false
                 }, function () {
                 //console.log('account is created');
                 });

                 });*/
                //Business.findOne({"where": {"id": businessId}}, function (err, business) {
                //console.log("business.name: "+business.name);
                Account.create({
                    'uid': ctx.result.id,
                    'userType': userType,
                    'userId': ctx.req.body.phone,
                    'email': ctx.req.body.email,
                    'mail': ctx.req.body.mail,
                    'businessName': "",
                    'name': "",
                    'businessTypeId': "",
                    'businessTypeName': "",
                    'seller': ctx.result,
                    'frameCount': 50,
                    'frameStatus': false
                }, function () {
                    //console.log('account is created');
                });

                //});

            } else if (ctx.req.baseUrl.toString() == '/api/Buyers') {
                userType = 'Buyer';
                Account.create({
                    'uid': ctx.result.id,
                    'userType': userType,
                    'userId': ctx.req.body.phone,
                    'email': ctx.req.body.email,
                    'mail': ctx.req.body.mail,
                    'name': ctx.req.body.name,
                    "buyer": ctx.result
                }, function () {
                    //console.log('account is created');
                });
            }

            if (ctx.req.baseUrl.toString() == '/api/Sellers' && ctx.req.method.toString() == 'POST') {
                for (var i = 0; i < 50; i++) {
                    //console.log('frame');
                    Frame.create({
                        'sellerId': ctx.result.id,
                        'status': false,
                        'frameNo': i + 1,
                        'visible': true
                    }, function () {
                    });
                }
                qrFlag = true;
                fileWriteToAWS(ctx, next);
            }


        }
        /*if (qrFlag) {
         fileWriteToAWS(ctx, next);
         }*/
        //else {
        /*if (ctx.req.originalUrl.toString().indexOf('/api/Sellers/login') > -1) {

         Seller.findById(ctx.result.userId, function (err, seller) {
         if(err){
         console.log("sellerLogin Error: "+JSON.stringify(err));
         next();
         }else{
         ctx.result["seller"]=seller;
         console.log("password: "+ctx.req.body.password);
         if (seller["otp"] == ctx.req.body.password) {
         ctx.result = {
         status: "success",
         //message: "OTP is not verified",
         message: "Registration completed",
         data: ctx.result
         };

         next();
         } else {
         // newly added for validating cheque status
         if(seller["paymentStatus"]==false && seller["paymodedesc"]=="Cheque"){
         ctx.result = {
         status: "success",
         message: "Account activation is pending",
         sellerName:seller.name,
         data: ctx.result
         };
         next();

         } else if (seller["paymentStatus"] == false) {
         ctx.result = {
         status: "success",
         message: "Payment is pending",
         sellerName:seller.name,
         data: ctx.result
         };
         next();
         } else {
         ctx.result.appUrl = "https://play.google.com/store/apps/details?id=com.aaray.ouroneapp";
         ctx.result = {
         status: "success",
         message: "Account activation is done",
         data: ctx.result
         };
         next();
         }


         }

         }
         });

         }*/
        /*else if (ctx.req.originalUrl.toString().indexOf('/api/Buyers/login') > -1) {
         console.log("buyerLOGIn: ");

         Buyer.findById(ctx.result.userId, function (err, buyer) {

         //if (buyer["otpStatus"] == false) {
         //    ctx.result = {
         //        status: "success",
         //        message: "OTP is not verified",
         //        data: {}
         //    };
         //
         //    next();
         //} else {
         ctx.result.buyer = buyer;
         ctx.result = {
         status: "success",
         message: "Account activation is done",
         data: ctx.result
         };
         next();
         //}
         });

         }*/
        else if (ctx.req.originalUrl.toString().indexOf('/api/Accounts/validateOTP') > -1) {

            console.log("response: " + JSON.stringify(ctx.result));
            if (ctx.result.user == null) {
                ctx.result = {
                    status: "failure",
                    message: "OTP is not Matched",
                    data: {}
                };
                next();
            } else {
                ctx.result = {
                    status: "success",
                    message: "OTP is validated",
                    data: ctx.result
                };
                next();
            }
        }
        else if (ctx.req.baseUrl.toString() == '/api/Buyers' && ctx.req.method.toString() == 'PUT') {

            var Account = server.models.Account;
            Account.find({"where": {"userId": ctx.result.phone, "userType": "Buyer"}}, function (err, accounts) {
                if (accounts) {
                    var account = accounts[0];
                    account.updateAttributes({"name": ctx.result.name}, function (err, accountData) {
                        console.log('Update Account:' + JSON.stringify(accountData));
                        ctx.result = {
                            status: "success",
                            message: "Updated",
                            data: ctx.result
                        };
                        next();
                    });
                }
            });
            //next();
        }
        else if (ctx.req.baseUrl.toString() == '/api/Sellers' && ctx.req.method.toString() == 'PUT') {
            console.log("seller req: "+JSON.stringify(ctx.req.body));
            var SMS = server.models.SMS;
            
            if(ctx.req.body.paymentStatus==true || ctx.req.body.paymentStatus=="1" || ctx.req.body.paymentStatus==1){


                var smsMessage = 'Hi, Congratulations your VYULI account is now activated, please go ahead uploading the images and showcase your latest product offerings to your customers';
                var destination =ctx.result.phone;

                SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                });
            }else if(ctx.req.body.paymentStatus==false || ctx.req.body.paymentStatus=="0" || ctx.req.body.paymentStatus==0){

                var smsMessage = "Hi! Thank you for downloading and registering on VYULI, on your payment realisation your account would be activated to showcase your product offerings to your customers.";
                var destination =ctx.result.phone;

                SMS.create({destination: destination, message: smsMessage}, function (err, sms) {
                });
            }

            var Account = server.models.Account;
            Account.find({"where": {"userId": ctx.result.phone, "userType": "Seller"}}, function (err, accounts) {
                if (accounts) {
                    var account = accounts[0];
                    account.updateAttributes({
                        "name": ctx.result.name, "businessTypeName": ctx.result.businessTypeName,
                        "businessTypeId": ctx.result.businessTypeId, "businessName": ctx.result.businessName,
                        "city": ctx.result.city, "mail": ctx.result.mail
                    }, function (err, accountData) {
                        //console.log('Update Account:'+JSON.stringify(accountData));
                        /*ctx.result = {
                         status: "success",
                         message: "Updated",
                         data: ctx.result
                         };*/
                        if (ctx.req.body.name != undefined || ctx.req.body.businessName != undefined || ctx.req.body.businessTypeId != undefined || ctx.req.body.businessTypeName != undefined) {
                            console.log("generating new qrcode for put method of seller");
                            fileWriteToAWS(ctx, next);
                        } else {
                            console.log("seller put method giving response with qrcode generation");
                            ctx.result = {
                                status: "success",
                                message: "Updated",
                                data: ctx.result
                            };
                            next();
                        }

                        /*next();*/
                    });
                }
            });
            //next();
        }
        else {
            console.log("entering into new response after else next method");
            ctx.result = {
                status: "success",
                message: "",
                data: ctx.result
            };
            //console.log("entering into new response after else method");
            if (ctx.req.baseUrl.toString() == '/api/Accounts' && ctx.req.method.toString() == 'GET') {
                console.log("after accounts method get");
                if (ctx.result.data == undefined) {

                } else {
                    if (ctx.result.data.length > 0) {
                        ctx.result.message = "User Already Registered";
                    } else {
                        ctx.result.message = 'User is Not Registered';
                    }
                }

            }
            next();
        }
        //}

    });

    var fs = require('fs');

// function to encode file data to base64 encoded string
    function base64_encode(file) {
        // read binary data
        var bitmap = fs.readFileSync(file);
        // convert binary data to base64 encoded string
        return new Buffer(bitmap).toString('base64');
    }

// function to create file from base64 encoded string
    function base64_decode(base64str, file) {
        // create buffer object from base64 encoded string, it is important to tell the constructor that the string is base64 encoded
        var bitmap = new Buffer(base64str, 'base64');
        // write buffer to file
        fs.writeFileSync(file, bitmap);
        console.log('******** File created from base64 encoded string ********');
    }

// convert image to base64 encoded string
//  var base64str = base64_encode('kitten.jpg');
//  console.log(base64str);
// convert base64 string back to image
//  base64_decode(base64str, 'copy.jpg');

    function thumbNailCreate(ctx, next) {

        // start.. code for thumbNail creation of frame image uploaded by seller
        var thumbNailName = ctx.req.body.image.uploadimagename;
        //var thumbNailName = ctx.req.body.imageName;
        console.log("frameInfo: " + JSON.stringify(ctx.req.body));
        console.log("thumbNailName: " + thumbNailName);
        var s3ImageUrl = "https://s3-ap-southeast-1.amazonaws.com/araay/" + thumbNailName;
        console.log("s3ImageUrl: " + s3ImageUrl);
        console.log("frameId: " + ctx.req.params.id);
        var frameId = ctx.req.params.id;
        var Jimp = require("jimp");
        var uploadImage = thumbNailName;
        var frameImageSplit = uploadImage.split('.');
        var frameName = frameImageSplit[0];
        var Frame = server.models.Frame;
        //var s3ImageUrl="/imgs/frame.jpg";
        console.log("frameName: " + frameName);
        // s3 bucket link /*https://s3-ap-southeast-1.amazonaws.com/araay/20141123_164955.jpg*/imgs/" + uploadImage + ""
        Jimp.read(s3ImageUrl, function (err, frameName) {
            console.log("s3ImageUrl: " + s3ImageUrl);
            console.log("uploadImage: " + uploadImage);
            if (err) {
                //cb(err,null);
                console.log("err in JIMP: " + JSON.stringify(err));
                next();
            } else {
                //console.log("image: "+uploadImage);
                frameName.resize(120, 120)            // resize
                    .quality(100)                 // set JPEG quality
                    .write("thumbNails/" + uploadImage + "", function (err, writeR) {
                        if (err) {
                            console.log("error in writing to file");
                            next();
                        } else {
                            console.log("writed Response: ");
                            imageUpload(uploadImage);
                        }

                    });          // save
                console.log("resized");

            }
        });
        try {

            var imageUpload = function (uploadImage) {
                var AWS = require('aws-sdk');
                var fs = require('fs');
                //aws credentials
                AWS.config = new AWS.Config();
                AWS.config.accessKeyId = "AKIAJBPDSJ47N7DAPPLQ";
                AWS.config.secretAccessKey = "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";
                AWS.config.region = "ap-southeast-1";
                var s3 = new AWS.S3();

                var bodystream = fs.createReadStream('thumbNails/' + uploadImage + '');

                var params = {
                    'Bucket': 'araay',
                    'Key': 'thumbnails/' + uploadImage + '',
                    'Body': bodystream,
                    'ContentEncoding': 'base64',
                    'ContentType ': 'image/jpeg'
                };

                //also tried with s3.putObject
                s3.upload(params, function (err, data) {
                    if (err) {
                        //cb(err, null);
                        console.log("error in upload of thumbNail");
                        next();
                    } else {
                        console.log("data: " + JSON.stringify(data));

                        //cb(null, "success");
                        if (data.Location) {

                            Frame.findById(frameId, function (err, frameR) {
                                console.log("frameR: " + JSON.stringify(frameR));
                                frameR.updateAttributes({
                                    "thumbNailUrl": data.Location,
                                    "frameImageUrl": s3ImageUrl
                                }, function (err, frameRes) {

                                    //cb(null, frameRes);
                                    try {
                                        cleanUp(uploadImage);
                                        /* setTimeout(function () {
                                         console.log("setTimeout: It's been 30 seconds!");
                                         cleanUp(uploadImage);
                                         }, 30000);*/
                                    } catch (err) {
                                        console.log("setTimeOutError: " + err);
                                    }

                                });

                            });
                            //cleanUp(uploadImage);
                            next();
                        } else {
                            console.log("failure of upload into s3 bucket: " + JSON.stringify(data));
                            next();
                        }

                    }
                })

            }
        } catch (err) {
            console.log("imageUploadError: " + err);
        }
        var cleanUp = function (cleanImageName) {
            console.log("called for clean up " + cleanImageName);

            try {
                var fs = require('fs');
                var filePath = "thumbNails/" + cleanImageName + "";
                fs.unlink(filePath);
                //cb(null, "success");
                console.log("cleaned");
            } catch (e) {
                console.log("cleanUp error: " + e);
            }
        }
        //next();
        // end.. code for thumbNail creation of frame image uploaded by seller
    }

    /*try {

     var imageUpload = function (uploadImage,frameId,ctx, next) {
     var AWS = require('aws-sdk');
     var fs = require('fs');
     //aws credentials
     AWS.config = new AWS.Config();
     AWS.config.accessKeyId = "AKIAJBPDSJ47N7DAPPLQ";
     AWS.config.secretAccessKey = "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";
     AWS.config.region = "ap-southeast-1";
     var s3 = new AWS.S3();

     var bodystream = fs.createReadStream('thumbNails/' + uploadImage + '');
     var Frame = server.models.Frame;
     var params = {
     'Bucket': 'araay',
     'Key': 'thumbnails/' + uploadImage + '',
     'Body': bodystream,
     'ContentEncoding': 'base64',
     'ContentType ': 'image/jpeg'
     };

     //also tried with s3.putObject
     s3.upload(params, function (err, data) {
     if (err) {
     //cb(err, null);
     console.log("error in upload of thumbNail");
     next();
     } else {
     console.log("data: " + JSON.stringify(data));

     //cb(null, "success");
     if (data.Location) {

     Frame.findById(frameId, function (err, frameR) {
     console.log("frameR: " + JSON.stringify(frameR));
     frameR.updateAttributes({
     "thumbNailUrl": data.Location,
     "frameImageUrl": s3ImageUrl
     }, function (err, frameRes) {

     //cb(null, frameRes);

     });

     });
     try {
     cleanUp(uploadImage,ctx, next);
     /!* setTimeout(function () {
     console.log("setTimeout: It's been 30 seconds!");
     cleanUp(uploadImage);
     }, 30000);*!/
     } catch (err) {
     console.log("setTimeOutError: " + err);
     }
     /!* next();*!/
     } else {
     console.log("failure of upload into s3 bucket: " + JSON.stringify(data));
     next();
     }

     }
     })

     }
     } catch (err) {
     console.log("imageUploadError: " + err);
     }
     var cleanUp = function (cleanImageName,ctx,next) {
     console.log("called for clean up " + cleanImageName);

     try {
     var fs = require('fs');
     var filePath = "thumbNails/" + cleanImageName + "";
     fs.unlink(filePath);
     //cb(null, "success");
     console.log("cleaned");
     } catch (e) {
     console.log("cleanUp error: " + e);
     }
     next();
     }*/

};