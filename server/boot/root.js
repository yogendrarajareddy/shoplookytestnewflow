module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  //router.get('/', server.loopback.status());

    /*router.use('/', function(req, res, next){

        //console.log("entering: "+ req.url);
        var urlString = req.url.toString();
        if(urlString.indexOf('api') > -1){
            console.log(urlString);
            var url = "http://ouroneapp.com"+ req.url;
            req.pipe(request(url)).pipe(res);
        } else {
            next();
        }

        //next();
        //res.render('index', { title: 'CremeVille' });

    });*/

    router.use('/payUSuccess', function (req, res, next) {
        console.log("request body" + JSON.stringify(req.body));
        console.log("res body" + res.body);
        res.render('payUSuccess', req.body);
    });

    router.use('/payUFailure', function (req, res, next) {
        console.log("request body" + JSON.stringify(req.body));
        console.log("res body" + res.body);
        res.render('payUFailure', req.body);
    });

  server.use(router);
};
